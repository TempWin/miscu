<?php 
//
//  Formulario para crear una transacción
//
//
//print_r($_SERVER);
$peticion = basename($_SERVER["REQUEST_URI"]);
$tipo_transaccion = ""; // gasto, ingreso, transferencia
$titulo = "";

$presupuestos = listar_presupuestos($usuario_id);
$categorias = listar_categorias($usuario_id);
$divisas = listado_divisas();

// Según se llame al script desde una u otra página,
// adaptaremos la variables / comportamiento
switch ($peticion) {
    case 'transaccion_gasto.php':
        $tipo_transaccion = "gasto";
        $tipo_transaccion_id = 1;
        $cuentas_origen = listar_cuentas($usuario_id, "activos");
        $cuentas_destino = listar_cuentas($usuario_id, "gastos");
        $titulo_origen = "Cuenta activos (origen)";
        $titulo_destino = "Cuenta de gastos (destino)";
        break;
    case 'transaccion_ingreso.php':
        $tipo_transaccion = "ingreso";
        $tipo_transaccion_id = 2;
        $cuentas_origen = listar_cuentas($usuario_id, "ingresos");
        $cuentas_destino = listar_cuentas($usuario_id, "activos");
        $titulo_origen = "Cuenta de ingresos (origen)";
        $titulo_destino = "Cuenta de activos (destino)";
        break;
    case 'transaccion_transferencia.php':
        $tipo_transaccion = "transferencia";
        $tipo_transaccion_id = 3;
        $cuentas_origen = listar_cuentas($usuario_id, "activos");
        $cuentas_destino = listar_cuentas($usuario_id, "activos");
        $titulo_origen = "Cuenta activos (origen)";
        $titulo_destino = "Cuenta activos (destino)";
        break;
    default:
        # code...
        break;
}
?>
        <div class="col-lg-4 col-xl-4 col-sm-12">
            <form action="transaccion_procesar.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="tipo_transaccion_id" value="<?php echo $tipo_transaccion_id; ?>">
                <input type="hidden" name="usuario_id" value="<?php echo $usuario_id; ?>">
                <div class="form-group">
                    <label for="usuario">Descripción</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" required aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="email"><?php echo $titulo_origen; ?></label>
                    <select name="cuenta_origen" id="cuenta_origen" class="custom-select">
<?php 
foreach ($cuentas_origen as $id => $datos) {
    echo "
                        <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
}
?>              
                    </select>  
                </div>
                <div class="form-group">
                    <label for="clave"><?php echo $titulo_destino; ?></label>
                    <select name="cuenta_destino" id="cuenta_destino" class="custom-select">
<?php 
foreach ($cuentas_destino as $id => $datos) {
    echo "
                        <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
}
?>                    
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="clave">Importe</label>
                        <input type="text" class="form-control" id="importe" name="importe" required>
                    </div>
                    
                    <div class="form-group col-md-5">
                        <label for="clave">Divisa</label>
                        <select name="divisa_id" class="custom-select">
<?php 
foreach ($divisas as $id => $datos) {
    if ($datos["simbolo"] == "€") {
        echo "
                        <option selected value=\"" . $id . "\">" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    } else {
        echo "
                        <option value=\"" . $id . "\">" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    }
}
?>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <fieldset><a href="#" onclick="mostrar_divisa_extranjera();">Divisa extranjera <i class="fas fa-caret-down"></i></a></fieldset>
                    </div>
                </div>
                <div class="form-row divisa-extranjera" style="display: none;">
                    <div class="form-group col-md-5">
                        <label for="clave">Importe extranjero</label>
                        <input type="text" class="form-control" id="importe_extranjero" name="importe_extranjero">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="clave">Divisa extranjera</label>
                        <select name="divisa_extranjera_id" class="custom-select">
                            <option value="0" selected>Ninguna</option>
<?php 
foreach ($divisas as $id => $datos) {
    echo "
                        <option value=\"" . $id . "\">" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
}
?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="clave">Fecha</label>
                    <input type="text" class="form-control" id="fecha" name="fecha" required value="<?php echo date("Y-m-d H:i"); ?>">
                </div>
<?php 
// Solo mostramos categorías y presupuestos si no se trata de transferencias
if ($tipo_transaccion_id != 3) {
?>            
                <div class="form-group">
                    <label for="clave">Presupuesto</label>
                    <select name="presupuesto_id" class="custom-select">
                        <option value="0" selected>Ninguno</option>
<?php
foreach ($presupuestos as $presupuestos_array_id => $presupuesto_datos) {
    echo "
                        <option value=\"" . $presupuesto_datos["id"] . "\">" . $presupuesto_datos["nombre"] . "</option>" . PHP_EOL;
}
?>                    
                    </select>
                </div>
                <div class="form-group">
                    <label for="categoria">Categoría</label>
                    <select name="categoria_id" class="custom-select">
                        <option value="0" selected>Ninguna</option>
<?php
foreach ($categorias as $categorias_array_id => $categoria_datos) {
    echo "
                        <option value=\"" . $categoria_datos["id"] . "\">" . $categoria_datos["nombre"] . "</option>" . PHP_EOL;
}
?>                
                    </select> 
                </div>
<?php 
}
?>            
                <div class="form-group">
                    <label for="exampleFormControlFile1">Subir archivo</label>
                    <input type="file" class="form-control-file" id="fichero" name="fichero">
                </div>
                <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
            </form>
        </div> <!-- col -->