<?php
    // Barra lateral con el menú de la aplicación
?>
            <ul class="navbar-nav sidebar-bg sidebar sidebar-dark accordion" id="accordionSidebar">
                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-donate"></i>
                        <!--<i class="fas fa-cash-register"></i>-->
                        <!--<i class="fab fa-monero"></i>-->
                    </div>
                    <div class="sidebar-brand-text mx-3">
                        Miscu
                    </div>
                </a>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">
                <!-- Nav Item - Dashboard -->
                <li class="nav-item">
                    <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Cuadro de Mando</span>
                    </a>
                </li>

                <hr class="sidebar-divider">

                <div class="sidebar-heading">
                    Operaciones
                </div>
                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCuentas" aria-expanded="true" aria-controls="collapseCuentas">
                    <i class="far fa-money-bill-alt"></i>
                    <span>Cuentas</span>
                    </a>
                    <div id="collapseCuentas" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded border-colored">
                            <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                            <a class="collapse-item" href="cuentas_activos.php"><i class="far fa-money-bill-alt"></i> Cuentas de activos</a>
                            <a class="collapse-item" href="cuentas_gastos.php"><i class="fas fa-shopping-cart"></i> Cuentas de gastos</a>
                            <a class="collapse-item" href="cuentas_ingresos.php"><i class="fas fa-hand-holding-usd"></i> Cuentas de ingresos</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTransacciones" aria-expanded="true" aria-controls="collapseTransacciones">
                    <i class="fas fa-sync"></i>
                    <span>Transacciones</span>
                    </a>
                    <div id="collapseTransacciones" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded border-colored">
                            <!-- <h6 class="collapse-header">Custom Components:</h6> -->
                            <a class="collapse-item" href="transaccion_gasto.php"><i class="fas fa-shopping-cart"></i> Gasto</a>
                            <a class="collapse-item" href="transaccion_ingreso.php"><i class="fas fa-hand-holding-usd"></i> Ingreso</a>
                            <a class="collapse-item" href="transaccion_transferencia.php"><i class="fas fa-exchange-alt"></i> Transferencia</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">
                <!-- Heading -->
                <div class="sidebar-heading">
                    Gestión del dinero
                </div>
                <li class="nav-item">
                    <a class="nav-link" href="categorias.php">
                    <i class="fas fa-bars"></i>
                    <span>Categorías</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="presupuestos.php">
                    <i class="fas fa-clipboard-list"></i>
                    <span>Presupuestos</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="huchas.php">
                    <i class="fas fa-piggy-bank"></i>
                    <span>Huchas</span></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="informes.php">
                    <i class="fas fa-chart-pie"></i>
                    <span>Informes</span></a>
                </li>
                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">
                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>
            </ul>