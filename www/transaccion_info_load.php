<?php
//
// Información sobre la transacción para cargar por AJAX
//
//  14/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");

$transaccion_log_id = $_GET["id"];

$transaccion_log = info_transaccion_log($transaccion_log_id);

// Detalle de la transacción con los movimientos de salida y entrada 
// de dinero (cuenta origen y destino)
$transaccion = info_transaccion($transaccion_log_id);

// Dependiendo del tipo de transacción, daremos un estilo
// diferente al importe
switch ($transaccion_log["tipo_id"]) {
    case 1:
        $estilo = "gasto";
        $importe = $transaccion[$transaccion_log_id]["origen"]["importe"];
        break;
    case 2: 
        $estilo = "ingreso";
        $importe = $transaccion[$transaccion_log_id]["destino"]["importe"];
        break;
    default:
        $estilo = "";
        $importe = $transaccion[$transaccion_log_id]["origen"]["importe"];
}

$fecha_transaccion = new DateTime($transaccion_log["fecha"]);
$fecha_transaccion = $fecha_transaccion->format("d/m/Y H:i");

/*
echo "<pre>" . PHP_EOL;
print_r($movimientos);
echo "</pre>" . PHP_EOL;
*/
?>
                                        <table class="table">
                                            <tr>
                                                <td>Tipo</td>
                                                <td><?php echo $transaccion_log["tipo"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Fecha</td>
                                                <td><?php echo $fecha_transaccion; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Cuenta origen</td>
                                                <td><a href="cuenta_info.php?id=<?php echo $transaccion[$transaccion_log_id]["origen"]["cuenta_id"]; ?>"><?php echo $transaccion[$transaccion_log_id]["origen"]["cuenta"] ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>Cuenta destino</td>
                                                <td><a href="cuenta_info.php?id=<?php echo $transaccion[$transaccion_log_id]["destino"]["cuenta_id"]; ?>"><?php echo $transaccion[$transaccion_log_id]["destino"]["cuenta"] ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>Importe</td>
                                                <td><span class="<?php echo $estilo;?>"><?php echo str_replace(".", ",", round($importe, $transaccion_log["decimales"])); ?> <?php echo $transaccion_log["simbolo"] ?></span></td>
                                            </tr>
                                        </table>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button onclick="mostrar_editar_transaccion(<?php echo $transaccion_log_id; ?>);" class="btn btn-primary">Editar</button>
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#eliminar_transaccion">Eliminar</a>
                                        </div>
