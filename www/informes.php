<?php
//  Página de informes
//
//  13/03/2019

session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$cuentas_usuario = listar_cuentas($usuario_id, "activos");
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Informes - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Datetimepicker -->
        <link rel="stylesheet" type="text/css" href="vendor/datetimepicker/jquery.datetimepicker.css">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Informes</h1>
                        <p>Descarga información de tus cuentas de activos.</p>
<?php 

if (empty($cuentas_usuario)) {
?>
                        <div class="card mb-4 py-3 border-left-primary">
                            <div class="card-body">
                                Aún no has creado ninguna cuenta, así que no puedes generar informes.
                            </div>
                        </div>
<?php
} else {
?>                        
                        <div class="row">
                            <!-- Area Chart -->
                            <div class="col-xl-7 col-lg-7">
                                <form method="post" action="informe_movimientos_cuenta.php">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="cuenta">Cuenta de activos</label>
                                            <select id="cuenta_id" class="form-control" name="cuenta_id" required>
                                                <option disabled>Selecciona...</option>
<?php 
foreach ($cuentas_usuario as $id => $datos) {
    echo "
                                                <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
}
?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="fecha_inicio">Desde la fecha</label>
                                            <input type="text" id="fecha_inicio" class="form-control" name="fecha_inicio" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="fecha_fin">Hasta la fecha</label>
                                            <input type="text" id="fecha_fin" class="form-control" name="fecha_fin" required>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-primary" value="Exportar">
                                </form>
                            </div> <!-- col -->
                        </div> <!-- row -->
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Datetimepicker -->
        <script src="vendor/datetimepicker/build/jquery.datetimepicker.full.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
        <script>
            $.datetimepicker.setLocale('es');
            $("#fecha_inicio").datetimepicker({
                timepicker: false,
                dayOfWeekStart: 1,
                format:'Y-m-d'
            });

            $("#fecha_fin").datetimepicker({
                timepicker: false,
                dayOfWeekStart: 1,
                format:'Y-m-d'
            });
        </script>

    </body>
</html>
