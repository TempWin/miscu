<?php
/* 
    Mensaje con información sobre la creación de la transacción

    15/02/2019
*/
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Transacciones - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Transacciones</h1>
                        <div class="row">
                            <div class="col-lg-12 mb-4">
                                <div class="card bg-warning text-white shadow">
                                    <div class="card-body">
                                        Error al crear la transacción
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <ul>
                                    <li><a href="/transaccion_gasto.php">Nuevo gasto</a></li>
                                    <li><a href="/transaccion_ingreso.php">Nuevo ingreso</a></li>
                                    <li><a href="/transaccion_transferencia.php">Nueva transferencia</a></li>
                                </ul>
                            </div>
                        </div>
                    </div> <!-- container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div> <!-- End of Content Wrapper -->
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
    </body>
</html>
