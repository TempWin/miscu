<?php
//
// Formulario para editar una transacción
//
require_once("functions.php");

extract($_POST, EXTR_OVERWRITE);

// Convertimos los decimales en la notación inglesa para poder
// introducirlo sin problemas en la base de datos
$importe = (float) str_replace(",", ".", $importe);

if (editar_transaccion($transaccion_log_id, $transaccion_origen_id, $transaccion_destino_id, $fecha, $descripcion, $cuenta_origen_id, $cuenta_destino_id, $importe, $divisa_id, $categoria, $presupuesto)) {
    echo "Transacción modificada correctamente";
} else {
    echo "Error al modificar la transacción";
}
?>
