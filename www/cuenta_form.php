<?php
//
// Formulario para crear una cuenta
//
//  La divisa por defecto es el Euro (id = 1)
//
ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");

$cuenta_tipo = $_GET["tipo"];
$usuario_id = $_GET["usuario"];
$divisas = listado_divisas();

switch ($cuenta_tipo) {
    // Nueva cuenta de activos
    case 1:
        $titulo_cuenta = "Nueva cuenta de activos";
        $tipos_cuenta = [1, 2, 3];
        break;
    case 2:
        $titulo_cuenta = "Nueva cuenta de ingresos";
        $tipos_cuenta = [5];
        break;
    default:
        $titulo_cuenta = "Nueva cuenta de gastos";
        $tipos_cuenta = [4];        
        break;
}
/*
    // Convertimos los decimales en la notación inglesa para poder
    // introducirlo sin problemas en la base de datos
    $saldo_inicial = (float) str_replace(",", ".", $saldo_inicial);

    $cuenta_tipo_id = (int) $cuenta_tipo_id;
    $divisa_id = (int) $divisa_id;
    */
?>
        <div class="col-lg-6 col-xl-6 col-sm-12">
            <form action="" method="post" id="form-cuenta-nueva">
                <h3><?php echo $titulo_cuenta; ?></h3>
                <input type="hidden" name="usuario_id" id="usuario_id" value="<?php echo $usuario_id; ?>">
                <div class="form-group">
                    <label for="email">Tipo de cuenta</label>
                    <select name="cuenta_tipo_id" class="custom-select" required>
<?php                    
$tipos = obtener_tipos_cuenta();
foreach ($tipos as $tipo) {
    if (in_array($tipo["id"], $tipos_cuenta)) {
        echo "
                    <option value=\"" . $tipo["id"] . "\">" . $tipo["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre_cuenta" name="nombre_cuenta" required aria-describedby="nombre" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>">
                </div>
                <div class="form-group">
                    <label for="usuario">Descripción</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" required aria-describedby="emailHelp" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>">
                </div>
                <div class="form-group">
                    <label for="iban">IBAN</label>
                    <input type="text" class="form-control" id="iban" name="iban" aria-describedby="emailHelp" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>">
                </div>
                <div class="form-group">
                    <label for="bic">BIC</label>
                    <input type="text" class="form-control" id="bic" name="bic" aria-describedby="emailHelp" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>">
                </div>                
                <div class="form-group">
                    <label for="email">Divisa</label>
                    <select name="divisa_id" class="custom-select" required>
<?php                    
// Seleccionamos por defecto el Euro como divisa
foreach ($divisas as $id => $datos) {
    if ($id == 1) {
        echo "
                        <option value=\"" . $id . "\" selected>" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    } else {
        echo "
                        <option value=\"" . $id . "\">" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    }
} 
?>
                    </select>
                </div>
<?php 
// Solo si es una cuenta de activos damos la posibilidad de establecer un saldo inicial
if ($cuenta_tipo == 1) {
?>                
                <div class="form-group">
                    <label for="clave">Saldo inicial</label>
                    <input type="text" class="form-control" id="saldo_inicial" name="saldo_inicial" required value="<?php if(isset($_POST["saldo_inicial"])) echo $_POST["saldo_inicial"]; ?>">
                </div>
<?php 
}
?>                
                <a href="#" onclick="nueva_cuenta(<?php echo $usuario_id; ?>, <?php echo $cuenta_tipo; ?>);" type="button" class="btn btn-primary" name="enviar">Crear</a>
            </form>
        </div><!-- col -->