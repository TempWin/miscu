<?php
//
// Formulario para crear una hucha
//
//  07/03/2019

require_once("functions.php");

extract($_POST, EXTR_OVERWRITE);

$hucha_nombre;
$hucha_cuenta;
$hucha_importe_objetivo;
$hucha_fecha_objetivo;

if ($hucha_fecha_objetivo == "") {
    $hucha_fecha_objetivo = null;
} 

if (!isset($descripcion)) {
    $descripcion = null;
}

if (crear_hucha($hucha_nombre, $hucha_cuenta, $hucha_importe_objetivo, $descripcion, $hucha_fecha_objetivo)) {
    echo "Hucha creada";
} else {
    echo "Error al crear hucha" . PHP_EOL;
}
?>
    