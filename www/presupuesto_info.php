<?php
//
// Información sobre el presupuesto
//
// En principio solo mostraría información del año actual
//
//      * Importe presupuestado por mes
//
//  26/02/2019
//
session_start();

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");
require_once("sql.php");

if (isset($_GET["id"])) {
    $presupuesto_id = $_GET["id"];
    $error = false;

    $presupuesto = obtener_presupuesto($presupuesto_id);
    $presupuesto_limites = obtener_limites_presupuesto($presupuesto_id);
} else {
    $error = true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
<?php 
if ($error) {
?>
                        <h1>Presupuesto</h1>
                        <div class="alert alert-warning" role="alert">
                            Debes seleccionar un <a href="/presupuestos.php">presupuesto</a> para consultar su información
                        </div>
<?php 
} else if (empty($presupuesto)) {
?>
                        <h1>Presupuesto</h1>
                        <div class="alert alert-warning" role="alert">
                            No existe ningún presupuesto con ese identificador.
                        </div>
<?php 
} else {
?>
                        <h1 class="h3"><span class="text-muted">Presupuesto »</span> <?php echo $presupuesto["nombre"]; ?></h1>

                        <div class="row">
                            <div class="col-xs-12 col-lg-8">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header">
                                        <h6 class="m-0 font-weight-bold text-primary">Presupuestos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Presupuesto</th>
                                                    <th>Gastado</th>
                                                    <th>Disponible</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
    foreach ($presupuesto_limites as $datos) {
        $importe = $datos["importe"];
        $fecha_limite_presupuesto = $datos["fecha"];
        $gastos = obtener_gastos_presupuesto($presupuesto_id, substr($fecha_limite_presupuesto, 0, 7));
        $disponible = $importe - $gastos;
        if ($disponible < 0) {
            $estilo_disponible = "negativo";
        } else {
            $estilo_disponible = "";
        }
        $importe = number_format($importe, 2, ",", ".");
        $gastos = number_format($gastos, 2, ",", ".");
        $disponible = number_format($disponible, 2, ",", ".");
        $fecha_presupuesto = (new DateTime($datos["fecha"]))->format("d/m/Y");
        echo "
                                                <tr>
                                                    <td>" . $fecha_presupuesto . "</td>
                                                    <td>" . $importe . " €</td>
                                                    <td>" . $gastos . " €</td>
                                                    <td><span class=\"" . $estilo_disponible . "\">" . $disponible . " €</span></td>
                                                </tr>" . PHP_EOL;
    }
?>                                                    
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- card -->
                            </div>
                        </div> <!-- row -->
<?php 
}
?>                            
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
    </body>
</html>