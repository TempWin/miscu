<?php 
//
//  Edita el nombre de un presupuesto
//
if (isset($_POST["presupuesto_id"]) && isset($_POST["presupuesto_nombre"])) {
    $presupuesto_id = $_POST["presupuesto_id"];
    $presupuesto_nombre = $_POST["presupuesto_nombre"];

    require_once("functions.php");

    $conexion = conectar_bd();
    $fecha_actual = date("Y-m-d H:i:s");

    $actualizar_presupuesto_sql = "
        UPDATE presupuestos
        SET nombre = :presupuesto_nombre,
            fecha_actualizacion = :fecha_actual
        WHERE id = :presupuesto_id
    ";

    $stmt = $conexion->prepare($actualizar_presupuesto_sql);
    $stmt->bindValue("presupuesto_nombre", $presupuesto_nombre);
    $stmt->bindValue("fecha_actual", $fecha_actual);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->execute();
} else {
    echo "Error modificando presupuesto";
}

?>        