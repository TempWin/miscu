<?php 
// Listado de presupuestos del usuario
//
//  06/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");
?>
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre presupuesto</th>
                                                    <th>Importe mes actual</th>
                                                    <th>Gastado</th>
                                                    <th>Disponible</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
$presupuestos = listar_presupuestos($usuario_id);
 
foreach ($presupuestos as $presupuesto) {
    $presupuesto_info = presupuesto_mes($presupuesto["id"], date("Y-m"));
    if (empty($presupuesto_info)) {
        $presupuesto_importe = 0;
    } else {
        $presupuesto_importe = round($presupuesto_info["importe"], 2);
    }

    $gastado = round(obtener_gastos_presupuesto($presupuesto["id"], date("Y-m")), 2);
    $restante = round($presupuesto_importe - $gastado, 2);
    echo "
                                                <tr>
                                                    <td id=\"prep-" . $presupuesto["id"] . "\"><a href=\"presupuesto_info.php?id=" .$presupuesto["id"] . "\">" . $presupuesto["nombre"] . "</a></td>
                                                    <td>
                                                        <div class=\"input-group\">
                                                            <div class=\"input-group-prepend\">
                                                                <div class=\"input-group-text form-control-sm\">€</div>
                                                            </div>
                                                            <input id=\"presupuesto_importe_id_" . $presupuesto["id"] . "\" value=\"" . $presupuesto_importe . "\" autocomplete=\"off\" min=\"0\" name=\"importe\" type=\"number\" class=\"form-control form-control-sm\"> <a class=\"btn\" href=\"#\" onclick=\"actualizar_importe_presupuesto(" . $presupuesto["id"] . ");\"><i class=\"fas fa-check\"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>" . $gastado . " €</td>
                                                    <td>" . $restante . " €</td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $presupuesto["id"] . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_presupuesto(" . $presupuesto["id"] . ");\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"presupuesto_eliminar.php?id=" . $presupuesto["id"] . "\"><i class=\"far fa-trash-alt\"></i></a>
                                                        </div>
                                                    </td>
                                                    <!-- <td id=\"edit-" . $presupuesto["id"] . "\"><a href=\"#\" onclick=\"campo_editable_presupuesto(" . $presupuesto["id"] , ");\"><i class=\"far fa-edit\"></i></a></td>
                                                    <td><a href=\"presupuesto_eliminar.php?id=" . $presupuesto["id"] . "\"><i class=\"far fa-trash-alt\"></i></td> -->
                                                </tr>" . PHP_EOL;
}
echo "
                                            </tbody>
                                        </table>" . PHP_EOL;