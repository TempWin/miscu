<?php
//
// Formulario para modificar una transacción
//
ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");

// Apunte de la transacción
$transaccion_log_id = $_GET["id"];

// Información sobre el apunte de la transacción
$transaccion_log = info_transaccion_log($transaccion_log_id);

// Detalle de la transacción con los movimientos de salida y entrada 
// de dinero (cuenta origen y destino)
$transaccion = info_transaccion($transaccion_log_id);

//$tipos_transacciones = obtener_tipos_transacciones();
$divisas = listado_divisas();

$importe = str_replace(".", ",", abs($transaccion[$transaccion_log_id]["origen"]["importe"]));

$categorias = listar_categorias($transaccion_log["usuario_id"]);
$categoria_transaccion = obtener_categoria_transaccion($transaccion_log_id);

$presupuestos = listar_presupuestos($transaccion_log["usuario_id"]);
$presupuesto_transaccion = obtener_presupuesto_transaccion($transaccion_log_id);

switch ($transaccion_log["tipo_id"]) {
    // Gasto
    case 1:
        $titulo_cuenta_origen = "Cuenta de activos (origen)";
        $titulo_cuenta_destino = "Cuenta de gastos (destino)";
        $cuentas_origen = listar_cuentas($transaccion_log["usuario_id"], "activos");
        $cuentas_destino = listar_cuentas($transaccion_log["usuario_id"], "gastos");
        break;
    // Ingreso
    case 2:
        $titulo_cuenta_origen = "Cuenta de ingresos (origen)";
        $titulo_cuenta_destino = "Cuenta de activos (destino)";
        $cuentas_origen = listar_cuentas($transaccion_log["usuario_id"], "ingresos");
        $cuentas_destino = listar_cuentas($transaccion_log["usuario_id"], "activos");
    case 3:
        $titulo_cuenta_origen = "Cuenta de activos (origen)";
        $titulo_cuenta_destino = "Cuenta de activos (destino)";
        $cuentas_origen = listar_cuentas($transaccion_log["usuario_id"], "activos");
        $cuentas_destino = listar_cuentas($transaccion_log["usuario_id"], "activos");
    default:
        # code...
        break;
}

/*
// DEBUG
echo "<pre>" . PHP_EOL;
print_r($datos_cuenta);
echo "</pre>" . PHP_EOL;
*/
?>
        <div class="col-lg-6 col-xl-6 col-sm-12">

            <form action="" method="post" id="form-editar-transaccion">
                <h2>Modificar transacción</h2>
                <div class="form-group">
                    <input type="hidden"  id="transaccion_log_id" name="transaccion_log_id" value="<?php echo $transaccion_log_id; ?>">
                    <input type="hidden" name="transaccion_origen_id" value="<?php echo $transaccion[$transaccion_log_id]["origen"]["id"] ?>">
                    <input type="hidden" name="transaccion_destino_id" value="<?php echo $transaccion[$transaccion_log_id]["destino"]["id"] ?>">

                    <label for="descripcion">Descripción</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" required aria-describedby="descripcion" value="<?php echo $transaccion_log["descripcion"]; ?>">
                </div>
                <div class="form-group">
                    <label for="cuenta_origen"><?php echo $titulo_cuenta_origen; ?></label>
                    <select name="cuenta_origen_id" class="custom-select">
<?php                    
foreach ($cuentas_origen as $id => $datos) {
    // Si la opción de ID de cuenta coincide con el ID de cuenta de la cuenta
    // seleccionada, la marcamos por defecto
    if ($id == $transaccion[$transaccion_log_id]["origen"]["cuenta_id"]) {
        echo "
                    <option value=\"" . $id . "\" selected>" . $datos["nombre"] . "</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cuenta_destino"><?php echo $titulo_cuenta_destino; ?></label>
                    <select name="cuenta_destino_id" class="custom-select">
<?php                    
foreach ($cuentas_destino as $id => $datos) {
    // Si la opción de ID de cuenta coincide con el ID de cuenta de la cuenta
    // seleccionada, la marcamos por defecto
    if ($id == $transaccion[$transaccion_log_id]["destino"]["cuenta_id"]) {
        echo "
                    <option value=\"" . $id . "\" selected>" . $datos["nombre"] . "</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="descripcion">Fecha</label>
                    <input type="text" class="form-control" id="fecha" name="fecha" required aria-describedby="fecha" value="<?php echo $transaccion_log["fecha"]; ?>">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="importe">Importe</label>
                        <input type="text" class="form-control" id="importe" name="importe" aria-describedby="importe" value="<?php echo $importe; ?>">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="email">Divisa</label>
                        <select name="divisa_id" class="custom-select">
<?php                    
foreach ($divisas as $id => $datos) {
    if ($id == $transaccion_log["divisa_id"]) {
        echo "
                    <option value=\"" . $id . "\" selected>" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $id . "\">" . $datos["simbolo"] . " (" . $datos["nombre"] . ")</option>" . PHP_EOL;
    }
}
?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="categoria">Categoría</label>
                    <select name="categoria" class="custom-select">
                        <option value="0">Ninguna</option>
<?php                    
foreach ($categorias as $categoria) {
    // Si la opción de ID de la categoría coincide con el ID de la categoría de la
    // transacción seleccionada, la marcamos por defecto
    if ($categoria["id"] == $categoria_transaccion["id"]) {
        echo "
                    <option value=\"" . $categoria["id"] . "\" selected>" . $categoria["nombre"] . "</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $categoria["id"] . "\">" . $categoria["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="presupuesto">Presupuesto</label>
                    <select name="presupuesto" class="custom-select">
                        <option value="0">Ninguno</option>

<?php                    
foreach ($presupuestos as $presupuesto) {
    // Si la opción de ID del presupuesto coincide con el ID del presupuesto de la
    // transacción seleccionada, lo marcamos por defecto
    if ($presupuesto["id"] == $presupuesto_transaccion["id"]) {
        echo "
                    <option value=\"" . $presupuesto["id"] . "\" selected>" . $presupuesto["nombre"] . "</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $presupuesto["id"] . "\">" . $presupuesto["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                <a href="#" onclick="editar_transaccion();" type="button" class="btn btn-primary" name="enviar">Guardar</a>
            </form>
        </div> <!-- col -->
        <script>
            $.datetimepicker.setLocale('es');
            $("#fecha").datetimepicker({
                timepicker: true,
                dayOfWeekStart: 1,
                format:'Y-m-d H:i:s'
            });
        </script>