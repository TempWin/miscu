<?php 
//
//  Actualiza el importe designado a un presupuesto
//
//  ToDo:
//      * Actualizar el listado de presupuestos al modificar el importe
//  
//  25/02/2019

if (isset($_POST["presupuesto_id"]) && isset($_POST["importe"])) {
    $presupuesto_id = $_POST["presupuesto_id"];
    $importe = str_replace(",", ".", $_POST["importe"]);

    $fecha_actual = date("Y-m-d H:i:s");
    $fecha_ano_mes = date("Y-m");

    require_once("functions.php");

    $conexion = conectar_bd();

    $fecha_inicio = $fecha_ano_mes . "-01";
    $fecha_fin = new DateTime('last day of this month');
    $fecha_fin = $fecha_fin->format("Y-m-d");
    // Buscamos si ya hay un presupuesto para el mes actual
    $buscar_presupuesto_limite_sql = "
        SELECT COUNT(id)
        FROM presupuestos_limites
        WHERE presupuesto_id = :presupuesto_id
          AND fecha_inicio = :fecha_inicio
    ";

    $stmt = $conexion->prepare($buscar_presupuesto_limite_sql);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->bindValue("fecha_inicio", $fecha_inicio);
    $stmt->execute();

    $resultado = $stmt->fetchColumn();

    // Si no existe límites para el presupuesto, lo añadimos
    if ($resultado == 0) {
        $nuevo_limite_presupuesto_sql = "
            INSERT INTO presupuestos_limites (
                presupuesto_id,
                fecha_creacion,
                fecha_actualizacion,
                fecha_inicio,
                fecha_fin,
                importe
            ) VALUES (
                :presupuesto_id,
                :fecha_creacion,
                :fecha_actualizacion,
                :fecha_inicio,
                :fecha_fin,
                :importe
            )
        ";

        $stmt_insertar = $conexion->prepare($nuevo_limite_presupuesto_sql);
        $stmt_insertar->bindValue("presupuesto_id", $presupuesto_id);
        $stmt_insertar->bindValue("fecha_creacion", $fecha_actual);
        $stmt_insertar->bindValue("fecha_actualizacion", $fecha_actual);
        $stmt_insertar->bindValue("fecha_inicio", $fecha_inicio);
        $stmt_insertar->bindValue("fecha_fin", $fecha_fin);
        $stmt_insertar->bindValue("importe", $importe);
        $stmt_insertar->execute();

    } else {
        // Como existe un límite, lo actualizamos con el nuevo importe
        $actualizar_importe_presupuesto_sql = "
            UPDATE presupuestos_limites
            SET fecha_actualizacion = :fecha_actual,
                importe = :importe
            WHERE presupuesto_id = :presupuesto_id
              AND DATE_FORMAT(fecha_inicio, '%Y-%m') = :fecha_ano_mes
        ";

        $stmt_actualizar = $conexion->prepare($actualizar_importe_presupuesto_sql);
        $stmt_actualizar->bindValue("fecha_actual", $fecha_actual);
        $stmt_actualizar->bindValue("importe", $importe);
        $stmt_actualizar->bindValue("presupuesto_id", $presupuesto_id);
        $stmt_actualizar->bindValue("fecha_ano_mes", $fecha_ano_mes);
        $stmt_actualizar->execute();
    }
} else {
    echo "<p>Error actualizando importe de presupuesto</p>";
}

?>