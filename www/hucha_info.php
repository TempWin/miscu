<?php
//
// Información sobre la hucha
//
// En principio solo mostraría información del año actual
//
//
//  26/02/2019
//
session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");

if (isset($_GET["id"])) {
    $hucha_id = $_GET["id"];
    $hucha = info_hucha($hucha_id);
    $hucha_creacion = $hucha["fecha_inicio"];
    $hucha_objetivo = $hucha["importe_objetivo"];
    $hucha_fecha_objetivo = $hucha["fecha_objetivo"];
    $cuenta_id = $hucha["cuenta_id"];
    $cuenta = obtener_cuenta($cuenta_id);
    $ahorrado = obtener_ahorro_hucha($hucha_id);
    $restante = $hucha_objetivo - $ahorrado;
    $hucha_objetivo = number_format($hucha_objetivo, 2, ",", ".");
    $ahorrado = number_format($ahorrado, 2, ",", ".");
    $restante = number_format($restante, 2, ",", ".");
    $movimientos_hucha = movimientos_hucha($hucha_id);

    $error = false;

} else {
    $error = true;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Huchas - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
<?php 
if ($error) {
?>
                        <h1>Hucha</h1>
                        <div class="alert alert-warning" role="alert">
                            Debes seleccionar una <a href="/huchas.php">hucha</a> para consultar su información
                        </div>
<?php 
} else if (empty($hucha)) {
?>
                        <h1>Hucha</h1>
                        <div class="alert alert-warning" role="alert">
                            No existe ninguna hucha con ese identificador.
                        </div>
<?php 
} else {
?>
                        <h1 class="h3"><span class="text-muted">Hucha »</span> <?php echo $hucha["nombre"]; ?></h1>

                        <div class="row">
                            <div class="col-xs-12 col-lg-5">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header">
                                        <h6 class="m-0 font-weight-bold text-primary">Información hucha</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <tbody>
                                                <tr>
                                                    <td>Cuenta</td>
                                                    <td><a href="cuenta_info.php?id=<?php echo $cuenta_id; ?>"><?php echo $cuenta["nombre"]; ?></a></td>
                                                </tr>
                                                <tr>
                                                    <td>Creación</td>
                                                    <td><?php echo (new DateTime($hucha_creacion))->format("d/m/Y"); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Objetivo</td>
                                                    <td><?php echo $hucha_objetivo; ?> €</td>
                                                </tr>
                                                <tr>
                                                    <td>Ahorrado</td>
                                                    <td><?php echo $ahorrado; ?> €</td>
                                                </tr>
                                                <tr>
                                                    <td>Restante</td>
                                                    <td><?php echo $restante; ?> €</td>
                                                </tr>
<?php 
if ($hucha_fecha_objetivo != null) {
?>                            
                                                <tr>
                                                    <td>Fecha objetivo</td>
                                                    <td><?php echo (new DateTime($hucha_fecha_objetivo))->format("d/m/Y"); ?></td>
                                                </tr>                    
<?php 
}
?>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- card -->
                            </div>
<?php 
    if (!empty($movimientos_hucha)) {
?>                            
                            <div class="col-xs-12 col-lg-5">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header">
                                        <h6 class="m-0 font-weight-bold text-primary">Movimientos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Importe</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
        foreach ($movimientos_hucha as $movimiento) {
            $fecha  = (new DateTime($movimiento["fecha"]))->format("d/m/Y");
            $importe = $movimiento["importe"];
            if ($importe < 0) {
                $estilo_importe = "gasto";
            } else {
                $estilo_importe = "";
            }
            $importe = number_format($importe, 2, ",", ".");
            echo "
                                                    <tr>
                                                        <td>" . $fecha . "</td>
                                                        <td><span class=\"" . $estilo_importe . "\">" . $importe . " €</span></td>
                                                    </tr>" . PHP_EOL;
        }
?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- card -->
                            </div>
<?php 
    }
?>                            
                        </div> <!-- row -->
<?php 
}
?>                            
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
    </body>
</html>