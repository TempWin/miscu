<?php
//
// Información sobre la transacción
//
//  ToDo:
//      - Refactorizar código
//
//  17/02/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");

$transaccion_log_id = $_GET["id"];

$transaccion_log = info_transaccion_log($transaccion_log_id);

$fichero = $transaccion_log["fichero"];

// Detalle de la transacción con los movimientos de salida y entrada 
// de dinero (cuenta origen y destino)
$transaccion = info_transaccion($transaccion_log_id);

// Categoría a la que pertenece la transacción
$categoria_transaccion = obtener_categoria_transaccion($transaccion_log_id);
if (empty($categoria_transaccion)) {
    $categoria = "Ninguna";
} else {
    $categoria = $categoria_transaccion["nombre"];
}

// Presupuesto al que está asociada la transacción
$presupuesto_transaccion = obtener_presupuesto_transaccion($transaccion_log_id);
if (empty($presupuesto_transaccion)) {
    $presupuesto = "Ninguno";
} else {
    $presupuesto = $presupuesto_transaccion["nombre"];
}

// Dependiendo del tipo de transacción, daremos un estilo
// diferente al importe
switch ($transaccion_log["tipo_id"]) {
    case 1:
        $estilo = "gasto";
        $importe = $transaccion[$transaccion_log_id]["origen"]["importe"];
        break;
    case 2: 
        $estilo = "ingreso";
        $importe = $transaccion[$transaccion_log_id]["destino"]["importe"];
        break;
    default:
        $estilo = "";
        $importe = $transaccion[$transaccion_log_id]["origen"]["importe"];
}

$fecha_transaccion = new DateTime($transaccion_log["fecha"]);
$fecha_transaccion = $fecha_transaccion->format("d/m/Y H:i");

/*
echo "<pre>" . PHP_EOL;
print_r($movimientos);
echo "</pre>" . PHP_EOL;
*/
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Datetimepicker -->
        <link rel="stylesheet" type="text/css" href="vendor/datetimepicker/jquery.datetimepicker.css">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3"><span class="text-muted">Transacción »</span> <?php echo $transaccion_log["descripcion"]; ?></h1>
                        <p></p>

                        <!-- Resultado de la modificación de la transaccion -->
                        <div class="alert alert-success" role="alert" id="editar-transaccion-resultado" style="display: none;">
                        </div>

                        <!-- Formulario para crear una nueva transaccion de activos -->
                        <div id="editar-transaccion" class="row mb-4 mt-2" style="display: none;" >
                        </div>    

                        <div class="row">
                            <div class="col-xs-12 col-lg-7">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="d-block card-header py-3" role="button" aria-expanded="false" aria-controls="collapseCardExample">
                                        <h6 class="m-0 font-weight-bold text-primary">Información de la transacción</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="transaccion-info">
                                        <table class="table">
                                            <tr>
                                                <td>Tipo</td>
                                                <td><?php echo $transaccion_log["tipo"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Fecha</td>
                                                <td><?php echo $fecha_transaccion; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Cuenta origen</td>
                                                <td><a href="cuenta_info.php?id=<?php echo $transaccion[$transaccion_log_id]["origen"]["cuenta_id"]; ?>"><?php echo $transaccion[$transaccion_log_id]["origen"]["cuenta"] ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>Cuenta destino</td>
                                                <td><a href="cuenta_info.php?id=<?php echo $transaccion[$transaccion_log_id]["destino"]["cuenta_id"]; ?>"><?php echo $transaccion[$transaccion_log_id]["destino"]["cuenta"] ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>Importe</td>
                                                <td><span class="<?php echo $estilo;?>"><?php echo str_replace(".", ",", round($importe, $transaccion_log["decimales"])); ?> <?php echo $transaccion_log["simbolo"] ?></span></td>
                                            </tr>
                                        </table>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button onclick="mostrar_editar_transaccion(<?php echo $transaccion_log_id; ?>);" class="btn btn-primary">Editar</button>
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#eliminar_transaccion">Eliminar</a>
                                        </div>
                                    </div>
                                </div> <!-- card -->
                            </div>
                            <!-- Meta información sobre la transacción -->
                            <div class="col-xs-12 col-lg-5">
                                <div class="row">
<?php 
if ($fichero != null) {
?>                            
                                    <div class="col-lg-12">
                                        <div class="card shadow mb-4">
                                            <!-- Card Header - Dropdown -->
                                            <div class="card-header">
                                                Fichero adjunto
                                            </div>
                                            <!-- Card Body -->
                                            <div class="card-body" id="transaccion-info-meta">
                                                <a href="uploads/<?php echo $fichero; ?>"><img class="fichero-transaccion" src="uploads/<?php echo $fichero; ?>"></a>
                                            </div>
                                        </div> <!-- card -->
                                    </div>
<?php 
}
?>                            
                                    <div class="col-lg-12">
                                        <div class="card shadow mb-4">
                                            <!-- Card Header - Dropdown -->
                                            <div class="card-header">
                                                Información adicional
                                            </div>
                                            <!-- Card Body -->
                                            <div class="card-body" id="transaccion-info-meta">
                                                <table class="table table-bt0">
                                                    <thead>
                                                        <tr>
                                                            <th>Categoría</th>
                                                            <th>Presupuesto</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo $categoria; ?></td>
                                                            <td><?php echo $presupuesto; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div> <!-- card -->
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Modal eliminar transacción-->
        <div class="modal fade" id="eliminar_transaccion" tabindex="-1" role="dialog" aria-labelledby="eliminar_transaccion" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminar_transaccion">Eliminar transacción</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Pulsando en <strong>Eliminar</strong> se borrará la transacción seleccionada. Esta operación no puede deshacerse.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a href="transaccion_eliminar.php?id=<?php echo $transaccion_log_id; ?>" class="btn btn-danger" >Eliminar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Chartjs -->
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <!-- Datetimepicker -->
        <script src="vendor/datetimepicker/build/jquery.datetimepicker.full.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
        
    </body>
</html>