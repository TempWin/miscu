<?php
//  Página de presupuestos
//
//      * Listado de presupuestos
//      * Edición de presupuestos
//    
//  ToDo:
//      * Avisar de que se ha guardado el importe introducido como presupuesto para el mes actual
//

session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$mes_actual = date("n");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Presupuestos - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Presupuestos</h1>
                        <p>Tipo de categoría que se repite habitualmente: facturas, supermercado...</p>
                        <p><button onclick="mostrar_insertar_presupuesto();" class="btn btn-primary">Añadir presupuesto</button></p>
                        <div id="insertar-presupuesto" class="row mb-3 ml-0" style="display: none;" >
                            <form class="form-inline">
                                <input class="form-control" type="text" name="presupuesto_nuevo" id="presupuesto_nuevo" value="" placeholder="Nombre presupuesto">
                                <a class="ml-2" href="#" onclick="nuevo_presupuesto(<?php echo $usuario_id; ?>);"><i class="fas fa-check"></i></a>
                            </form>
                        </div>
                        <div class="alert alert-success" role="alert" id="insertar-presupuesto-resultado" style="display: none;">
                        </div>
<?php 
if (count(listar_presupuestos($usuario_id)) != 0) {
?>                        
                        <div class="row">
                            <!-- Area Chart -->
                            <div class="col-xs-12 col-xl-10">
                                <div class="card shadow">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Listado de presupuestos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="presupuestos-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre presupuesto</th>
                                                    <th>Importe mes actual</th>
                                                    <th>Gastado</th>
                                                    <th>Disponible</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
$presupuestos = listar_presupuestos($usuario_id);
 
foreach ($presupuestos as $presupuesto) {
    $presupuesto_info = presupuesto_mes($presupuesto["id"], date("Y-m"));
    if (empty($presupuesto_info)) {
        $presupuesto_importe = 0;
    } else {
        $presupuesto_importe = $presupuesto_info["importe"];
    }

    $gastado = obtener_gastos_presupuesto($presupuesto["id"], date("Y-m"));
    $restante = $presupuesto_importe - $gastado;
    if ($restante < 0) {
        $estilo_restante = "gasto";
    } else {
        $estilo_restante = "";
    }
    $gastado = number_format($gastado, 2, ",", ".");
    $restante = number_format($restante, 2, ",", ".");
    echo "
                                                <tr>
                                                    <td id=\"prep-" . $presupuesto["id"] . "\"><a href=\"presupuesto_info.php?id=" .$presupuesto["id"] . "\">" . $presupuesto["nombre"] . "</a></td>
                                                    <td>
                                                        <div class=\"input-group\">
                                                            <div class=\"input-group-prepend\">
                                                                <div class=\"input-group-text form-control-sm\">€</div>
                                                            </div>
                                                            <input id=\"presupuesto_importe_id_" . $presupuesto["id"] . "\" value=\"" . $presupuesto_importe . "\" autocomplete=\"off\" min=\"0\" name=\"importe\" type=\"number\" class=\"form-control form-control-sm\"> <a class=\"btn\" href=\"#\" onclick=\"actualizar_importe_presupuesto(" . $presupuesto["id"] . ");\"><i class=\"fas fa-check\"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>" . $gastado . " €</td>
                                                    <td><span class=\"" . $estilo_restante . "\">" . $restante . " €</span></td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $presupuesto["id"] . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_presupuesto(" . $presupuesto["id"] . ");\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"presupuesto_eliminar.php?id=" . $presupuesto["id"] . "\"><i class=\"far fa-trash-alt\"></i></a>
                                                        </div>
                                                    </td>
                                                    <!-- <td id=\"edit-" . $presupuesto["id"] . "\"><a href=\"#\" onclick=\"campo_editable_presupuesto(" . $presupuesto["id"] , ");\"><i class=\"far fa-edit\"></i></a></td>
                                                    <td><a href=\"presupuesto_eliminar.php?id=" . $presupuesto["id"] . "\"><i class=\"far fa-trash-alt\"></i></td> -->
                                                </tr>" . PHP_EOL;
}
echo "
                                            </tbody>
                                        </table>" . PHP_EOL;
?>                                        
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizadso -->
        <script src="js/scripts.js"></script>
    </body>
</html>