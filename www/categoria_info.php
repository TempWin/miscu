<?php
//  Página de transacciones por categorías
//
//  14/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$categoria_id = $_GET["id"];

$categoria = info_categoria($categoria_id);
$movimientos = movimientos_categoria($categoria_id);

$mes_actual = date("n");
$ano_actual = date("Y");
$total_importe_mes = importe_total_categoria($categoria_id, $mes_actual, $ano_actual);
$total_importe_ano = importe_total_categoria($categoria_id, null, $ano_actual);

if ($total_importe_mes < 0) {
    $estilo_total_mes = "negativo";
} else {
    $estilo_total_mes = "";
}

if ($total_importe_ano < 0) {
    $estilo_total_ano = "negativo";
} else {
    $estilo_total_ano = "";
}

$total_importe_mes = number_format(importe_total_categoria($categoria_id, $mes_actual, $ano_actual), 2, ",", ".");
$total_importe_ano = number_format(importe_total_categoria($categoria_id, null, $ano_actual), 2, ",", ".");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Categorías - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800"><span class="text-muted">Categoría »</span> <?php echo $categoria["nombre"]; ?></h1>
                        
<?php 
if (!empty($movimientos)) {
?>                        
                        <div class="row">
                            <div class="col-sm-12 col-lg-8">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Transacciones de la categoría</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="categorias-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>
                                                    <th>Cuenta</th>
                                                    <th>Importe</th>
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($movimientos as $movimiento) {
        $fecha = (new DateTime($movimiento["fecha"]))->format("d/m/Y");
        $descripcion = $movimiento["descripcion"];
        $cuenta_id = $movimiento["cuenta_id"];
        $cuenta = obtener_cuenta($cuenta_id);
        $cuenta = $cuenta["nombre"];
        $importe = $movimiento["importe"];
        $transaccion_log_id = $movimiento["transaccion_log_id"];
        if ($importe < 0) {
            $estilo = "gasto";
        } else {
            $estilo = "";
        }
        $importe = number_format($movimiento["importe"], 2, ",", ".");
        echo "
                                                <tr>
                                                    <td>" . $fecha . "</td>
                                                    <td><a href=\"transaccion_info.php?id=" . $transaccion_log_id . "\">" . $descripcion . "</a></td>
                                                    <td><a href=\"cuenta_info.php?id=" . $cuenta_id . "\">" .$cuenta . "</a></td>
                                                    <td><span class=\"" . $estilo . "\">" . $importe . " €</span></td>
                                                </tr>" . PHP_EOL;
    }
?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-3">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-12 mb-2">
                                        <div class="card border-left-primary shadow py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total mes</div>
                                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><span class="<?php echo $estilo_total_mes; ?>"><?php echo $total_importe_mes; ?> €</span></div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-12">
                                        <div class="card border-left-primary shadow py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total año</div>
                                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><span class="<?php echo $estilo_total_ano; ?>"><?php echo $total_importe_ano; ?> €</span></div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
<?php 
} else {
?>
                        <div class="alert alert-info" role="alert" id="insertar-categoria-resultado">
                            Aún no tienes ninguna transacción en esta categoría.
                        </div>    
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Modal editar categoría-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Chart.js -->
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <!-- Scripts personalizadso -->
        <script src="js/scripts.js"></script>
    </body>
</html>