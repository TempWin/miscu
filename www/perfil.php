<?php
//  Página de perfil del usuario
//
//  27/02/2019
//
session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$usuario_info = usuario_info($usuario_id);
$usuario = $usuario_info["usuario"];
$usuario_nombre = $usuario_info["nombre"];
$usuario_apellidos = $usuario_info["apellidos"];
$usuario_email = $usuario_info["email"];
$usuario_fecha_nacimiento = $usuario_info["fecha_nacimiento"];
$usuario_fecha_login = (new DateTime($usuario_info["fecha_login"]))->format("d/m/Y");
$usuario_fecha_registro = (new DateTime($usuario_info["fecha_registro"]))->format("d/m/Y");

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Datetimepicker -->
        <link rel="stylesheet" type="text/css" href="vendor/datetimepicker/jquery.datetimepicker.css">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800"><span class="text-muted">Perfil »</span> <?php echo $usuario; ?></h1>
                        <div class="row pb-2">
                            <div class="d-md-none d-lg-block col-lg-3">
                                <div class="card shadow mb-4">
                                    <div class="card-body perfil">
                                        <img src="img/avatar.png" class="rounded mx-auto d-block" alt="Avatar">
                                        <p>Miembro desde: <?php echo $usuario_fecha_registro; ?></p>
                                        <p>Última conexión: <?php echo $usuario_fecha_login; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-lg-5">
                                <div class="alert alert-success" role="alert" id="actualizar-perfil-resultado" style="display: none;">
                                </div>
                                <form class="form" id="form-perfil">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="nombre" value="<?php if($usuario_nombre != null) echo $usuario_nombre ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="apellidos">Apellidos</label>
                                        <input type="text" class="form-control" id="apellidos" name="apellidos" aria-describedby="apellidos" value="<?php if($usuario_apellidos != null) echo $usuario_apellidos ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="fecha_nacimiento">Fecha nacimiento</label>
                                        <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" aria-describedby="fecha_nacimiento" value="<?php if($usuario_fecha_nacimiento != null) echo $usuario_fecha_nacimiento; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <input type="text" class="form-control" id="email" name="email" disabled aria-describedby="email" value="<?php echo $usuario_email; ?>">
                                        <input type="hidden" name="usuario_id" id="usuario_id" value="<?php echo $usuario_id; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="clave">Cambiar contraseña</label>
                                        <input type="password" class="form-control" id="clave" name="clave" required aria-describedby="clave">
                                    </div>
                                    <div class="form-group">
                                        <label for="clave_confirmar">Repetir contraseña</label>
                                        <input type="password" class="form-control" id="clave_confirmar" name="clave_confirmar" required aria-describedby="clave_confirmar">
                                    </div>
                                    <a href="#" onclick="editar_perfil();" class="btn btn-primary">Guardar</a>
                                </form>
                            </div>
                        </div> <!-- row -->
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div> <!-- End of Content Wrapper -->
        </div> <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Datetimepicker -->
        <script src="vendor/datetimepicker/build/jquery.datetimepicker.full.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
        <script>
            $.datetimepicker.setLocale('es');
            $("#fecha_nacimiento").datetimepicker({
                timepicker: false,
                dayOfWeekStart: 1,
                format:'Y-m-d'
            });
        </script>
    </body>
</html>