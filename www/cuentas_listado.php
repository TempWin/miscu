<?php
// Listado de las cuentas que tiene un usuario
//
session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

// (1: activos, 2:ingresos, 3: gastos)
$tipo_cuenta_agrupado = $_GET["id"];
switch ($tipo_cuenta_agrupado) {
    case 1:
        $tipo_cuenta = "activos";
        break;
    case 2:
        $tipo_cuenta = "ingresos";
        break;
    default:
        $tipo_cuenta = "gastos";
        break;
}
$cuentas = listar_cuentas($usuario_id, $tipo_cuenta);
$numero_cuentas_gastos = count($cuentas);

?>
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre cuenta</th>
                                                    <th>Última actividad</th>
<?php 
// Solo mostramos el saldo cuando se trate de cuentas de activos
if ($tipo_cuenta == "activos") {
?>                                                    
                                                    <th>Saldo</th>
<?php 
}
?>                                                    
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($cuentas as $id => $datos) {
        $ultimo_movimiento = obtener_ultimo_movimiento($id);
        if (empty($ultimo_movimiento)) {
            $ultima_actividad = "Sin movimientos";
        } else {
            $ultima_actividad = new DateTime($ultimo_movimiento["fecha"]);
            $ultima_actividad = $ultima_actividad->format("Y-m-d");
        }
        echo "
                                                <tr>
                                                    <td><a href=\"cuenta_info.php?id=" . $id . "\">" . $datos["nombre"] . "</a></td>
                                                    <td>" . $ultima_actividad . "</td>" . PHP_EOL;
        // Solo mostramos el saldo cuando se trate de cuentas de activos
        if ($tipo_cuenta == "activos") {                                                    
            $saldo = number_format(obtener_saldo($id), 2, ",", ".");
            echo "
                                                    <td>" . $saldo . " " . $datos["divisa_simbolo"] . "</td>" . PHP_EOL;
        }
        echo "

                                                </tr>" . PHP_EOL;
    }
?>
                                            </tbody>
                                        </table>