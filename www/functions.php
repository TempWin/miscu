<?php
// Fichero con funciones para la administración de la aplicación
//
//  ToDo:
//      - Agrupar por categorías (cuentas, movimientos...)

date_default_timezone_set("Europe/Madrid");

// Crea una conexión con la base de datos usando PDO
function conectar_bd() {
    require_once("config.php");

    $dsn = "mysql:host=" . SERVIDOR_BD . ";dbname=" . NOMBRE_BD . ";charset=" . CHARSET_BD . "";

    $opciones = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false
    ];
    try {
         $pdo = new PDO($dsn, USUARIO_BD, CLAVE_BD, $opciones);
         return $pdo;
    } catch (\PDOException $e) {
         throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
}

function existe_usuario($usuario, $email) {
    $conexion = conectar_bd();
    $buscar_email_sql = "
        SELECT *
        FROM usuarios
        WHERE email = :email
           OR usuario = :usuario
    ";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($buscar_email_sql);
    $stmt->execute([
        "email"   => $email,
        "usuario" => $usuario
    ]);
    $coincidencias = $stmt->fetchColumn(); 

    /*
    echo "<pre>" . PHP_EOL;
    $stmt->debugDumpParams();
    echo "</pre>" . PHP_EOL;
    */

    if ($coincidencias != 0) {
        // Existe el usuario
        return true;
    } else {
        return false;
    }
}

// Crea usuarios
function crear_usuario($usuario, $email, $clave) {
    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");

    // Creamos un hash a partir de esa clave
    // PASSWORD_DEFAULT utiliza el algoritmo bcrypt
    $clave_hash = password_hash($clave, PASSWORD_DEFAULT);

    $insertar_usuario_sql = "
        INSERT INTO usuarios (
            usuario,
            email,
            clave,
            fecha_registro
        ) VALUES (
            :usuario,
            :email,
            :clave,
            '" . $fecha . "'
        )";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($insertar_usuario_sql);
    $stmt->execute([
        'usuario'   => $usuario,
        'email'     => $email, 
        'clave'     => $clave_hash
    ]);

    if (!$stmt) {
        //echo "Error SQL insertar_usuario_sql: " . PHP_EOL;
        return false;
    } else {
        //echo "Usuario creado correctamente" . PHP_EOL;
        return true;
    }    
}

// Comprueba si la clave y correo electrónico son correctos
function login_usuario($email, $clave) {
    
    $conexion = conectar_bd();

    $comprobar_usuario_sql = "
        SELECT id,
               usuario,
               clave
        FROM usuarios
        WHERE email = :email
    ";

    $stmt = $conexion->prepare($comprobar_usuario_sql);
    $stmt->execute(["email" => $email]);

    $usuario = $stmt->fetch();

    $usuario_info = [
        "id" => $usuario["id"], 
        "usuario" => $usuario["usuario"]
    ];
    // Comprobamos si la contraseña introducida por el usuario
    // coincide con el hash almacenado en la base de datos
    if (password_verify($clave, $usuario["clave"])) {
        return $usuario_info;
    } else {
        return false;
    }
}

// Registra en la BD el inicio de sesión del usuario
function registrar_login($usuario_id) {

    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");

    $registrar_login_sql = "
        INSERT INTO usuarios_log (
            usuario_id,
            fecha_login
        ) VALUES (
            " . $usuario_id . ",
            '" . $fecha . "'
        )
    ";

    $stmt = $conexion->query($registrar_login_sql);
}

// A partir del ID del usuario, obtiene toda su información
function usuario_info($id) {

    $conexion = conectar_bd();
    $usuario_info = [];

    $obtener_usuario_sql = "
        SELECT id,
               usuario,
               email,
               fecha_registro,
               MAX(fecha_login) as fecha_login,
               nombre,
               apellidos,
               fecha_nacimiento,
               avatar
        FROM usuarios 
        LEFT JOIN usuarios_log
           ON usuarios.id = usuarios_log.usuario_id
        LEFT JOIN usuarios_info
           ON usuarios.id = usuarios_info.usuario_id
           where id = :usuario_id
    ";

    $stmt = $conexion->prepare($obtener_usuario_sql);
    $stmt->bindValue("usuario_id", $id);
    $stmt->execute();

    foreach ($stmt as $usuario) {
        $usuario_info = [
            "id"                => $usuario["id"],
            "usuario"           => $usuario["usuario"],
            "email"             => $usuario["email"],
            "fecha_registro"    => $usuario["fecha_registro"],
            "fecha_login"       => $usuario["fecha_login"],
            "nombre"            => $usuario["nombre"],
            "apellidos"         => $usuario["apellidos"],
            "fecha_nacimiento"  => $usuario["fecha_nacimiento"],
            "avatar"            => $usuario["avatar"]
        ];
    }

    return $usuario_info;
}

function crear_cuenta($cuenta_tipo_id, $nombre_cuenta, $usuario_id, $iban, $bic, $saldo_inicial, $divisa_id, $descripcion) {

    $conexion = conectar_bd();
    $fecha_creacion = date("Y-m-d H:i:s");

    $insertar_cuenta_sql = "
        INSERT INTO cuentas (
            cuenta_tipo_id,
            nombre,
            usuario_id,
            iban,
            bic,
            fecha_creacion,
            fecha_actualizacion,
            fecha_cierre,
            saldo_inicial,
            divisa_id,
            descripcion
        ) VALUES (
            " . $cuenta_tipo_id . ",
            :nombre_cuenta,
            " . $usuario_id . ",
            :iban,
            :bic,
            '" . $fecha_creacion . "',
            '" . $fecha_creacion . "',
            null,
            :saldo_inicial,
            " . $divisa_id . ",
            :descripcion
        )";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($insertar_cuenta_sql);
    $stmt->execute([
            "nombre_cuenta" => $nombre_cuenta,
            "iban"          => $iban,
            "bic"           => $bic,
            "saldo_inicial" => $saldo_inicial,
            "descripcion"   => $descripcion
        ]);

    if (!$stmt) {
        return false;
    } else {
        return true;
    }
}

function editar_cuenta($cuenta_id, $cuenta_tipo_id, $nombre_cuenta, $iban, $bic, $saldo_inicial, $divisa_id, $descripcion) {

    $conexion = conectar_bd();
    $actualizar_cuenta_sql = "
        UPDATE cuentas 
        SET cuenta_tipo_id = " . $cuenta_tipo_id . ",
            nombre = :nombre_cuenta,
            iban = :iban,
            bic = :bic,
            saldo_inicial = :saldo_inicial,
            divisa_id = " . $divisa_id . ",
            descripcion = :descripcion
        WHERE id = " . $cuenta_id . "";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($actualizar_cuenta_sql);
    $stmt->execute([
            "nombre_cuenta"   => $nombre_cuenta,
            "iban" => $iban,
            "bic" => $bic,
            "saldo_inicial" => $saldo_inicial,
            "descripcion" => $descripcion
        ]);

    /*
    // DEBUG
    $stmt->debugDumpParams();
    */

    if (!$stmt) {
        return false;
    } else {
        return true;
    }
}

// Devuelve toda la información de una cuenta
function info_cuenta($cuenta_id) {
    $conexion = conectar_bd();
    $obtener_info_cuenta_sql = "
        SELECT c.id as cuenta_id,
               c.cuenta_tipo_id,
               t.nombre as tipo,
               c.nombre,
               c.usuario_id,
               c.iban,
               c.bic,
               DATE_FORMAT(c.fecha_creacion, '%Y-%m-%d') as fecha_creacion,
               c.fecha_cierre,
               c.saldo_inicial,
               c.divisa_id,
               d.nombre as divisa,
               d.simbolo as divisa_simbolo,
               d.decimales,
               c.descripcion,
               DATE_FORMAT(MAX(tran_log.fecha), '%Y-%m-%d') as fecha_ultimo_movimiento
        FROM cuentas c
        INNER JOIN cuentas_tipos t
           ON c.cuenta_tipo_id = t.id
        INNER JOIN divisas d
           ON c.divisa_id = d.id
        inner join transacciones tran
           on c.id = tran.cuenta_id
        inner join transacciones_log tran_log
           on tran_log.id = tran.transaccion_log_id
        WHERE c.id = " . $cuenta_id . "";

    $stmt = $conexion->query($obtener_info_cuenta_sql);
    // DEBUG
    //$stmt->debugDumpParams();

    $cuenta = $stmt->fetch();

    $cuenta_info = [
        "cuenta_id"                 => $cuenta["cuenta_id"],
        "cuenta_tipo_id"            => $cuenta["cuenta_tipo_id"],
        "tipo"                      => $cuenta["tipo"],
        "nombre"                    => $cuenta["nombre"],
        "iban"                      => $cuenta["iban"],
        "bic"                       => $cuenta["bic"],
        "fecha_creacion"            => $cuenta["fecha_creacion"],
        "fecha_cierre"              => $cuenta["fecha_cierre"],
        "saldo_inicial"             => $cuenta["saldo_inicial"],
        "divisa_id"                 => $cuenta["divisa_id"],
        "divisa"                    => $cuenta["divisa"],
        "divisa_simbolo"            => $cuenta["divisa_simbolo"],
        "decimales"                 => $cuenta["decimales"],
        "descripcion"               => $cuenta["descripcion"],
        "fecha_ultimo_movimiento"   => $cuenta["fecha_ultimo_movimiento"]
    ];

    return $cuenta_info;
}



// Muestra un listado de las cuentas del usuario
function listar_cuentas($usuario_id, $tipo_cuenta=null) {

    // Si no le indicamos el tipo de cuenta, buscaremos
    // todas las cuentas
    switch ($tipo_cuenta) {
        case null:
            $filtro_tipo_cuenta = "";
            break;
        case "activos":
            $filtro_tipo_cuenta = " AND c.cuenta_tipo_id IN (1, 2, 3)";
            break;
        case "gastos":
            $filtro_tipo_cuenta = " AND c.cuenta_tipo_id = 4";
            break;
        // Cuenta de ingresos:
        default:
            $filtro_tipo_cuenta = " AND c.cuenta_tipo_id = 5";
            break;
    }

    $conexion = conectar_bd();
    $listado_cuentas_sql = "
        SELECT c.id as cuenta_id,
               c.cuenta_tipo_id,
               t.nombre as tipo,
               c.nombre,
               c.iban,
               c.bic,
               c.fecha_creacion,
               c.divisa_id,
               d.nombre as divisa_nombre,
               d.simbolo as divisa_simbolo,
               c.descripcion
        FROM cuentas c
        INNER JOIN cuentas_tipos t
           ON c.cuenta_tipo_id = t.id
        INNER JOIN divisas d
           ON c.divisa_id = d.id
        WHERE c.usuario_id = " . $usuario_id . "
        " . $filtro_tipo_cuenta . "
        ORDER BY c.nombre
    ";

    $cuentas = $conexion->query($listado_cuentas_sql);

    $listado_cuentas = [];

    foreach ($cuentas as $cuenta) {
        $listado_cuentas[$cuenta["cuenta_id"]] = [
            "tipo_id"           => $cuenta["cuenta_tipo_id"],
            "tipo"              => $cuenta["tipo"],
            "nombre"            => $cuenta["nombre"],
            "iban"              => $cuenta["iban"],
            "bic"               => $cuenta["bic"],
            "fecha_creacion"    => $cuenta["fecha_creacion"],
            "divisa_id"         => $cuenta["divisa_id"],
            "divisa_nombre"     => $cuenta["divisa_nombre"],
            "divisa_simbolo"    => $cuenta["divisa_simbolo"],
            "descripcion"       => $cuenta["descripcion"]
        ];
    }

    return $listado_cuentas;
}

// Crea categoría
function crear_categoria($usuario_id, $nombre_categoria) {
    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");

    $crear_categoria_sql = "
        INSERT INTO categorias (
            nombre,
            usuario_id,
            fecha_creacion,
            fecha_modificacion
        ) VALUES (
            :nombre_categoria,
            " . $usuario_id . ",
            '" . $fecha . "',
            '" . $fecha . "'
        )
    ";

    $stmt = $conexion->prepare($crear_categoria_sql);
    $stmt->execute([
        "nombre_categoria" => $nombre_categoria
    ]);

    if (!$stmt) {
        return false;
    } else {
        return true;
    }
}

// Crea presupuesto
function crear_presupuesto($usuario_id, $nombre_presupuesto, $descripcion=null) {
    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");

    $crear_presupuesto_sql = "
        INSERT INTO presupuestos (
            nombre,
            usuario_id,
            fecha_creacion,
            fecha_actualizacion,
            descripcion
        ) VALUES (
            :nombre_presupuesto,
            " . $usuario_id . ",
            '" . $fecha . "',
            '" . $fecha . "',
            :descripcion
        )
    ";

    $stmt = $conexion->prepare($crear_presupuesto_sql);
    $stmt->execute([
        "nombre_presupuesto" => $nombre_presupuesto,
        "descripcion" => $descripcion
    ]);

    // Obtenemos el ID del presupuestos que acabamos de crear
    $presupuesto_id = $conexion->lastInsertId();
    $fecha_actual = date("Y-m-d H:i:s");
    // Primer día del mes actual
    $f_inicio = new DateTime('first day of this month');
    $f_fin = new DateTime('last day of this month');
    $fecha_inicio = $f_inicio->format("Y-m-d");
    // Último día del mes actual
    $fecha_fin = $f_fin->format("Y-m-d");
    $importe = 0;

    $presupuesto_limite_sql = "
        INSERT INTO presupuestos_limites (
            presupuesto_id,
            fecha_creacion,
            fecha_actualizacion,
            fecha_inicio,
            fecha_fin,
            importe
        ) VALUES (
            :presupuesto_id,
            :fecha_creacion,
            :fecha_actualizacion,
            :fecha_inicio,
            :fecha_fin,
            :importe
        )
    ";

    $stmt_limite = $conexion->prepare($presupuesto_limite_sql);
    $stmt_limite->bindValue("presupuesto_id", $presupuesto_id);
    $stmt_limite->bindValue("fecha_creacion", $fecha_actual);
    $stmt_limite->bindValue("fecha_actualizacion", $fecha_actual);
    $stmt_limite->bindValue("fecha_inicio", $fecha_inicio);
    $stmt_limite->bindValue("fecha_fin", $fecha_fin);
    $stmt_limite->bindValue("importe", $importe);
    $stmt_limite->execute();

    if (!$stmt_limite) {
        return false;
    } else {
        return true;
    }
}

// Muestra un listado de las categorías del usuario
function listar_categorias($usuario_id) {
    $conexion = conectar_bd();
    $listado_categorias_sql = "
        SELECT id,
               nombre
        FROM categorias
        WHERE usuario_id = " . $usuario_id . "
        ORDER BY nombre
    ";

    $categorias = $conexion->query($listado_categorias_sql);

    $listado_categorias = [];

    foreach ($categorias as $categoria) {
        $listado_categorias[] = [
            "id"        => $categoria["id"],
            "nombre"    => $categoria["nombre"]
        ];
    }

    return $listado_categorias;
}

// Muestra un listado de los presupuestos del usuario
function listar_presupuestos($usuario_id) {
    $conexion = conectar_bd();
    $listado_presupuestos_sql = "
        SELECT id,
               nombre
        FROM presupuestos
        WHERE usuario_id = " . $usuario_id . "
        ORDER BY nombre
    ";

    $presupuestos = $conexion->query($listado_presupuestos_sql);

    $listado_presupuestos = [];

    foreach ($presupuestos as $presupuesto) {
        $listado_presupuestos[] = [
            "id"        => $presupuesto["id"],
            "nombre"    => $presupuesto["nombre"]
        ];
    }

    return $listado_presupuestos;
}

// Devuelve toda la información de una categoría
function info_categoria($categoria_id) {
    $conexion = conectar_bd();
    $obtener_info_categoria_sql = "
        SELECT id,
               nombre
        FROM categorias
        WHERE id = " . $categoria_id . "";

    $stmt = $conexion->query($obtener_info_categoria_sql);
    // DEBUG
    //$stmt->debugDumpParams();

    $categoria = $stmt->fetch();

    $categoria_info = [
        "categoria_id"  => $categoria["id"],
        "nombre"        => $categoria["nombre"]
    ];

    return $categoria_info;
}

function editar_categoria($categoria_id, $nombre) {

    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");
    $actualizar_categoria_sql = "
        UPDATE categorias 
        SET nombre = :nombre_categoria,
            fecha_modificacion = '" . $fecha . "'
        WHERE id = " . $categoria_id . "";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($actualizar_categoria_sql);
    $stmt->execute([
            "nombre_categoria"   => $nombre
        ]);

    if (!$stmt) {
        return false;
    } else {
        return true;
    }
}

// Crea transacciones
//  - Gastos
//  - Ingresos
//  - Transferencias
function crear_transaccion($tipo, $usuario_id, $cuenta_origen, $cuenta_destino, $importe, $divisa, $importe_extranjero, $divisa_extranjera, $fecha, $descripcion, $presupuesto_id, $categoria_id, $fichero) {

    $fecha_actual = date("Y-m-d H:i:s");

    $importe_origen = "-" . $importe;
    $importe_destino = $importe;
    if ($importe_extranjero != null) {
        $importe_extranjero = "-" . $importe_extranjero;
    }

    $conexion = conectar_bd();
    //$fecha_creacion = date("Y-m-d H:i:s");
    $error = true;

    $insertar_transaccion_log_sql =  "
        INSERT INTO transacciones_log (
            usuario_id,
            fecha,
            fecha_creacion,
            fecha_actualizacion,
            tipo_id,
            divisa_id,
            descripcion
        ) VALUES (
            " . $usuario_id . ",
            :fecha,
            '" . $fecha_actual . "',
            '" . $fecha_actual . "',
            " . $tipo . ",
            " . $divisa . ",
            :descripcion
        )
    ";

    $stmt = $conexion->prepare($insertar_transaccion_log_sql);
    $stmt->bindValue('fecha', $fecha);
    $stmt->bindValue('descripcion', $descripcion);
    $stmt->execute();

    // Obtenemos el ID de la transacción para usarlo en las dos
    // transacciones que se generarán (entrada y salida de dinero)
    $transaccion_log_id = $conexion->lastInsertId();

    // Inserción de gasto en cuenta origen
    // Si no se introduce moneda extranjera
    if ($importe_extranjero == null) {
        $insertar_transaccion_cuenta_origen_sql = "
            INSERT INTO transacciones (
                fecha_creacion,
                fecha_actualizacion,
                cuenta_id,
                transaccion_log_id,
                importe,
                divisa_id
            ) VALUES (
                '" . $fecha_actual . "',
                '" . $fecha_actual . "',
                " . $cuenta_origen . ",
                " . $transaccion_log_id . ",
                :importe_origen,
                " . $divisa . "
            )
        ";
   
        $stmt_transaccion_origen = $conexion->prepare($insertar_transaccion_cuenta_origen_sql);
        $stmt_transaccion_origen->bindValue('importe_origen', $importe_origen);
        $stmt_transaccion_origen->execute();

        // Inserción de ingreso en cuenta destino 
        $insertar_transaccion_cuenta_destino_sql = "
            INSERT INTO transacciones (
                fecha_creacion,
                fecha_actualizacion,
                cuenta_id,
                transaccion_log_id,
                importe,
                divisa_id
            ) VALUES (
                '" . $fecha_actual . "',
                '" . $fecha_actual . "',
                " . $cuenta_destino . ",
                " . $transaccion_log_id . ",
                :importe,
                " . $divisa . "
            )
        ";

        $stmt_transaccion_destino = $conexion->prepare($insertar_transaccion_cuenta_destino_sql);
        $stmt_transaccion_destino->bindValue('importe', $importe);
        $stmt_transaccion_destino->execute();

    } else {

        // Si se introduce moneda extranjera
        $insertar_transaccion_cuenta_origen_sql = "
            INSERT INTO transacciones (
                fecha_creacion,
                fecha_actualizacion,
                cuenta_id,
                transaccion_log_id,
                importe,
                divisa_id,
                importe_extranjero,
                divisa_extranjera_id
            ) VALUES (
                '" . $fecha_actual . "',
                '" . $fecha_actual . "',
                " . $cuenta_origen . ",
                " . $transaccion_log_id . ",
                :importe_origen,
                " . $divisa . ",
                :importe_extranjero_origen,
                " . $divisa_extranjera . "
            )
        ";

        $stmt_transaccion_origen = $conexion->prepare($insertar_transaccion_cuenta_origen_sql);
        $stmt_transaccion_origen->bindValue('importe_origen', $importe_origen);
        $stmt_transaccion_origen->bindValue('importe_extranjero_origen', $importe_extranjero_origen);
        $stmt_transaccion_origen->execute();

        // Inserción de ingreso en cuenta destino
        $insertar_transaccion_cuenta_destino_sql = "
            INSERT INTO transacciones (
                fecha_creacion,
                fecha_actualizacion,
                cuenta_id,
                transaccion_log_id,
                importe,
                divisa_id,
                importe_extranjero,
                divisa_extranjera_id
            ) VALUES (
                '" . $fecha_actual . "',
                '" . $fecha_actual . "',
                " . $cuenta_destino . ",
                " . $transaccion_log_id . ",
                :importe,
                " . $divisa . ",
                :importe_extranjero,
                " . $divisa_extranjera . "
            )
        ";

        $stmt_transaccion_destino = $conexion->prepare($insertar_transaccion_cuenta_destino_sql);
        $stmt_transaccion_destino->bindValue('importe', $importe);
        $stmt_transaccion_destino->bindValue('importe_extranjero', $importe_extranjero);
        $stmt_transaccion_destino->execute();
    }

    if ($categoria_id != 0) {
        // Categorizamos la transacción
        $categorizar_transaccion_sql = "
            INSERT INTO categorias_transacciones (
                categoria_id,
                transaccion_log_id
            ) VALUES (
                :categoria_id,
                :transaccion_log_id
            )
        ";

        $stmt_categoria_nueva = $conexion->prepare($categorizar_transaccion_sql);
        $stmt_categoria_nueva->bindValue("categoria_id", $categoria_id);
        $stmt_categoria_nueva->bindValue("transaccion_log_id", $transaccion_log_id);
        $stmt_categoria_nueva->execute();
    }

    // Actualizamos el presupuesto de la transacción
    if ($presupuesto_id != 0) {
    // Comprobamos si la transacción tiene algún presupuesto
        $presupuesto = obtener_presupuesto_transaccion($transaccion_log_id);

        // Metemos la transacción en el presupuesto
        $presupuesto_transaccion_sql = "
            INSERT INTO presupuestos_transacciones (
                presupuesto_id,
                transaccion_log_id
            ) VALUES (
                :presupuesto_id,
                :transaccion_log_id
            )
        ";

        $stmt_presupuesto_nuevo = $conexion->prepare($presupuesto_transaccion_sql);
        $stmt_presupuesto_nuevo->bindValue("presupuesto_id", $presupuesto_id);
        $stmt_presupuesto_nuevo->bindValue("transaccion_log_id", $transaccion_log_id);
        $stmt_presupuesto_nuevo->execute();
    }

    // Subir fichero
    /*
    [_FILES] => Array
        (
            [fichero] => Array
                (
                    [name] => factura_acumasmtc_2018.png
                    [type] => image/png
                    [tmp_name] => C:\xampp\tmp\phpCC4D.tmp
                    [error] => 0
                    [size] => 49311
                )

        )
    */
    if ($fichero["fichero"]["name"] != "") {
        $directorio_subidas = __DIR__ . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR. $usuario_id;
        if (!is_dir($directorio_subidas)) {
            mkdir($directorio_subidas, 0700);
        }

        $fichero_temporal = $fichero["fichero"]["tmp_name"];
        $fichero_nombre = $fichero["fichero"]["name"];
        $fichero_tamano = $fichero["fichero"]["size"];
        $fichero_destino = $directorio_subidas . DIRECTORY_SEPARATOR . $fichero_nombre;
        
        if (move_uploaded_file($fichero_temporal, $fichero_destino)) {
            echo "Subido";
            // Lo registramos en la base de datos
            $insertar_fichero_sql = "
                INSERT INTO transacciones_archivos (
                    transaccion_log_id,
                    nombre_fichero,
                    ruta_fichero,
                    tamano_fichero
                ) VALUES (
                    :transaccion_log_id,
                    :nombre_fichero,
                    :ruta_fichero,
                    :tamano_fichero
                )
            ";

            $stmt_fichero = $conexion->prepare($insertar_fichero_sql);
            $stmt_fichero->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_fichero->bindValue("nombre_fichero", $fichero_nombre);
            $stmt_fichero->bindValue("ruta_fichero", $fichero_destino);
            $stmt_fichero->bindValue("tamano_fichero", $fichero_tamano);
            $stmt_fichero->execute();

        } else {
            echo "Error al subir: " . $fichero["fichero"]["error"];
            exit();
        }
    }

    if ($stmt) {
        return true;
    } else {
        return false;
    }
}

// Elimina una transacción
//
//  $transaccion_log_id: identificador de la transacción
//
function eliminar_transaccion($transaccion_log_id) {
    $conexion = conectar_bd();

    // La tabla relacionada "transacciones" también eliminará
    // los registros que hacen referencia a ese identificador
    // ya que las claves primarias tienen restricción de CASCADE
    // ON DELETE
    $eliminar_transaccion_sql = "
        DELETE
        FROM transacciones_log
        WHERE id = :transaccion_log_id
    ";

    $stmt = $conexion->prepare($eliminar_transaccion_sql);
    $stmt->bindValue("transaccion_log_id", $transaccion_log_id);
    $stmt->execute();

    $registros_borrados = $stmt->rowCount();

    if ($registros_borrados == 0) {
        return false;
    } else {
        return true;
    }
}

// Elimina una cuenta
// 
//  $cuenta_id: identificador de la cuenta que se borrará
//
function eliminar_cuenta($cuenta_id) {

    $conexion = conectar_bd();
    
    // Primero tenemos que buscar las transacciones que se verán afectadas
    // a la hora de eliminar la cuenta porque ON DELETE CASCADE solo afectarä
    // a las transacciones que están relacionadas con la cuenta a eliminar,
    // pero tendremos que buscar también la otra transacción a la cuenta 
    $transacciones_cuenta_eliminada_sql = "
        SELECT t.transaccion_log_id
        FROM transacciones t
        WHERE t.cuenta_id = :cuenta_id
    ";

    $transacciones_a_borrar = [];

    $stmt = $conexion->prepare($transacciones_cuenta_eliminada_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    foreach ($stmt as $transaccion) {
        $transacciones_a_borrar[] = $transaccion["transaccion_log_id"];
    }

    foreach ($transacciones_a_borrar as $id) {
        eliminar_transaccion($id);
    }

    $eliminar_cuenta_sql = "
        DELETE
        FROM cuentas
        WHERE id = :cuenta_id
    ";

    $stmt_eliminar = $conexion->prepare($eliminar_cuenta_sql);
    $stmt_eliminar->bindValue("cuenta_id", $cuenta_id);
    $stmt_eliminar->execute();

    $registros_borrados = $stmt_eliminar->rowCount();

    if ($registros_borrados == 0) {
        return false;
    } else {
        return true;
    }
}

// Elimina una categoría
function eliminar_categoria($categoria_id) {
    $conexion = conectar_bd();

    $eliminar_categoria_sql = "
        DELETE
        FROM categorias
        WHERE id = :categoria_id
    ";

    $stmt = $conexion->prepare($eliminar_categoria_sql);
    $stmt->bindValue("categoria_id", $categoria_id);
    $stmt->execute();

    $registros_borrados = $stmt->rowCount();

    if ($registros_borrados == 0) {
        return false;
    } else {
        return true;
    }
}

// Elimina un presupuesto
function eliminar_presupuesto($presupuesto_id) {
    $conexion = conectar_bd();

    $eliminar_presupuesto_sql = "
        DELETE
        FROM presupuestos
        WHERE id = :presupuesto_id
    ";

    $stmt = $conexion->prepare($eliminar_presupuesto_sql);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->execute();

    $registros_borrados = $stmt->rowCount();

    if ($registros_borrados == 0) {
        return false;
    } else {
        return true;
    }
}

function ultimos_movimientos_cuentas($usuario_id) {
    $conexion = conectar_bd();
    // Array que contendrá todos los movimientos de la cuenta
    $movimientos = [];
    
    $movimientos_cuenta_sql = "
        SELECT t.id, 
               DATE_FORMAT(l.fecha, '%Y-%m-%d') AS fecha,
               tt.tipo,
               tt.id as tipo_id,
               l.descripcion,
               l.id as log_id,
               c.nombre AS cuenta,
               c.id AS cuenta_id,
               t.importe,
               d.simbolo as divisa,
               d.decimales as divisa_decimales
        FROM transacciones t
        INNER JOIN transacciones_log l
           ON t.transaccion_log_id = l.id
        INNER JOIN transacciones_tipos tt
           ON l.tipo_id = tt.id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id
        inner join divisas d
           on t.divisa_id = d.id
        WHERE c.cuenta_tipo_id IN (1, 2, 3) -- cuentas de activos
          AND l.usuario_id = " . $usuario_id . "
        ORDER BY l.fecha DESC
        LIMIT 10
    ";

    $stmt = $conexion->query($movimientos_cuenta_sql);

    foreach ($stmt as $movimiento) {
        $movimientos[$movimiento["id"]] = [
            "fecha"             => $movimiento["fecha"],
            "tipo"              => $movimiento["tipo"],
            "tipo_id"           => $movimiento["tipo_id"],
            "descripcion"       => $movimiento["descripcion"],
            "log_id"            => $movimiento["log_id"],
            "cuenta"            => $movimiento["cuenta"],
            "cuenta_id"         => $movimiento["cuenta_id"],
            "importe"           => $movimiento["importe"],
            "divisa"            => $movimiento["divisa"],
            "divisa_decimales"  => $movimiento["divisa_decimales"]
        ];
    }

    return $movimientos;

}

// Obtiene el importe por categorías atendiendo al tipo (ingreso o gasto)
//
//  $usuario_id: ID del usuario
//  $tipo: tipo de la categoría (gastos o ingresos)
function importe_categorias($usuario_id, $tipo, $ano=null, $mes=null) {
    $conexion = conectar_bd();

    $importe_categorias = [];

    if ($tipo == "gastos") {
        $filtro_tipo = " AND t.importe < 0";
    } else {
        $filtro_tipo = " AND t.importe > 0";
    }

    if ($ano != null) {
        $filtro_ano = " AND YEAR(tl.fecha) = :ano";
    } else {
        $filtro_ano = "";
    }

    if ($mes != null) {
        $filtro_mes = " AND MONTH(tl.fecha) = :mes";
    } else {
        $filtro_mes = "";
    }

    $importe_categorias_sql = "
        SELECT CASE
                   WHEN ct.categoria_id IS NULL THEN 0
                   ELSE ct.categoria_id
               END as categoria_id,
               cat.nombre as categoria,
               SUM(t.importe) as importe
        FROM transacciones_log tl
        LEFT JOIN categorias_transacciones ct
          ON tl.id = ct.transaccion_log_id
        INNER JOIN transacciones t
           ON tl.id = t.transaccion_log_id
        INNER join cuentas c
           ON t.cuenta_id  = c.id
           AND c.cuenta_tipo_id IN (1, 2, 3) -- cuentas de activos
        LEFT JOIN categorias cat
           ON ct.categoria_id = cat.id           
        WHERE tl.usuario_id = :usuario_id
          " . $filtro_tipo . "
          " . $filtro_ano . "
          " . $filtro_mes . "
        GROUP BY ct.categoria_id
    ";

    $stmt = $conexion->prepare($importe_categorias_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->bindValue("ano", $ano);
    $stmt->bindValue("mes", $mes);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $importe_categorias[$datos["categoria_id"]] = [
            "nombre"    => $datos["categoria"],
            "importe"   => $datos["importe"]
        ];
    }

    return $importe_categorias;
}

// Devuelve el importe total de las transacciones de una categoría
function importe_total_categoria($categoria_id, $mes=null, $ano=null) {

    $conexion = conectar_bd();

    $importe = 0;

    if ($mes == null) {
        $filtro_mes = "";
    } else {
        $filtro_mes = " \nAND MONTH(tl.fecha) = " . $mes;
    }

    if ($ano == null) { 
        $filtro_ano = "";
    } else {
        $filtro_ano = " \nAND YEAR(tl.fecha) = " . $ano;
    }

    $importe_categoria_sql = "
        SELECT SUM(t.importe) as total
        FROM categorias_transacciones ct
        INNER JOIN transacciones_log tl
           ON ct.transaccion_log_id = tl.id
        INNER JOIN transacciones t
           ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id
        INNER JOIN cuentas_tipos ctip
           ON c.cuenta_tipo_id = ctip.id
          AND ctip.id IN (1, 2, 3)     
        WHERE ct.categoria_id = :categoria_id
          $filtro_ano
          $filtro_mes
    ";

    $stmt = $conexion->prepare($importe_categoria_sql);
    $stmt->bindValue("categoria_id", $categoria_id);
    $stmt->execute();

    $importe = $stmt->fetchColumn(); 

    return $importe;
    
}

// Obtiene el total de transacciones agrupadas por categorías
function transacciones_categorias($usuario_id) {

    $conexion = conectar_bd();
    $transacciones_categorias = [];

    $transacciones_por_categoria_sql = "
        SELECT cat.nombre as categoria_nombre,
               cat.id as categoria_id,
               COUNT(t.transaccion_log_id) as total_transacciones
        FROM transacciones t
        INNER join transacciones_log tl
           on t.transaccion_log_id = tl.id
        LEFT join categorias_transacciones ct
           on ct.transaccion_log_id = tl.id
        LEFT join categorias cat 
           on ct.categoria_id = cat.id
        WHERE tl.usuario_id = " . $usuario_id . "   
        GROUP by cat.id
        ORDER by COUNT(t.transaccion_log_id) DESC
    ";

    $stmt = $conexion->query($transacciones_por_categoria_sql);

    foreach ($stmt as $datos) {
        $transacciones_categorias[$datos["categoria_nombre"]] = $datos["total_transacciones"];
    }

    return $transacciones_categorias;

}

// Devuelve el saldo actual de todas las cuentas de activos
// del usuario
function obtener_estado_cuentas($usuario_id) {

    $conexion = conectar_bd();

    $info_cuentas = [];

    $estado_cuentas_sql = "
        SELECT c.id,
               DATE_FORMAT(tl.fecha, '%Y-%m') as fecha,
               c.nombre as cuenta,
               c.cuenta_tipo_id as tipo_id,
               ct.nombre as tipo,
               (c.saldo_inicial + SUM(t.importe)) AS saldo,
               d.simbolo as divisa,
               d.decimales as divisa_decimales
        FROM cuentas c
        INNER JOIN transacciones t
           ON c.id = t.cuenta_id
        inner join cuentas_tipos ct
           on c.cuenta_tipo_id = ct.id   
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id   
        inner join divisas d
           on t.divisa_id = d.id   
        WHERE c.cuenta_tipo_id in (1, 2, 3) -- Cuentas de activos (corrientes, ahorros y efectivo)
          and c.usuario_id = :usuario_id
          and DATE_FORMAT(tl.fecha, '%Y-%m') = DATE_FORMAT(CURDATE(), '%Y-%m')  
        group by c.id 
    ";

    $stmt = $conexion->prepare($estado_cuentas_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();
    foreach ($stmt as $datos) {
        $info_cuentas[$datos["id"]] = [
            "fecha"             => $datos["fecha"],
            "cuenta"            => $datos["cuenta"],
            "tipo_id"           => $datos["tipo_id"],
            "tipo"              => $datos["tipo"],
            "saldo"             => $datos["saldo"],
            "divisa"            => $datos["divisa"],
            "divisa_decimales"  => $datos["divisa_decimales"]
        ];
    }

    return $info_cuentas;
}

// Devuelve el saldo actual de una cuenta
function obtener_saldo($cuenta_id) {

    $conexion = conectar_bd();

    $saldo = 0.0;

    $saldo_cuenta_sql = "
        SELECT 
            CASE 
                WHEN (c.saldo_inicial + SUM(t.importe)) IS NULL THEN c.saldo_inicial -- Cuando no hay transacciones (cuenta nueva)
                ELSE (c.saldo_inicial + SUM(t.importe))
            END AS saldo
        FROM cuentas c
        INNER JOIN transacciones t
           ON c.id = t.cuenta_id
        inner join cuentas_tipos ct
           on c.cuenta_tipo_id = ct.id   
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id   
        WHERE c.id = :cuenta_id
          -- and DATE_FORMAT(tl.fecha, '%Y-%m') = DATE_FORMAT(CURDATE(), '%Y-%m')  
    ";

    $stmt = $conexion->prepare($saldo_cuenta_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    // Como solo devuelve un registro:
    $saldo = $stmt->fetchColumn(); 

    return $saldo;
}

// Devuelve el saldo inicial de una cuenta
//
function obtener_saldo_inicial($cuenta_id) {
    $conexion = conectar_bd();

    $saldo_inicial = 0.0;

    $saldo_inicial_cuenta_sql = "
        SELECT saldo_inicial
        FROM cuentas
        WHERE id = :cuenta_id
    ";

    $stmt = $conexion->prepare($saldo_inicial_cuenta_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    // Como solo devuelve un registro:
    $saldo_inicial = $stmt->fetchColumn(); 

    return $saldo_inicial;
}

// Devuelve el saldo en una determinada fecha
function obtener_saldo_fecha($cuenta_id, $fecha) {
    $conexion = conectar_bd();

    $saldo = 0.0;

    $saldo_fecha_sql = "
        SELECT (c.saldo_inicial + SUM(t.importe)) as saldo_disponible
        FROM transacciones_log tl
        INNER JOIN transacciones t
          ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
          ON t.cuenta_id = c.id  
        WHERE tl.fecha <= :fecha
          AND t.cuenta_id = :cuenta_id
    ";

    $stmt = $conexion->prepare($saldo_fecha_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->bindValue("fecha", $fecha);
    $stmt->execute();

    // Como solo devuelve un registro:
    $saldo = $stmt->fetchColumn(); 

    return $saldo;
}

// Devuelve la información del último movimiento de la cuenta
function obtener_ultimo_movimiento($cuenta_id) {
    $conexion = conectar_bd();

    $ultimo_movimiento = [];

    $ultimo_movimiento_sql = "
        SELECT t.id,
               tl.fecha,
               tl.descripcion,
               t.importe
        FROM cuentas c
        LEFT JOIN transacciones t
          ON c.id = t.cuenta_id
        INNER JOIN transacciones_log tl
          ON t.transaccion_log_id = tl.id
        WHERE c.id = :cuenta_id
        ORDER BY 2 desc
        LIMIT 1
    ";

    $stmt = $conexion->prepare($ultimo_movimiento_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $ultimo_movimiento = [
            "id"            => $datos["id"],
            "fecha"         => $datos["fecha"],
            "descripcion"   => $datos["descripcion"],
            "importe"       => $datos["importe"]
        ];
    }

    return $ultimo_movimiento;
}

// Devuelve las divisas que hay en la base de datos
function listado_divisas() {
    $conexion = conectar_bd();
    $divisas = [];

    $divisas_sql = "
        SELECT id,
               nombre,
               codigo,
               simbolo,
               decimales
        FROM divisas
        ORDER BY nombre
    ";

    $stmt = $conexion->prepare($divisas_sql);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $divisas[$datos["id"]] = [
            "nombre"    => $datos["nombre"],
            "codigo"    => $datos["codigo"],
            "simbolo"   => $datos["simbolo"],
            "decimales" => $datos["decimales"]
        ];
    }

    return $divisas;

}

// 
//  Obtiene los detalles de una transacción
//
//      $log_id: identificador del apunte de la transacción
//
function info_transaccion($log_id) {
    $conexion = conectar_bd();
    $transaccion = [];

    $transaccion_sql = "
        SELECT t.id,
               tl.fecha,
               t.importe,
               tl.descripcion,
               c.id as cuenta_id,
               c.nombre as cuenta,
               t.transaccion_log_id as log_id
        FROM transacciones t
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id  
        WHERE tl.id = :log_id
    ";

    $stmt = $conexion->prepare($transaccion_sql);
    $stmt->bindValue("log_id", $log_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        // Cuenta origen
        if ($datos["importe"] < 0) {
                $transaccion[$log_id]["origen"] = [
                "id"            => $datos["id"],
                "fecha"         => $datos["fecha"],
                "importe"       => $datos["importe"],
                "cuenta_id"     => $datos["cuenta_id"],
                "cuenta"        => $datos["cuenta"]
            ];
        } else {
            // Cuenta destino
            $transaccion[$log_id]["destino"] = [
                "id"            => $datos["id"],
                "fecha"         => $datos["fecha"],
                "importe"       => $datos["importe"],
                "cuenta_id"     => $datos["cuenta_id"],
                "cuenta"        => $datos["cuenta"]
            ];
        }
    }    

    return $transaccion;

}

function info_transaccion_log($log_id) {

    $conexion = conectar_bd();
    $transaccion_log = [];

    $transaccion_log_sql = "
        SELECT tl.id as log_id,
               tl.fecha as fecha,
               tl.tipo_id,
               tt.tipo,
               tl.divisa_id,
               d.nombre as divisa,
               d.simbolo,
               d.decimales,
               tl.descripcion,
               tl.usuario_id,
               CONCAT(tl.usuario_id, \"/\", ta.nombre_fichero) as fichero
        FROM transacciones_log tl
        INNER JOIN transacciones_tipos tt
          ON tl.tipo_id = tt.id
        INNER JOIN divisas d
          ON tl.divisa_id = d.id  
        LEFT JOIN transacciones_archivos ta
          ON tl.id = ta.transaccion_log_id
        WHERE tl.id = :log_id
    ";

    $stmt = $conexion->prepare($transaccion_log_sql);
    $stmt->bindValue("log_id", $log_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $transaccion_log = [
            "log_id"        => $datos["log_id"],
            "fecha"         => $datos["fecha"],
            "tipo_id"       => $datos["tipo_id"],
            "tipo"          => $datos["tipo"],
            "divisa_id"     => $datos["divisa_id"],
            "divisa"        => $datos["divisa"],
            "simbolo"       => $datos["simbolo"],
            "decimales"     => $datos["decimales"],
            "descripcion"   => $datos["descripcion"],
            "usuario_id"    => $datos["usuario_id"],
            "fichero"       => $datos["fichero"]
        ];
    }    

    return $transaccion_log;

}

//
//  Devuelve los diferentes tipos de transacciones
//
function obtener_tipos_transacciones() {
    $conexion = conectar_bd();
    $tipos_transacciones = [];

    $tipos_transacciones_sql = "
        SELECT id,
               tipo as nombre
        FROM transacciones_tipos
        ORDER BY 2
    ";

    $stmt = $conexion->query($tipos_transacciones_sql);

    foreach ($stmt as $datos) {
        $tipos_transacciones[$datos["id"]] = $datos["nombre"];
    }

    return $tipos_transacciones;

}


//
//  Modifica una transacción
//
//  ToDo:
//      - Qué hacer si categoría o presupuesto es 0 (eliminar de las tablas?)
//
//  $transaccion_log_id: identificador de la transacción
//  $transaccion_origen_id: identificador del movimiento origen
//  $transaccion_destino_id: identificador del movimiento destino
//  $fecha: fecha de la transacción
//  $descripcion: descripción de la transacción
//  $cuenta_origen: identificador de la cuenta de origen
//  $cuenta_destino: identificador de la cuenta de destino
//  $importe: monto de la transacción
//  $divisa: id de la divisa empleada en la transacción

function editar_transaccion($transaccion_log_id, $transaccion_origen_id, $transaccion_destino_id, $fecha, $descripcion, $cuenta_origen, $cuenta_destino, $importe, $divisa, $categoria_id, $presupuesto_id) {

    $importe_origen = "-" . $importe;
    $importe_destino = $importe;

    $conexion = conectar_bd();

    // Fecha de actualización (momento actual)
    $fecha_actual = date("Y-m-d H:i:s");

    // Actualizamos el registro de la transacción
    $actualizar_transaccion_log_sql = "
        UPDATE transacciones_log
        SET descripcion = :descripcion,
            fecha_actualizacion = :fecha_actual,
            fecha = :fecha,
            divisa_id = :divisa_id
        WHERE id = :transaccion_log_id
    ";

    $stmt = $conexion->prepare($actualizar_transaccion_log_sql);
    $stmt->bindValue("descripcion", $descripcion);
    $stmt->bindValue("fecha_actual", $fecha_actual);
    $stmt->bindValue("fecha", $fecha);
    //$stmt->bindValue("tipo_id", $tipo);
    $stmt->bindValue("divisa_id", $divisa);
    $stmt->bindValue("transaccion_log_id", $transaccion_log_id);
    $stmt->execute();

    // Actualizamos los registros de transacciones 
    // Cuenta origen
    $actualizar_transaccion_origen_sql = "
        UPDATE transacciones 
        SET fecha_actualizacion = :fecha_actual,
            cuenta_id = :cuenta_origen,
            importe = :importe_origen,
            divisa_id = :divisa_id
        WHERE id = :transaccion_origen_id
    ";

    $stmt = $conexion->prepare($actualizar_transaccion_origen_sql);
    $stmt->bindValue("fecha_actual", $fecha_actual);
    $stmt->bindValue("cuenta_origen", $cuenta_origen);
    $stmt->bindValue("importe_origen", $importe_origen);
    $stmt->bindValue("divisa_id", $divisa);
    $stmt->bindValue("transaccion_origen_id", $transaccion_origen_id);
    $stmt->execute();


    // Cuenta destino
    $actualizar_transaccion_destino_sql = "
        UPDATE transacciones 
        SET fecha_actualizacion = :fecha_actual,
            cuenta_id = :cuenta_destino,
            importe = :importe_destino,
            divisa_id = :divisa_id
        WHERE id = :transaccion_destino_id
    ";

    $stmt = $conexion->prepare($actualizar_transaccion_destino_sql);
    $stmt->bindValue("fecha_actual", $fecha_actual);
    $stmt->bindValue("cuenta_destino", $cuenta_destino);
    $stmt->bindValue("importe_destino", $importe_destino);
    $stmt->bindValue("divisa_id", $divisa);
    $stmt->bindValue("transaccion_destino_id", $transaccion_destino_id);
    $stmt->execute();

    // Actualizamos la categoría de la transacción
    if ($categoria_id != 0) {
        // Comprobamos si la transacción tiene alguna categoría
        $categoria = obtener_categoria_transaccion($transaccion_log_id);

        // Si la transacción aún no ha sido categorizada:
        if (count($categoria) == 0) {
            $categorizar_transaccion_sql = "
                INSERT INTO categorias_transacciones (
                    categoria_id,
                    transaccion_log_id
                ) VALUES (
                    :categoria_id,
                    :transaccion_log_id
                )
            ";

            $stmt_categoria_nueva = $conexion->prepare($categorizar_transaccion_sql);
            $stmt_categoria_nueva->bindValue("categoria_id", $categoria_id);
            $stmt_categoria_nueva->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_categoria_nueva->execute();
        // Si la transacción está categorizada, actualizamos con la
        // nueva categoría
        } else {
            $actualizar_categoria_transaccion_sql = "
                UPDATE categorias_transacciones 
                SET categoria_id = :categoria_id
                WHERE transaccion_log_id = :transaccion_log_id
            ";

            $stmt_categoria_actualizar = $conexion->prepare($actualizar_categoria_transaccion_sql);
            $stmt_categoria_actualizar->bindValue("categoria_id", $categoria_id);
            $stmt_categoria_actualizar->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_categoria_actualizar->execute();
        }
    } else {
        // Si la categoría es 0, quitamos la transacción de esa categoría

        // Comprobamos primero si la transacción tiene alguna categoría
        $categoria = obtener_categoria_transaccion($transaccion_log_id);

        // Si la transacción ya estaba categorizada, eliminamos la categorización
        if (count($categoria) != 0) {
            $borrar_categorizacion_sql = "
                DELETE
                FROM categorias_transacciones
                WHERE transaccion_log_id = :transaccion_log_id
            ";

            $stmt_borrar_categorizacion = $conexion->prepare($borrar_categorizacion_sql);
            $stmt_borrar_categorizacion->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_borrar_categorizacion->execute();
        }
    }

    // Actualizamos el presupuesto de la transacción
    if ($presupuesto_id != 0) {
        // Comprobamos si la transacción tiene algún presupuesto
        $presupuesto = obtener_presupuesto_transaccion($transaccion_log_id);

        // Si la transacción aún no ha sido metida en un presupuesto
        if (count($presupuesto) == 0) {
            $presupuesto_transaccion_sql = "
                INSERT INTO presupuestos_transacciones (
                    presupuesto_id,
                    transaccion_log_id
                ) VALUES (
                    :presupuesto_id,
                    :transaccion_log_id
                )
            ";

            $stmt_presupuesto_nuevo = $conexion->prepare($presupuesto_transaccion_sql);
            $stmt_presupuesto_nuevo->bindValue("presupuesto_id", $presupuesto_id);
            $stmt_presupuesto_nuevo->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_presupuesto_nuevo->execute();
        // Si la transacción ya está en un presupuesto, actualizamos con el
        // nuevo presupuesto
        } else {
            $actualizar_presupuesto_transaccion_sql = "
                UPDATE presupuestos_transacciones 
                SET presupuesto_id = :presupuesto_id
                WHERE transaccion_log_id = :transaccion_log_id
            ";

            $stmt_presupuesto_actualizar = $conexion->prepare($actualizar_categoria_transaccion_sql);
            $stmt_presupuesto_actualizar->bindValue("presupuesto_id", $presupuesto_id);
            $stmt_presupuesto_actualizar->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_presupuesto_actualizar->execute();
        }
    } else {
        // Si el presupuesto es 0, quitamos la transacción de ese presupuesto

        // Comprobamos antes si la transacción tiene algún presupuesto
        $presupuesto = obtener_presupuesto_transaccion($transaccion_log_id);

        // Si la transacción está en un presupuesto, la borramos
        if (count($presupuesto) != 0) {
            $borrar_presupuesto_sql = "
                DELETE
                FROM presupuestos_transacciones
                WHERE transaccion_log_id = :transaccion_log_id
            ";

            $stmt_borrar_presupuesto = $conexion->prepare($borrar_presupuesto_sql);
            $stmt_borrar_presupuesto->bindValue("transaccion_log_id", $transaccion_log_id);
            $stmt_borrar_presupuesto->execute();
        }
    }

    // 

    if (!$stmt) {
        return false;
    } else {
        return true;
    }  

}

// Devuelve la categoría a la que pertenece una transacción
function obtener_categoria_transaccion($transaccion_log_id) {

    $conexion = conectar_bd();
    $categoria = [];
    $categoria_transaccion_sql = "
        SELECT ct.categoria_id AS id,
               c.nombre
        FROM categorias_transacciones ct
        INNER JOIN transacciones_log tl
           ON ct.transaccion_log_id = tl.id
          AND ct.transaccion_log_id = :transaccion_log_id
        LEFT JOIN categorias c
          ON ct.categoria_id = c.id  
    ";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($categoria_transaccion_sql);
    $stmt->bindValue("transaccion_log_id", $transaccion_log_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $categoria = [
            "id"        => $datos["id"],
            "nombre"    => $datos["nombre"]
        ];
    }

    return $categoria;
}

// Devuelve el presupuesto al que está asociado la transacción
function obtener_presupuesto_transaccion($transaccion_log_id) {

    $conexion = conectar_bd();
    $presupuesto = [];
    $presupuesto_transaccion_sql = "
        SELECT pt.presupuesto_id AS id,
               p.nombre
        FROM presupuestos_transacciones pt
        INNER JOIN transacciones_log tl
           ON pt.transaccion_log_id = tl.id
          AND pt.transaccion_log_id = :transaccion_log_id
        LEFT JOIN presupuestos p
          ON pt.presupuesto_id = p.id  
    ";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($presupuesto_transaccion_sql);
    $stmt->bindValue("transaccion_log_id", $transaccion_log_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $presupuesto = [
            "id"        => $datos["id"],
            "nombre"    => $datos["nombre"]
        ];
    }

    return $presupuesto;
} 

// Devuelve la suma de todos los gastos de un año
function total_gastos($usuario_id, $ano)  {

    $conexion = conectar_bd();
    $gastos = [];
    $total_gastos_sql = "
        SELECT YEAR(tl.fecha) AS ano,
               SUM(t.importe) AS total_gastos
        FROM transacciones t
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id
        WHERE t.importe < 0
          AND YEAR(tl.fecha) = :ano
          AND tl.usuario_id = :usuario_id
          AND c.cuenta_tipo_id = 1 -- solo cuentas de activos
        GROUP BY YEAR(tl.fecha)
    ";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($total_gastos_sql);
    $stmt->bindValue("ano", $ano);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $gastos = [
            "ano"   => $datos["ano"],
            "total" => $datos["total_gastos"]
        ];
    }

    return $gastos;
}

// Devuelve la suma de todos los ingresos de un año
function total_ingresos($usuario_id, $ano)  {

    $conexion = conectar_bd();
    $ingresos = [];
    $total_ingresos_sql = "
        SELECT YEAR(tl.fecha) AS ano,
               SUM(t.importe) AS total_ingresos
        FROM transacciones t
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id
        WHERE t.importe > 0
          AND YEAR(tl.fecha) = :ano
          AND tl.usuario_id = :usuario_id
          AND c.cuenta_tipo_id = 1 -- solo cuentas de activos 
        GROUP BY YEAR(tl.fecha)
    ";

    // Preparando (para evitar inyecciones SQL) y ejecutando la sentencia
    $stmt = $conexion->prepare($total_ingresos_sql);
    $stmt->bindValue("ano", $ano);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $ingresos = [
            "ano"   => $datos["ano"],
            "total" => $datos["total_ingresos"]
        ];
    }

    return $ingresos;
}

// Devuelve el número de cuentas que tiene un usuario
// Permite filtrar también por tipo de cuenta
function numero_cuentas($usuario_id, $tipo_cuenta=null) {

    $conexion = conectar_bd();
    $numero_cuentas = 0;
    $filtro_tipo_cuenta = "";

    if ($tipo_cuenta != null) {
        $filtro_tipo_cuenta = " AND c.cuenta_tipo_id = :tipo_cuenta";
    }

    $numero_cuentas_sql = "
        SELECT COUNT(*) as numero_cuentas
        FROM cuentas c
        WHERE c.usuario_id = :usuario_id" .
              $filtro_tipo_cuenta;

    $stmt = $conexion->prepare($numero_cuentas_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    if ($tipo_cuenta != null) {
        $stmt->bindValue("tipo_cuenta", $tipo_cuenta);
    }
    $stmt->execute();
    $numero_cuentas = $stmt->fetchColumn(); 

    return $numero_cuentas;
}

// Devuelve el número de transacciones de un usuario
function numero_transacciones($usuario_id, $cuenta_id=null) {

    $conexion = conectar_bd();
    $numero_transacciones = 0;
    $filtro_cuenta = "";
    $filtro_join = "";

    if ($cuenta_id != null) {
        $filtro_join = " INNER JOIN transacciones t
                            on tl.id = t.transaccion_log_id
                           and t.cuenta_id = :cuenta_id";
    }

    $numero_transacciones_sql = "
        SELECT COUNT(*) as numero_transacciones
        FROM transacciones_log tl " .
        $filtro_join . "
        WHERE tl.usuario_id = :usuario_id
    ";

    $stmt = $conexion->prepare($numero_transacciones_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    if ($cuenta_id != null) {
        $stmt->bindValue("cuenta_id", $cuenta_id);
    }
    $stmt->execute();
    
    $numero_transacciones = $stmt->fetchColumn(); 

    return $numero_transacciones;

}

// Devuelve las cuentas que tiene un usuario
function cuentas_usuario($usuario_id, $tipo_cuenta=null) {
    $conexion = conectar_bd();

    $cuentas = [];

    if ($tipo_cuenta != null) {
        if ($tipo_cuenta == 1) {
            $filtro_tipo_cuenta = " AND c.cuenta_tipo_id IN (1, 2, 3)";
        } else {
            $filtro_tipo_cuenta = " AND c.cuenta_tipo_id = " . $tipo_cuenta;
        }
    } else {
        $filtro_tipo_cuenta = "";
    }
 
    $cuentas_usuario_sql = "
        SELECT c.id,
               c.nombre
        FROM cuentas c
        WHERE c.usuario_id = :usuario_id" . 
        $filtro_tipo_cuenta;

    $stmt = $conexion->prepare($cuentas_usuario_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $cuentas[$datos["id"]] = $datos["nombre"];
    }

    return $cuentas;
}

// Devuelve el saldo de una cuenta en un determinado mes
function obtener_saldo_mes($cuenta_id, $ano, $mes) {

    $conexion = conectar_bd();

    $saldo_mes_sql = "
        SELECT 
            CASE 
                WHEN SUM(t.importe) IS NULL THEN c.saldo_inicial
                ELSE (c.saldo_inicial + SUM(t.importe)) 
            END as saldo
        FROM transacciones_log tl
        INNER JOIN transacciones t
           ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id  
        WHERE c.id = :cuenta_id
          AND YEAR(tl.fecha) = :ano
          AND MONTH(tl.fecha) <= :mes
    ";

    $stmt = $conexion->prepare($saldo_mes_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->bindValue("ano", $ano);
    $stmt->bindValue("mes", $mes);
    $stmt->execute();

    $saldo = $stmt->fetchColumn();

    return $saldo;
}

// Devuelve el saldo de una cuenta de activos por mes
function saldo_cuenta_mensual($cuenta_id, $ano) {
    $conexion = conectar_bd();

    $meses_transcurridos = date("n");

    $saldo_cuenta = [
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    ];

    // Obtenemos el saldo mensual de los meses que ya hayan pasado
    // del año seleccionado
    for ($mes = 1; $mes <= $meses_transcurridos; $mes++) {
        $saldo_cuenta[$mes-1] = obtener_saldo_mes($cuenta_id, $ano, $mes);
    }

    return $saldo_cuenta;
}

// Devuelve los gastos de cierto mes
function obtener_gastos_mes($usuario_id, $mes) {
    $conexion = conectar_bd();

    $gastos_mes_sql = "
        SELECT CASE 
                    WHEN SUM(t.importe) IS NULL THEN 0
                    ELSE SUM(t.importe)
                END AS total
        FROM transacciones_log tl
        INNER JOIN transacciones t
           ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id   
        WHERE tl.usuario_id = :usuario_id
          AND t.importe < 0 -- gastos
          AND c.cuenta_tipo_id = 1 -- activos  
          AND MONTH(tl.fecha) = :mes
    ";

    $stmt = $conexion->prepare($gastos_mes_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->bindValue("mes", $mes);
    $stmt->execute();

    return $stmt->fetchColumn();
}

// Devuelve los gastos de cierto mes
function obtener_ingresos_mes($usuario_id, $mes) {
    $conexion = conectar_bd();

    $ingresos_mes_sql = "
        SELECT CASE 
                    WHEN SUM(t.importe) IS NULL THEN 0
                    ELSE SUM(t.importe)
                END AS total
        FROM transacciones_log tl
        INNER JOIN transacciones t
           ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id   
        WHERE tl.usuario_id = :usuario_id
          AND t.importe > 0 -- ingresos
          AND c.cuenta_tipo_id IN (1, 2, 3) -- activos  
          AND MONTH(tl.fecha) = :mes
    ";

    $stmt = $conexion->prepare($ingresos_mes_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->bindValue("mes", $mes);
    $stmt->execute();

    return $stmt->fetchColumn();
}

function buscar_transacciones($busqueda) {
    $conexion = conectar_bd();
    $busqueda_like = "%$busqueda%";

    $resultados = [];

    $buscar_transacciones_sql = "
        SELECT tl.id,
               tl.fecha,
               tl.descripcion,
               t.importe,
               d.simbolo as divisa
        FROM transacciones_log tl
        INNER JOIN transacciones t
          ON tl.id = t.transaccion_log_id
        INNER JOIN cuentas c
           ON t.cuenta_id = c.id
        INNER JOIN divisas d
           ON t.divisa_id = d.id             
        WHERE tl.descripcion LIKE :busqueda
          AND c.cuenta_tipo_id IN (1, 2, 3) -- activos
    ";
    $stmt = $conexion->prepare($buscar_transacciones_sql);
    $stmt->bindValue("busqueda", $busqueda_like);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $resultados[] = [
            "id"            => $datos["id"],
            "fecha"         => $datos["fecha"],
            "descripcion"   => $datos["descripcion"],
            "importe"       => $datos["importe"],
            "divisa"        => $datos["divisa"]
        ];
    }

    return $resultados;
}

// Devuelve el presupuesto establecido para el mes actual
function presupuesto_mes($presupuesto_id, $fecha) {
    $conexion = conectar_bd();

    $presupuesto = [];

    $importe_presupuesto_sql = "
        SELECT p.id,
               p.nombre AS nombre,
               pl.importe,
               pl.fecha_inicio,
               pl.fecha_fin
        FROM presupuestos p
        INNER JOIN presupuestos_limites pl
           ON p.id = pl.presupuesto_id
          AND p.id = :presupuesto_id
          AND DATE_FORMAT(pl.fecha_inicio, '%Y-%m') = :fecha
    ";

    $stmt = $conexion->prepare($importe_presupuesto_sql);
    $stmt->bindValue("fecha", $fecha);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $presupuesto = [
            "id"            => $datos["id"],
            "nombre"        => $datos["nombre"],
            "importe"       => $datos["importe"],
            "fecha_inicio"  => $datos["fecha_inicio"],
            "fecha_fin"     => $datos["fecha_fin"]
        ];
    }

    return $presupuesto;
}

// Devuelve la suma de los gastos/transacciones asociados con
// un presupuesto
function obtener_gastos_presupuesto($presupuesto_id, $fecha_ano_mes) {
    $conexion = conectar_bd();

    $gastos = 0;

    $gastos_presupuesto_sql = "
        SELECT ABS(SUM(importe)) as gastos
        FROM presupuestos_transacciones pt
        INNER JOIN transacciones t
           ON pt.transaccion_log_id = t.transaccion_log_id
        INNER JOIN transacciones_log tl
           ON t.transaccion_log_id = tl.id   
        WHERE pt.presupuesto_id = :presupuesto_id
          AND t.importe < 0 -- gastos
          AND DATE_FORMAT(tl.fecha, '%Y-%m') = :fecha_ano_mes
    ";

    $stmt = $conexion->prepare($gastos_presupuesto_sql);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->bindValue("fecha_ano_mes", $fecha_ano_mes);
    $stmt->execute();

    $gastos = $stmt->fetchColumn(); 
    
    return $gastos;
}

// Devuelve información sobre un presupuesto
function obtener_presupuesto($presupuesto_id) {
    $conexion = conectar_bd();

    $presupuesto = [];

    $presupuesto_sql = "
        SELECT nombre,
               usuario_id,
               fecha_fin,
               descripcion
        FROM presupuestos
        WHERE id = :presupuesto_id
    ";

    $stmt = $conexion->prepare($presupuesto_sql);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $presupuesto = [
            "nombre"        => $datos["nombre"],
            "usuario_id"    => $datos["usuario_id"],
            "fecha_fin"     => $datos["fecha_fin"],
            "descripcion"   => $datos["descripcion"]
        ];
    }

    return $presupuesto;
}

// Devuelve información sobre los diferentes límites de un
// presupuesto
function obtener_limites_presupuesto($presupuesto_id) {

    $conexion = conectar_bd();

    $limites = [];

    $presupuesto_limites_sql = "
        SELECT pl.id,
               pl.fecha_inicio,
               pl.importe
        FROM presupuestos_limites pl
        WHERE pl.presupuesto_id = :presupuesto_id
        ORDER BY pl.fecha_inicio
    ";

    $stmt = $conexion->prepare($presupuesto_limites_sql);
    $stmt->bindValue("presupuesto_id", $presupuesto_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $limites[] = [
            "id"        => $datos["id"],
            "fecha"     => $datos["fecha_inicio"],
            "importe"   => $datos["importe"]
        ];
    }

    return $limites;
}

// Modifica la información del perfil del usuario
function editar_perfil($usuario_id, $nombre, $apellidos, $fecha_nacimiento, $clave) {

    // Mientras no se implemente la característica
    $avatar = null;

    $conexion = conectar_bd();

    // Creamos un hash a partir de esa clave
    // PASSWORD_DEFAULT utiliza el algoritmo bcrypt
    $clave_hash = password_hash($clave, PASSWORD_DEFAULT);

    $actualizar_perfil_sql = "
        UPDATE usuarios
        SET clave = :clave_hash
        WHERE id = :usuario_id
    ";

    $stmt = $conexion->prepare($actualizar_perfil_sql);
    $stmt->bindValue("clave_hash", $clave_hash);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    // Comprobamos si tiene información personal
    $informacion_personal_sql = "
        SELECT COUNT(*)
        FROM usuarios_info
        WHERE usuario_id = :usuario_id
    ";

    $stmt_info = $conexion->prepare($informacion_personal_sql);
    $stmt_info->bindValue("usuario_id", $usuario_id);
    $stmt_info->execute();

    $resultados = $stmt_info->fetchColumn(); 

    if ($resultados == 0) {
        // Añadimos información personal del usuario
        $insertar_informacion_personal_sql = "
            INSERT INTO usuarios_info (
                usuario_id,
                avatar,
                nombre,
                apellidos,
                fecha_nacimiento
            ) VALUES (
                :usuario_id,
                :avatar,
                :nombre,
                :apellidos,
                :fecha_nacimiento
            )
        ";

        $stmt_personal = $conexion->prepare($insertar_informacion_personal_sql);
        $stmt_personal->bindValue("usuario_id", $usuario_id);
        $stmt_personal->bindValue("avatar", $avatar);
        $stmt_personal->bindValue("nombre", $nombre);
        $stmt_personal->bindValue("apellidos", $apellidos);
        $stmt_personal->bindValue("fecha_nacimiento", $fecha_nacimiento);
        $stmt_personal->execute();

    } else {
    // Si ya tenía información personal guardada, la actualizamos
        $actualizar_perfil_personal_sql = "
            UPDATE usuarios_info
            SET nombre = :nombre,
                apellidos = :apellidos,
                fecha_nacimiento = :fecha_nacimiento
            WHERE usuario_id = :usuario_id
        ";

        $stmt_personal = $conexion->prepare($actualizar_perfil_personal_sql);
        $stmt_personal->bindValue("nombre", $nombre);
        $stmt_personal->bindValue("apellidos", $apellidos);
        $stmt_personal->bindValue("fecha_nacimiento", $fecha_nacimiento);
        $stmt_personal->bindValue("usuario_id", $usuario_id);
        $stmt_personal->execute();
    }

    /*
    // DEBUG
    $debug = get_defined_vars();
    print_r($debug);
    */

    if ($stmt && $stmt_personal && $stmt_info) {
        return true;
    } else {
        return false;
    }
}

// Muestra un listado de las huchas por cuenta
function listar_huchas($cuenta_id) {
    $conexion = conectar_bd();

    $huchas = [];

    $huchas_sql = "
        SELECT id,
               nombre,
               importe_objetivo
        FROM huchas
        WHERE cuenta_id = :cuenta_id
        ORDER BY nombre
    ";

    $stmt = $conexion->prepare($huchas_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $huchas[$datos["id"]] = [
            "nombre"            => $datos["nombre"],
            "importe_objetivo"  => $datos["importe_objetivo"]
        ];
    }

    return $huchas;

}

// Crea hucha
function crear_hucha($nombre, $cuenta, $importe_objetivo, $descripcion, $fecha_objetivo) {
    $conexion = conectar_bd();
    $fecha = date("Y-m-d H:i:s");
    $fecha_inicio = date("Y-m-d H:i:s");

    $crear_hucha_sql = "
        INSERT INTO huchas (
            nombre,
            cuenta_id,
            importe_objetivo,
            descripcion,
            fecha_creacion,
            fecha_actualizacion,
            fecha_inicio,
            fecha_objetivo
        ) VALUES (
            :nombre,
            :cuenta_id,
            :importe_objetivo,
            :descripcion,
            :fecha_creacion,
            :fecha_actualizacion,
            :fecha_inicio,
            :fecha_objetivo
        )
    ";

    $stmt = $conexion->prepare($crear_hucha_sql);
    $stmt->bindValue("nombre", $nombre);
    $stmt->bindValue("cuenta_id", $cuenta);
    $stmt->bindValue("importe_objetivo", $importe_objetivo);
    $stmt->bindValue("descripcion", $descripcion);
    $stmt->bindValue("fecha_creacion", $fecha);
    $stmt->bindValue("fecha_actualizacion", $fecha);
    $stmt->bindValue("fecha_inicio", $fecha_inicio);
    $stmt->bindValue("fecha_objetivo", $fecha_objetivo);
    $stmt->execute();

    if (!$stmt) {
        return false;
    } else {
        return true;
    }
}

// Elimina una hucha
function eliminar_hucha($hucha_id) {
    $conexion = conectar_bd();

    $eliminar_hucha_sql = "
        DELETE
        FROM huchas
        WHERE id = :hucha_id
    ";

    $stmt = $conexion->prepare($eliminar_hucha_sql);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->execute();

    $registros_borrados = $stmt->rowCount();

    if ($registros_borrados == 0) {
        return false;
    } else {
        return true;
    }
}

function info_hucha($hucha_id) {
    $conexion = conectar_bd();

    $hucha = [];

    $hucha_sql = "
        SELECT nombre,
               cuenta_id,
               importe_objetivo,
               descripcion,
               fecha_inicio,
               fecha_objetivo
        FROM huchas
        WHERE id = :hucha_id
    ";

    $stmt = $conexion->prepare($hucha_sql);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $hucha = [
            "nombre"            => $datos["nombre"],
            "cuenta_id"         => $datos["cuenta_id"],
            "importe_objetivo"  => $datos["importe_objetivo"],
            "descripcion"       => $datos["descripcion"],
            "fecha_inicio"      => $datos["fecha_inicio"],
            "fecha_objetivo"    => $datos["fecha_objetivo"]
        ];
    }

    return $hucha;
}

// Devuelve información sobre una cuenta
function obtener_cuenta($cuenta_id) {

    $conexion = conectar_bd();

    $cuenta = [];

    $cuenta_sql = "
        SELECT cuenta_tipo_id,
               nombre,
               usuario_id,
               iban,
               bic,
               saldo_inicial,
               divisa_id,
               descripcion
        FROM cuentas
        WHERE id = :cuenta_id
    ";

    $stmt = $conexion->prepare($cuenta_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $cuenta = [
            "cuenta_tipo_id"    => $datos["cuenta_tipo_id"],
            "nombre"            => $datos["nombre"],
            "usuario_id"        => $datos["usuario_id"],
            "iban"              => $datos["iban"],
            "bic"               => $datos["bic"],
            "saldo_inicial"     => $datos["saldo_inicial"],
            "divisa_id"         => $datos["divisa_id"],
            "descripcion"       => $datos["descripcion"]
        ];
    }

    return $cuenta;
}

function crear_movimiento_hucha($hucha_id, $importe, $fecha) {

    $conexion = conectar_bd();

    $fecha_actual = date("Y-m-d H:i:s");
    
    $crear_movimiento_hucha_sql = "
        INSERT INTO huchas_movimientos (
            hucha_id,
            importe,
            fecha,
            fecha_creacion,
            fecha_actualizacion
        ) VALUES (
            :hucha_id,
            :importe,
            :fecha,
            :fecha_creacion,
            :fecha_modificacion
        )
    ";

    $stmt = $conexion->prepare($crear_movimiento_hucha_sql);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->bindValue("importe", $importe);
    $stmt->bindValue("fecha", $fecha);
    $stmt->bindValue("fecha_creacion", $fecha_actual);
    $stmt->bindValue("fecha_modificacion", $fecha_actual);
    $stmt->execute();

    if ($stmt) {
        return true;
    } else {
        return false;
    }

}

// Devuelve la cantidad de dinero ahorrado en una hucha
function obtener_ahorro_hucha($hucha_id) {

    $conexion = conectar_bd();

    $obtener_ahorro_hucha_sql = "
        SELECT SUM(importe) AS ahorrado
        FROM huchas_movimientos 
        WHERE hucha_id = :hucha_id
    ";

    $stmt = $conexion->prepare($obtener_ahorro_hucha_sql);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->execute();

    $ahorro = $stmt->fetchColumn(); 

    return $ahorro;
}

function movimientos_hucha($hucha_id) {

    $conexion = conectar_bd();

    $movimientos = [];

    $movimientos_hucha_sql = "
        SELECT fecha,
               importe
        FROM huchas_movimientos
        WHERE hucha_id = :hucha_id
        ORDER BY fecha_creacion DESC
    ";

    $stmt = $conexion->prepare($movimientos_hucha_sql);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $movimientos[] = [
            "fecha"     => $datos["fecha"],
            "importe"   => $datos["importe"]
        ];
    }

    return $movimientos;
}

// Devuelve los tipos de cuenta que se pueden
// crear en Miscu
function obtener_tipos_cuenta() {

    $conexion = conectar_bd();

    $tipos = [];

    $tipos_cuenta_sql = "
        SELECT id,
               nombre
        FROM cuentas_tipos
        ORDER BY nombre
    ";

    $stmt = $conexion->prepare($tipos_cuenta_sql);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $tipos[] = [
            "id"     => $datos["id"],
            "nombre"   => $datos["nombre"]
        ];
    }

    return $tipos;
}

// Devuelve las divisas que hay disponibles en Miscu
function obtener_divisas() {
    $conexion = conectar_bd();

    $divisas = [];

    $divisas_sql = "
        SELECT id,
               nombre,
               codigo,
               simbolo,
               decimales
        FROM divisas
    ";

    $stmt = $conexion->prepare($divisas_sql);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $divisas[] = [
            "id"        => $datos["id"],
            "nombre"    => $datos["nombre"],
            "codigo"    => $datos["codigo"],
            "simbolo"   => $datos["simbolo"],
            "decimales" => $datos["decimales"]
        ];
    }

    return $divisas;
}

// 
function obtener_transacciones_categorizadas($usuario_id) {

    $conexion = conectar_bd();

    $categorias = [];

    $transacciones_categorias_sql = "
        SELECT ct.categoria_id,
               ct.transaccion_log_id
        FROM categorias_transacciones ct
        INNER JOIN categorias c
          on ct.categoria_id = c.id
         and c.usuario_id = :usuario_id
    ";

    $stmt = $conexion->prepare($transacciones_categorias_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $categorias[] = [
            "categoria_id"          => $datos["categoria_id"],
            "transaccion_log_id"    => $datos["transaccion_log_id"]
        ];
    }

    return $categorias;
}

// Devuelve la primera transacción del usuario, no por la
// fecha de creación en la base de datos sino por la fecha
// en la que el usuario realizó esa transacción en el mundo real
function obtener_primera_transaccion($usuario_id) {

    $conexion = conectar_bd();

    $transaccion = [];

    $transaccion_sql = "
        SELECT id,
               fecha,
               descripcion
        FROM transacciones_log
        WHERE fecha = (
            SELECT MIN(fecha)
            FROM transacciones_log
            )
          AND usuario_id = :usuario_id
        LIMIT 1
    ";

    $stmt = $conexion->prepare($transaccion_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $transaccion = [
            "id"            => $datos["id"],
            "fecha"         => $datos["fecha"],
            "descripcion"   => $datos["descripcion"],
        ];
    }

    return $transaccion;
}

// Devuelve los movimientos de una cuenta
function movimientos_cuenta($cuenta_id, $fecha_inicio=null, $fecha_fin=null) {

    if ($fecha_inicio == null && $fecha_fin == null) {
        $filtro_fecha = "";
    } else {
        $filtro_fecha = " AND DATE_FORMAT(l.fecha, '%Y-%m-%d') BETWEEN DATE_FORMAT('" . $fecha_inicio . "', '%Y-%m-%d') AND DATE_FORMAT('" . $fecha_fin . "', '%Y-%m-%d')";
    }

    $conexion = conectar_bd();
    // Array que contendrá todos los movimientos de la cuenta
    $movimientos = [];
    
    $movimientos_cuenta_sql = "
        SELECT t.id, 
               l.id as log_id,
               DATE_FORMAT(l.fecha, '%Y-%m-%d %H:%i:%s') AS fecha,
               tt.tipo,
               l.descripcion,
               cat.nombre AS categoria,
               -- pre.nombre AS presupuesto,
               t.importe,
               (c.saldo_inicial + t.importe) AS saldo,
               d.simbolo as divisa_simbolo
        FROM transacciones t
        INNER JOIN transacciones_log l
        ON t.transaccion_log_id = l.id
        INNER JOIN transacciones_tipos tt
        ON l.tipo_id = tt.id
        INNER JOIN cuentas c
        ON t.cuenta_id = c.id
        INNER JOIN divisas d
           ON l.divisa_id = d.id
        LEFT JOIN categorias_transacciones ct
          ON ct.transaccion_log_id = tt.id
        LEFT JOIN categorias cat
          ON cat.id = ct.categoria_id
        -- LEFT JOIN presupuestos_transacciones pt
          -- ON pt.transaccion_id = t.id
        -- LEFT JOIN presupuestos pre
          -- ON pt.presupuesto_id = pre.id
        WHERE t.cuenta_id = :cuenta_id -- Cuenta de la que queremos sacar los datos
          $filtro_fecha 
        ORDER BY l.fecha DESC
    ";

    $stmt = $conexion->prepare($movimientos_cuenta_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    foreach ($stmt as $registro) {
        $movimientos[$registro["id"]] = [
            "log_id"        => $registro["log_id"],
            "fecha"         => $registro["fecha"],
            "tipo"          => $registro["tipo"],
            "descripcion"   => $registro["descripcion"],
            "categoria"     => $registro["categoria"],
            "importe"       => $registro["importe"],
            "saldo"         => $registro["saldo"],
            "divisa_simbolo"=> $registro["divisa_simbolo"]
        ];
    }

    return $movimientos;

}

// Devuelve las transacciones agrupadas por categoría
function movimientos_categoria($categoria_id) {

    $conexion = conectar_bd();

    $movimientos = [];

    $movimientos_sql = "
        SELECT ct.categoria_id,
               ct.transaccion_log_id,
               tl.descripcion,
               t.importe,
               tl.fecha,
               t.cuenta_id
        FROM categorias_transacciones ct
        INNER join transacciones_log tl
           ON ct.transaccion_log_id = tl.id
        INNER join transacciones t
           ON tl.id = t.transaccion_log_id    
        INNER join cuentas c
           ON t.cuenta_id = c.id
        INNER join cuentas_tipos ctip
           ON c.cuenta_tipo_id = ctip.id       
          AND ctip.id IN (1, 2, 3)   -- cuentas de activos
        WHERE ct.categoria_id = :categoria_id
        ORDER BY tl.fecha DESC
    ";

    $stmt = $conexion->prepare($movimientos_sql);
    $stmt->bindValue("categoria_id", $categoria_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $movimientos[] = [
            "transaccion_log_id"    => $datos["transaccion_log_id"],
            "fecha"                 => $datos["fecha"],
            "descripcion"           => $datos["descripcion"],
            "importe"               => $datos["importe"],
            "cuenta_id"             => $datos["cuenta_id"]
        ];
    }

    return $movimientos;
}

// Cuentas que tienen huchas
function cuentas_hucha($usuario_id) {

    $conexion = conectar_bd();

    $cuentas = [];

    $cuentas_sql = "
        SELECT h.cuenta_id
        FROM huchas h
        INNER JOIN cuentas c
           ON h.cuenta_id = c.id
        WHERE c.usuario_id = :usuario_id
        GROUP BY cuenta_id
        ORDER BY 1
    ";

    $stmt = $conexion->prepare($cuentas_sql);
    $stmt->bindValue("usuario_id", $usuario_id);
    $stmt->execute();

    foreach ($stmt as $datos) {
        $cuentas[] = $datos["cuenta_id"];
    }

    return $cuentas;
}

// Devuelve el total de dinero apartado de las huchas
// asociadas a una cuenta
function importe_huchas($cuenta_id) {

    $conexion = conectar_bd();

    $importe = 0;

    $importe_sql = "
        SELECT SUM(importe) as total
        FROM huchas h
        INNER JOIN huchas_movimientos hm
           ON h.id = hm.hucha_id
        WHERE cuenta_id = :cuenta_id
    ";

    $stmt = $conexion->prepare($importe_sql);
    $stmt->bindValue("cuenta_id", $cuenta_id);
    $stmt->execute();

    $importe = $stmt->fetchColumn(); 

    return $importe;
}
?>