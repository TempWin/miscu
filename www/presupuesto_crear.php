<?php
//
// Formulario para crear un presupuesto
//
//
require_once("functions.php");

$presupuesto_nombre = $_POST["presupuesto_nombre"];

if (!isset($_POST["descripcion"])) {
    $descripcion = null;
} else {
    $descripcion = $_POST["descripcion"];
}
$usuario_id = $_POST["usuario_id"];

if (crear_presupuesto($usuario_id, $presupuesto_nombre, $descripcion)) {
    echo "Presupuesto creado";
} else {
    echo "Error al crear presupuesto" . PHP_EOL;
}
?>
    