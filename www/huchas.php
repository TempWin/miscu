<?php
//  Página de huchas
//
//  06/03/2019

session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$mes_actual = date("n");

$cuentas = listar_cuentas($usuario_id, "activos");
$cuentas_id = [];

foreach ($cuentas as $id => $datos) {
    $cuentas_id[] = $id;
}

$huchas = [];

foreach ($cuentas_id as $cuenta_id) {
    $hucha = listar_huchas($cuenta_id);
    if (!empty($hucha)) {
        $huchas[$cuenta_id] = $hucha;
    }
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Huchas - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Datetimepicker -->
        <link rel="stylesheet" type="text/css" href="vendor/datetimepicker/jquery.datetimepicker.css">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Huchas</h1>
                        <p>Igual que las huchas de toda la vida: separa dinero para poder comprar algo en un futuro</p>
                        <p><button onclick="mostrar_insertar_hucha();" class="btn btn-primary">Añadir hucha</button></p>
                        <div id="insertar-hucha" class="row mb-3 ml-0" style="display: none;" >
                            <form id="form-hucha-nueva">
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="hucha_nombre" name="hucha_nombre" required aria-describedby="nombre" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Cuenta asociada</label>

                                    <select class="custom-select" name="hucha_cuenta" required>
<?php 
foreach ($cuentas as $id => $datos) {
    echo "
                                        <option value=\"" . $id . "\">" . $datos["nombre"] . "</option>" . PHP_EOL;
}
?>                                    
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="importe_objetivo">Importe objetivo</label>
                                    <input class="form-control" type="text" name="hucha_importe_objetivo" id="hucha_importe_objetivo" value="" placeholder="" required>
                                </div>
                                <div class="form-group">
                                    <label for="hucha_fecha_objetivo">Fecha objetivo</label>
                                    <input class="form-control" type="text" name="hucha_fecha_objetivo" id="hucha_fecha_objetivo" value="" placeholder="">
                                </div>
                                <a class="btn btn-primary ml-2" href="#" onclick="nueva_hucha();">Crear</a>
                            </form>
                        </div>
                        <div class="alert alert-success" role="alert" id="insertar-hucha-resultado" style="display: none;">
                        </div>
<?php 
if (count($huchas) != 0) {
?>                        
                        <div class="row">
                            <div class="col-xs-12 col-lg-10 col-xl-10">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Listado de huchas</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="huchas-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Objetivo</th>
                                                    <th>Ahorrado</th>
                                                    <th>Restante</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($huchas as $cuenta_id => $datos) {
        foreach ($datos as $hucha_id => $datos_hucha) {
            $ahorrado = obtener_ahorro_hucha($hucha_id);
            $importe_objetivo = $datos_hucha["importe_objetivo"];
            $restante = $importe_objetivo - $ahorrado;
            $importe_objetivo = number_format($importe_objetivo, 2, ",", ".");
            $ahorrado = number_format($ahorrado, 2, ",", ".");
            $restante = number_format($restante, 2, ",", ".");
        echo "
                                                <tr>
                                                    <td id=\"hucha-" . $hucha_id . "\"><a href=\"hucha_info.php?id=" . $hucha_id . "\">" . $datos_hucha["nombre"] . "</a></td>
                                                    <td>" . $importe_objetivo . " €</td>
                                                    <td>
                                                            <a href=\"hucha_movimiento_crear.php?id=" . $hucha_id . "&t=1\"><i class=\"fas fa-plus\"></i></a>
                                                            " . $ahorrado . " €
                                                            <a href=\"hucha_movimiento_crear.php?id=" . $hucha_id . "&t=0\"><i class=\"fas fa-minus\"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>" . $restante . " €</td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $hucha_id . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_hucha(" . $hucha_id . ");\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"hucha_eliminar.php?id=" . $hucha_id . "\"><i class=\"far fa-trash-alt\"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>" . PHP_EOL;
        }
    }
    echo "
                                            </tbody>
                                        </table>" . PHP_EOL;
?>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-lg-10 col-xl-10">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Estado de cuentas</h6>
                                    </div>
                                    <!-- Card Body -->

                                    <div class="card-body" id="huchas-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Cuenta</th>
                                                    <th>Saldo actual</th>
                                                    <th>En Huchas</th>
                                                    <th>Saldo posterior</th>
                                            </thead>
                                            <tbody>
<?php 
    $cuentas_hucha = cuentas_hucha($usuario_id);

    foreach ($cuentas_hucha as $id) {
        $cuenta = obtener_cuenta($id);
        $saldo = obtener_saldo($id);
        if ($saldo < 0) {
            $estilo_saldo = "gasto";
        } else {
            $estilo_saldo = "";
        }
        $importe_huchas = importe_huchas($id);
        $saldo_sin_huchas = $saldo - $importe_huchas;
        if ($saldo_sin_huchas < 0) {
            $estilo_saldo_sin_huchas = "gasto";
        } else {
            $estilo_saldo_sin_huchas = "";
        }
        $saldo_sin_huchas = number_format($saldo_sin_huchas, 2, ",", ".");
        $saldo = number_format($saldo, 2, ",", ".");
        $importe_huchas = number_format($importe_huchas, 2, ",", ".");
        echo "
                                                <tr>
                                                    <td><a href=\"cuenta_info.php?id=" . $id . "\">" . $cuenta["nombre"] . "</a></td>
                                                    <td><span class=\"" . $estilo_saldo . "\">" . $saldo . " €</span></td>
                                                    <td>" . $importe_huchas . " €</td>
                                                    <td><span class=\"" . $estilo_saldo_sin_huchas . "\">" . $saldo_sin_huchas . " €</span></td>
                                                </tr>" . PHP_EOL;
    }
?>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Modal aportar a hucha -->
        <div class="modal fade" id="hucha_in" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Meter dinero en la hucha</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-hucha">
                            <input type="text" class="form-control" id="importe" name="importe" value="">
                            <input type="hidden" id="hucha_id_movimiento" name="hucha_id_movimiento" value="">
                            <input type="hidden" id="tipo_movimiento" name="tipo_movimiento" value="1">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="#" onclick="movimiento_hucha(1);">Guardar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal quitar de la hucha-->
        <div class="modal fade" id="hucha_out" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Quitar dinero de la hucha</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form-hucha">
                            <input type="text" class="form-control" id="importe" name="importe" value="">
                            <input type="hidden" id="hucha_id_movimiento" name="hucha_id_movimiento" value="">
                            <input type="hidden" id="tipo_movimiento" name="tipo_movimiento" value="0">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="#" onclick="movimiento_hucha(0);">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Datetimepicker -->
        <script src="vendor/datetimepicker/build/jquery.datetimepicker.full.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
        <script>
            $.datetimepicker.setLocale('es');
            $("#hucha_fecha_objetivo").datetimepicker({
                timepicker: false,
                dayOfWeekStart: 1,
                format:'Y-m-d'
            });

            $(document).on("click", ".hucha_movimiento", function () {
                 var hucha_id = $(this).data('id');
                 $("#hucha_id_movimiento").val(hucha_id);
                 console.log(hucha_id);
                 // As pointed out in comments, 
                 // it is superfluous to have to manually call the modal.
                 // $('#addBookDialog').modal('show');
            });
        </script>
    </body>
</html>