<?php
//
// Información extra sobre la transacción para cargar por AJAX
//
//  14/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");

$transaccion_log_id = $_GET["id"];

$transaccion_log = info_transaccion_log($transaccion_log_id);

// Categoría a la que pertenece la transacción
$categoria_transaccion = obtener_categoria_transaccion($transaccion_log_id);
if (empty($categoria_transaccion)) {
    $categoria = "Ninguna";
} else {
    $categoria = $categoria_transaccion["nombre"];
}

// Presupuesto al que está asociada la transacción
$presupuesto_transaccion = obtener_presupuesto_transaccion($transaccion_log_id);
if (empty($presupuesto_transaccion)) {
    $presupuesto = "Ninguno";
} else {
    $presupuesto = $presupuesto_transaccion["nombre"];
}
?>
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Categoría</th>
                                                    <th>Presupuesto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $categoria; ?></td>
                                                    <td><?php echo $presupuesto; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>