<?php
//  Página de cuentas de ingresos
//
//  ToDo:
//      * Recargar el listado de cuentas tras la inserción/eliminación
//

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$cuentas = listar_cuentas($usuario_id, "ingresos");
$numero_cuentas_ingresos = count($cuentas);

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Cuentas de ingresos - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Cuentas de ingresos</h1>
                        <p>Todos los sitios/personas de donde conseguimos dinero (empresa, devolución Hacienda...)</p>
                        <p><button onclick="mostrar_insertar_cuenta(2, <?php echo $usuario_id; ?>);" class="btn btn-primary">Añadir cuenta</button></p>

                        <div class="alert alert-success" role="alert" id="insertar-cuenta-resultado" style="display: none;">
                        </div>
                        <!-- Formulario para crear una nueva cuenta de activos -->
                        <div id="insertar-cuenta" class="row mb-4 mt-2" style="display: none;" >
                        </div>

<?php 
if ($numero_cuentas_ingresos == 0) {
?>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card mb-4 py-3 border-left-primary">
                                    <div class="card-body">
                                        Aún no tienes ninguna cuenta creada. Utiliza el botón de arriba para crear tu primera cuenta de gastos.
                                    </div>
                                </div>
                            </div>
                        </div>
<?php
} else {
?>
                        
                        <div class="row">
                            <div class="col-xl-7 col-lg-7">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Listado de cuentas</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="cuentas-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre cuenta</th>
                                                    <th>Última actividad</th>
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($cuentas as $id => $datos) {
        $ultimo_movimiento = obtener_ultimo_movimiento($id);
        if (empty($ultimo_movimiento)) {
            $ultima_actividad = "Sin movimientos";
        } else {
            $ultima_actividad = new DateTime($ultimo_movimiento["fecha"]);
            $ultima_actividad = $ultima_actividad->format("d/m/Y");
        }
        /*
        // DEBUG
        echo "<pre>" . PHP_EOL;
        print_r($ultimo_movimiento) . PHP_EOL;
        echo "</pre>" . PHP_EOL;
        */
        echo "
                                                <tr>
                                                    <td><a href=\"cuenta_info.php?id=" . $id . "\">" . $datos["nombre"] . "</a></td>
                                                    <td>" . $ultima_actividad . "</td>
                                                </tr>" . PHP_EOL;
    }
?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizadso -->
        <script src="js/scripts.js"></script>
    </body>
</html>