<?php 
// Lista las categorías del usuario

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");
?>

                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre categoría</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
$categorias = listar_categorias($usuario_id);

foreach ($categorias as $categoria) {
    echo "
                                                <tr>
                                                    <td id=\"cat-" . $categoria["id"] . "\"><a href=\"categoria_info.php?id=" . $categoria["id"] . "\">" . $categoria["nombre"] . "</a></td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $categoria["id"] . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_categoria(" . $categoria["id"] . ");\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"categoria_eliminar.php?id=" . $categoria["id"] . "\"><i class=\"far fa-trash-alt\"></i></a>
                                                        </div>
                                                    </td>
                                                    <!-- <td id=\"edit-" . $categoria["id"] . "\"><a href=\"#\" onclick=\"campo_editable_categoria(" . $categoria["id"] , ");\"><i class=\"far fa-edit\"></i></a></td>
                                                    <td><a href=\"categoria_eliminar.php?id=" . $categoria["id"] . "\"><i class=\"far fa-trash-alt\"></i></td> -->
                                                </tr>" . PHP_EOL;
}
echo "
                                            </tbody>
                                        </table>" . PHP_EOL;