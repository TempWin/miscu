<?php
//
//  Script que procesa y crea una transacción
//
//
require_once("functions.php");

// Creamos variables con el mismo nombre y contenido
// que viene por POST:
extract($_POST, EXTR_OVERWRITE);
/*
echo "<pre>";
$debug = get_defined_vars();
print_r($debug);
echo "</pre>";
exit();
*/

$fichero = $_FILES;

/*
// DEBUG
$arr = get_defined_vars();
echo "<pre>" . PHP_EOL;
var_dump($arr);
echo "</pre>" . PHP_EOL;
exit();
*/

// Reemplazamos la coma por el punto como separador de decimales
// para no tener problemas a la hora de insertarlo en la base de datos
$importe = str_replace(",", ".", $importe);

// Gasto: 1
// Ingreso: 2
// Transferencia: 3

if (crear_transaccion($tipo_transaccion_id, $usuario_id, $cuenta_origen, $cuenta_destino, $importe, $divisa_id, $importe_extranjero, $divisa_extranjera_id, $fecha, $descripcion, $presupuesto_id, $categoria_id, $fichero)) {
    //echo "OK" . PHP_EOL;
    header("Location: transaccion_exito.php");
} else {
    //echo "KO" . PHP_EOL;
    header("Location: transaccion_error.php");
}

?>