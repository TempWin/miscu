<?php 
//
//  Edita el nombre de una hucha
//
if (isset($_POST["hucha_id"]) && isset($_POST["hucha_nombre"])) {
    $hucha_id = $_POST["hucha_id"];
    $hucha_nombre = $_POST["hucha_nombre"];

    require_once("functions.php");

    $conexion = conectar_bd();
    $fecha_actual = date("Y-m-d H:i:s");

    $actualizar_hucha_sql = "
        UPDATE huchas
        SET nombre = :hucha_nombre,
            fecha_actualizacion = :fecha_actual
        WHERE id = :hucha_id
    ";

    $stmt = $conexion->prepare($actualizar_hucha_sql);
    $stmt->bindValue("hucha_nombre", $hucha_nombre);
    $stmt->bindValue("fecha_actual", $fecha_actual);
    $stmt->bindValue("hucha_id", $hucha_id);
    $stmt->execute();
} else {
    echo "Error modificando hucha";
}

?>        