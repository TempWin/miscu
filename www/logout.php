<?php
// Cierre de la sesión

session_start();
unset($_SESSION["usuario_id"]);
unset($_SESSION["usuario"]);

header("Location: login.php");
?>