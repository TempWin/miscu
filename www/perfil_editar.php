<?php
//
// Formulario para editar el perfil del usuario

ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");

print_r($_POST);

extract($_POST, EXTR_OVERWRITE);

if ($nombre == "") {
    $nombre = null;
}

if ($apellidos == "") {
    $apellidos = null;
}

if ($fecha_nacimiento == "") {
    $fecha_nacimiento = null;
}

if (editar_perfil($usuario_id, $nombre, $apellidos, $fecha_nacimiento, $clave)) {
    echo "Perfil actualizado";
} else {
    echo "Error editando perfil";
}

