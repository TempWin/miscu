<?php
//  Crea un nuevo movimiento en la hucha
//
//  07/03/2019

session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$hucha_id = $_GET["id"];
$tipo_movimiento = $_GET["t"];

if ($tipo_movimiento == 0) {
    $movimiento = "retirar";
} else {
    $movimiento = "aportar";
}

if ($_POST) {
    $fecha = date("Y-m-d");
    if ($tipo_movimiento == 0) {
        $importe = -($_POST["importe"]);
    } else {
        $importe = $_POST["importe"];
    }
    if (crear_movimiento_hucha($hucha_id, $importe, $fecha)) {
        header("Location: huchas.php");
    } else {
        $error = "Ha ocurrido un error con la hucha";
    }
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Datetimepicker -->
        <link rel="stylesheet" type="text/css" href="vendor/datetimepicker/jquery.datetimepicker.css">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Huchas</h1>
                        <p>Introduce la cantidad que quieras <?php echo $movimiento; ?></p>
                        <form method="post" action="">
                            <input type="text" name="importe">
                            <input type="submit" value="Guardar">
                        </form>
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Datetimepicker -->
        <script src="vendor/datetimepicker/build/jquery.datetimepicker.full.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
        <script>
            $.datetimepicker.setLocale('es');
            $("#hucha_fecha_objetivo").datetimepicker({
                timepicker: false,
                dayOfWeekStart: 1,
                format:'Y-m-d'
            });

            $(document).on("click", ".hucha_movimiento", function () {
                 var hucha_id = $(this).data('id');
                 $("#hucha_id_movimiento").val(hucha_id);
                 console.log(hucha_id);
                 // As pointed out in comments, 
                 // it is superfluous to have to manually call the modal.
                 // $('#addBookDialog').modal('show');
            });
        </script>
    </body>
</html>