<?php
//
// Información sobre la cuenta
//
session_start();

if(!isset($_SESSION["usuario_id"])) {

    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");

$cuenta_id = $_GET["id"];
//$saldo_inicial = obtener_saldo_inicial($cuenta_id);
$info_cuenta = info_cuenta($cuenta_id);

$numero_transacciones = numero_transacciones($usuario_id, $cuenta_id);

$movimientos = movimientos_cuenta($cuenta_id);

$decimales = $info_cuenta["decimales"];
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3"><span class="text-muted">Cuenta » </span><?php echo $info_cuenta["nombre"]; ?></h1>

                        <!-- Resultado de la modificación de la cuenta -->
                        <div class="alert alert-success" role="alert" id="editar-cuenta-resultado" style="display: none;">
                        </div>

                        <!-- Formulario para crear una nueva cuenta de activos -->
                        <div id="editar-cuenta" class="row mb-4 mt-2" style="display: none;" >
                        </div>    

                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <a href="#collapseCardExample" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseCardExample">
                                        <h6 class="m-0 font-weight-bold text-primary">Información de la cuenta</h6>
                                    </a>
                                    <!-- Card Body -->
                                    <div class="collapse" id="collapseCardExample" style="">
                                        <div class="card-body">

                                            <table class="table table-bt0">
                                                <tbody>
                                                    <tr>
                                                        <td>Descripción</td>
                                                        <td><?php echo $info_cuenta["descripcion"]; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tipo de cuenta</td>
                                                        <td><?php echo $info_cuenta["tipo"]; ?></td>
                                                    </tr>
<?php 
if ($info_cuenta["iban"] != "") {
?>                                                
                                                    <tr>
                                                        <td>IBAN</td>
                                                        <td><?php echo $info_cuenta["iban"]; ?></td>
                                                    </tr>
<?php 
}
if ($info_cuenta["bic"] != "") {
?>                                                
                                                    <tr>
                                                        <td>BIC</td>
                                                        <td><?php echo $info_cuenta["bic"]; ?></td>
                                                    </tr>
<?php 
}
?>                                                
                                                    <tr>
                                                        <td>Divisa</td>
                                                        <td><?php echo $info_cuenta["divisa"]; ?> (<?php echo $info_cuenta["divisa_simbolo"]; ?>)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fecha alta</td>
                                                        <td><?php $fecha_creacion = new DateTime($info_cuenta["fecha_creacion"]); echo $fecha_creacion->format("d/m/Y"); ?></td>
                                                    </tr>
<?php 
if ($info_cuenta["cuenta_tipo_id"] == 1) {
    $saldo = obtener_saldo($cuenta_id);
    if ($saldo < 0) {
        $estilo_saldo = "gasto";
    } else {
        $estilo_saldo = "";
    }
    $saldo = number_format($saldo, 2, ",", ".");
?>                                                
                                                    <tr>
                                                        <td>Saldo</td>
                                                        <td><span class="<?php echo $estilo_saldo; ?>"><?php echo $saldo . " " . $info_cuenta["divisa_simbolo"]; ?></span></td>
                                                    </tr>
<?php 
}
?>                                              </tbody>      
                                            </table>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                            <button onclick="mostrar_editar_cuenta(<?php echo $cuenta_id; ?>);" class="btn btn-primary">Editar cuenta</button>
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#eliminar_cuenta">Eliminar</a>
                                        </div>
                                        </div>
                                    </div>
                                </div> <!-- card -->
                            </div>

<?php 
if ($numero_transacciones == 0) {
?>
                            <div class="col-lg-12">
                                <div class="card mb-4 py-3 border-left-primary">
                                    <div class="card-body">
                                        Aún no hay ningún movimiento registrado en esta cuenta. Puedes anotar <a href="/transaccion_gasto.php">gastos</a>, <a href="/transaccion_ingreso.php">ingresos</a> o <a href="/transaccion_transferencia.php">transferencias</a>.
                                    </div>
                                </div>
                            </div>
<?php
} else {
?>      

                            <div class="col-xl-12 col-lg-12">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Últimos movimientos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <!--<th>Tipo</th>-->
                                                    <!--<th>Categoría</th>-->
                                                    <th>Descripción</th>
                                                    <th>Importe</th>
<?php 
// Si es una cuenta de activos, mostramos el saldo
    if ($info_cuenta["cuenta_tipo_id"] == 1) {
?>                                                    
                                                    <th>Saldo</th>
<?php 
    }
?>                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($movimientos as $id => $datos) {
        // Saldo disponible en cada transacción
        $saldo = obtener_saldo_fecha($cuenta_id, $datos["fecha"]);
        $fecha = new DateTime($datos["fecha"]);
        $fecha_corta = $fecha->format("d/m/Y");

        if ($datos["importe"] < 0) {
            $estilo_importe = "gasto";
        } else {
            $estilo_importe = "";
        }

        if ($saldo < 0) {
            $estilo_saldo = "gasto";
        } else {
            $estilo_saldo = "";
        }
        echo "
            <tr>
                <td>" . $fecha_corta . "</td>
                <!-- <td>" . $datos["tipo"] . "</td> -->
                <!-- <td>" . $datos["categoria"] . "</td> -->
                <td><a href=\"transaccion_info.php?id=" . $datos["log_id"] . "\">" . $datos["descripcion"] . "</a></td>
                <td><span class=\"" . $estilo_importe . "\">" . number_format($datos["importe"], $decimales, ",", ".") . " " . $datos["divisa_simbolo"] . "</span></td>" . PHP_EOL;
        if ($info_cuenta["cuenta_tipo_id"] == 1) {
            echo "                
                <td><span class=\"" . $estilo_saldo . "\">" . number_format($saldo, $decimales, "," , ".") . " " . $datos["divisa_simbolo"] . "</span></td>" . PHP_EOL;
        }
        echo "
            </tr>" . PHP_EOL;
    }
?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- card -->
                            </div> <!-- col -->
                            <!-- Para un futuro -->
                            <!-- 
                            <div class="col-xl-12 col-lg-12">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Evolución</h6>
                                    </div>
                                    <div class="card-body">
                                        <p>Aquí un gráfico donde se vea la evolución de gastos e ingresos</p>
                                    </div>
                                </div>
                            </div>
                        -->
<?php 
}
?>                            
                        </div> <!-- row -->
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Modal eliminar cuenta-->
        <div class="modal fade" id="eliminar_cuenta" tabindex="-1" role="dialog" aria-labelledby="eliminar_cuenta" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminar_cuenta">Eliminar cuenta</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Pulsando en <strong>Eliminar</strong> se borrará la cuenta seleccionada, así como todas sus transacciones relacionadas. Esta operación no puede deshacerse.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a href="/cuenta_eliminar.php?id=<?php echo $cuenta_id; ?>" class="btn btn-danger" >Eliminar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizados -->
        <script src="js/scripts.js"></script>
    </body>
</html>