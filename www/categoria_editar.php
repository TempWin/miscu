<?php
//
// Formulario para editar una categoría

ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");
require_once("sql.php");

// Obtenerlo por GET
$categoria_id = $_POST["categoria_id"];
$categoria_nombre = $_POST["categoria_nombre"];

if (editar_categoria($categoria_id, $categoria_nombre)) {
    echo "Categoría editada";
} else {
    echo "Error editando categoría";
}

