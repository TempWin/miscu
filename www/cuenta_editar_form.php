<?php
//
// Formulario para crear una cuenta
//
require_once("functions.php");

// Obtener información de cuenta
$cuenta_id = $_GET["id"];
$datos_cuenta = info_cuenta($cuenta_id);
$saldo_inicial = str_replace(".", ",", round($datos_cuenta["saldo_inicial"], 2));
$tipos_cuenta = obtener_tipos_cuenta();
$divisas = obtener_divisas();
?>
        <div class="col-lg-6 col-xl-6 col-sm-12">

            <form action="" method="post" id="form-editar-cuenta">
                <h2>Modificar cuenta</h2>
                <div class="form-group">
                    <input type="hidden" name="cuenta_id" id="cuenta_id" value="<?php echo $cuenta_id; ?>">
                    <label for="cuenta_tipo">Tipo de cuenta</label>
                    <select name="cuenta_tipo_id" class="custom-select">
<?php                    
foreach ($tipos_cuenta as $tipo) {
    // Si la opción de tipo de cuenta coincide con el tipo de cuenta de la cuenta
    // seleccionada, la marcamos por defecto
    if ($tipo["id"] == $datos_cuenta["cuenta_tipo_id"]) {
        echo "
                    <option value=\"" . $datos_cuenta["cuenta_tipo_id"] . "\" selected>" . $datos_cuenta["tipo"] . "</option>" . PHP_EOL;
    } else {
        echo "
                    <option value=\"" . $tipo["id"] . "\">" . $tipo["nombre"] . "</option>" . PHP_EOL;
    }
}
?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre_cuenta" name="nombre_cuenta" required aria-describedby="nombre" value="<?php echo $datos_cuenta["nombre"]; ?>">
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" required aria-describedby="descripcion" value="<?php echo $datos_cuenta["descripcion"]; ?>">
                </div>
                <div class="form-group">
                    <label for="iban">IBAN</label>
                    <input type="text" class="form-control" id="iban" name="iban" aria-describedby="iban" value="<?php echo $datos_cuenta["iban"]; ?>">
                </div>
                <div class="form-group">
                    <label for="bic">BIC</label>
                    <input type="text" class="form-control" id="bic" name="bic" aria-describedby="bic" value="<?php echo $datos_cuenta["bic"]; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Divisa</label>
                    <select name="divisa_id" class="custom-select">
    <?php                    
    foreach ($divisas as $divisa) {
        if ($divisa["id"] == $datos_cuenta["divisa_id"]) {
            echo "
                        <option value=\"" . $datos_cuenta["divisa_id"] . "\" selected>" . $datos_cuenta["divisa_simbolo"] . " (" . $datos_cuenta["divisa"] . ")</option>" . PHP_EOL;
        } else {
            echo "
                        <option value=\"" . $divisa["id"] . "\">" . $divisa["simbolo"] . " (" . $divisa["nombre"] . ")</option>" . PHP_EOL;
        }
    }
    ?>
                    </select>
                </div>
<?php 
// Solo si es una cuenta de activos damos la posibilidad de establecer un saldo inicial
if ($datos_cuenta["cuenta_tipo_id"] == 1) {
?>                
                <div class="form-group">
                    <label for="clave">Saldo inicial</label>
                    <input type="text" class="form-control" id="saldo_inicial" name="saldo_inicial" required value="<?php echo $saldo_inicial; ?>">
                </div>
<?php 
}
?>                
                <a href="#" onclick="editar_cuenta();" type="button" class="btn btn-primary" name="enviar">Guardar</a>
            </form>
        </div> <!-- col -->