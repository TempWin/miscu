<?php
//  Script que genera un CSV con los movimientos de cierta cuenta
//
//  13/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

extract($_POST, EXTR_OVERWRITE);

$movimientos = movimientos_cuenta($cuenta_id, $fecha_inicio, $fecha_fin);
$cuenta = info_cuenta($cuenta_id);
$saldo = obtener_saldo($cuenta_id);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=movimientos_" . $cuenta["nombre"] . ".csv");
header("Pragma: no-cache");
header("Expires: 0");

// Apaño para que Excel detecte correctamente la codificación UTF-8
echo chr(239) . chr(187) . chr(191);

// CSV
// https://tools.ietf.org/html/rfc4180
// Usamos de delimitador el punto y coma para poder
// meter números decimales con la coma como separador
echo "Fecha; Concepto; Importe; Saldo" . PHP_EOL;

foreach ($movimientos as $id => $datos) {

    $importe = $datos["importe"];
    $fecha = new DateTime($datos["fecha"]);
    $fecha_corta = $fecha->format("d/m/Y");
    $descripcion = $datos["descripcion"];
    
    echo "" . $fecha_corta . "; \"" . $descripcion . "\"; \"" . number_format($importe, 2, ",", "") . "\"; \"" . number_format($saldo, 2, ",", "") . "\"" . PHP_EOL;
    $saldo = $saldo - $importe;
}
/*
$debug = get_defined_vars();
print_r($debug);
*/
?>