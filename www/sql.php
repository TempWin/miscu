<?php
$tipos_cuenta_sql = "
    SELECT id,
           nombre
    FROM cuentas_tipos
    ORDER BY nombre
";

$divisas_sql = "
    SELECT id,
           nombre,
           codigo,
           simbolo,
           decimales
    FROM divisas
";

// Listado de cuentas por usuario
$cuentas_listado_sql = "
    SELECT c.id as cuenta_id,
           c.cuenta_tipo_id,
           t.nombre as tipo,
           c.nombre,
           c.fecha_creacion
    FROM cuentas c
    INNER JOIN cuentas_tipos t
       ON c.cuenta_tipo_id = t.id
    WHERE c.usuario_id = :usuario_id
";
?>