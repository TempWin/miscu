<?php
//
//  Página principal 
//
//  * Evolución de gastos / ingresos mes actual
//  * Comparativa gastos / ingresos  
//  * Últimas transacciones
//
session_start();

$usuario_id = "";

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}

require_once("functions.php");
$ano_actual = date("Y");
$mes_actual = date("n");
$dias_mes = date('t', mktime(0, 0, 0, $mes_actual, 1, $ano_actual)); 
$meses = [
    'Enero', 
    'Febrero', 
    'Marzo', 
    'Abril', 
    'Mayo', 
    'Junio', 
    'Julio', 
    'Agosto', 
    'Septiembre', 
    'Octubre', 
    'Noviembre',
    'Diciembre'
];

// Para pintar la gráfica de evolución de saldo con información
// desde el inicio del año hasta el mes en curso
$meses_transcurridos = [];
for ($m = 0; $m < $mes_actual; $m++) {
    $meses_transcurridos[] = $meses[$m];
}

//$mes_actual_nombre = date("F");
// $mes_actual_nombre = strftime("%B", strtotime(date("Y-m-d")));

$categorias_gastos = [];
$categorias_ingresos = [];
$gastos = [];
$ingresos = [];
/*
$transacciones_total = [];
$transacciones_por_categoria = transacciones_categorias($usuario_id);
*/
$mes_actual = (int) date("n");

// Suma de gastos agrupados por categorías
$importe_categorias_gastos = importe_categorias($usuario_id, "gastos", $ano_actual, $mes_actual);

foreach ($importe_categorias_gastos as $id => $datos) {
    if ($id == 0) {
        $categorias_gastos[] = "Otros";
    } else {
        $categorias_gastos[] = $datos["nombre"];
    }
    $gastos[] = round($datos["importe"], 2);
}

// Suma de ingresos agrupados por categorías
$importe_categorias_ingresos = importe_categorias($usuario_id, "ingresos", 2019, $mes_actual);

foreach ($importe_categorias_ingresos as $id => $datos) {
    if ($id == 0) {
        $categorias_ingresos[] = "Otros";
    } else {
        $categorias_ingresos[] = $datos["nombre"];
    }
    $ingresos[] = round($datos["importe"], 2);
}

$categorias_gastos_js = json_encode($categorias_gastos);
$gastos_js = json_encode($gastos);

$categorias_ingresos_js = json_encode($categorias_ingresos);
$ingresos_js = json_encode($ingresos);

$numero_cuentas = numero_cuentas($usuario_id);
$numero_transacciones = numero_transacciones($usuario_id);

$colores = [
    '#f44336', // Rojo        #f44336
    '#e91e63', // rosa
    '#ff9800', // naranja     #f76d3c
    '#ff5722', // naranja oscuro
    '#ffeb3b', // amarillo    #f7d842
    '#ffc107', // ámbar
    '#4caf50', // verde       #98cb4a, #4caf50
    '#8bc34a', // verde claro
    '#cddc39', // lima
    '#2196f3', // azul        #5481e6
    '#03a9f4', // azul claro
    '#3f51b5', // indigo
    '#00bcd4', // cyan
    '#009688', // teal
    '#9c27b0', // morado     #913ccd
    '#673ab7', // morado oscuro
    '#795548', // marrón
    '#9e9e9e', // gris        #839098
    '#607d8b'  // gris azulado
];

// Rojo claro           #f15f74


//$saldo_3 = saldo_cuenta_mensual(3, 2019);
$activos_usuario = cuentas_usuario($usuario_id, 1);

$datasets = [];

foreach ($activos_usuario as $id => $nombre_cuenta) {
    $datasets[$id] = [
        "nombre"    => $nombre_cuenta,
        "saldo"     => saldo_cuenta_mensual($id, $ano_actual)
    ];
}
/*
foreach ($activos_usuario as $id => $nombre_cuenta) {
    $datasets[$id] = [
        "nombre"    => $nombre_cuenta,
        "saldo"     => saldo_cuenta_diario($id, $ano_actual, $mes_actual)
    ];
}
*/
if ($numero_transacciones != 0) {
    // Total de gastos e ingresos en el año actual
    $gastos_ano = total_gastos($usuario_id, $ano_actual);
    if (empty($gastos_ano)) {
        $gastos_total = 0;
    } else {
        $gastos_total = $gastos_ano["total"];
    }
    $gastos_mes = number_format(obtener_gastos_mes($usuario_id, $mes_actual), 2, ",", ".");

    $ingresos_ano = total_ingresos($usuario_id, $ano_actual);
    if (empty($ingresos_ano)) {
        $ingresos_total = 0;
    } else {
        $ingresos_total = $ingresos_ano["total"];
    }

    $ingresos_mes = number_format(obtener_ingresos_mes($usuario_id, $mes_actual), 2, ",", ".");
    $balance = $gastos_total + $ingresos_total;
    if ($balance < 0) {
        $estilo_balance = "negativo";
    } else {
        $estilo_balance = "";
    }


    $cuentas_activos_usuario = listar_cuentas($usuario_id, "activos");

    // Saldo total de todas las cuentas de activos
    $capital = 0.00;
    foreach ($cuentas_activos_usuario as $cuenta_id => $datos) {
        $capital += obtener_saldo($cuenta_id);
    }

    $ultimos_movimientos = ultimos_movimientos_cuentas($usuario_id);
    $estado_cuentas = obtener_estado_cuentas($usuario_id);
    $array_cuentas = [];

    for ($m = 1; $m <= $mes_actual; $m++) {
        foreach ($cuentas_activos_usuario as $id => $datos) {
            $array_cuentas[$m][$id] = [
                "nombre"    => $datos["nombre"],
                "saldo"     => 0
            ];
        }
    }

    $saldo_actual = 0;

    foreach ($cuentas_activos_usuario as $cuenta => $datos) {
        $saldo_actual += obtener_saldo($cuenta);
    }

    $categorias_transacciones = obtener_transacciones_categorizadas($usuario_id);

} /*else {
    $gastos = 0.0;
    $ingresos = 0.0;
    $balance = 0.0;
}
*/
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Cuadro de mando</h1>

                        <div class="row">
<?php 
// Si el usuario no ha creado ninguna cuenta, le mostramos
// información inicial
if ($numero_cuentas == 0) {
?>
                            <div class="col-xl-8 col-lg-7">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">¡Bienvenido a Miscu!</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <p>Antes de empezar a apuntar tus gastos/ingresos, tienes que crear una cuenta:</p>
                                        <ul>
                                            <li><a href="cuentas_activos.php">Cuenta de activos</a></li>
                                            <li><a href="cuentas_gastos.php">Cuenta de gastos</a></li>
                                            <li><a href="cuentas_ingresos.php">Cuenta de ingresos</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- col -->
<?php
} else if ($numero_transacciones == 0) {
?>   
                            <div class="col-xl-8 col-lg-7">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Registra tus movimientos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <p>Ya puedes comenzar a llevar un control del dinero que sale y entra:</p>
                                        <ul>
                                            <li><a href="transaccion_gasto.php">Anotar gasto</a></li>
                                            <li><a href="transaccion_ingreso.php">Anotar ingreso</a></li>
                                            <li><a href="transaccion_transferencia.php">Anotar transferencia</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- col -->
<?php 
} else {
?>                            
                        </div>
                        <div class="row">
                            <!-- Patrimonio: dinero que tenemos en total -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Capital</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo number_format($capital, 2, ",", "."); ?> €</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Balance anual</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><span class="<?php echo $estilo_balance; ?>"><?php echo number_format($balance, 2, ",", "."); ?> €</span></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-balance-scale fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Ingresos mes actual -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Ingresos mes</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $ingresos_mes; ?> €</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-hand-holding-usd  fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Gastos mes actual -->
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-danger shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Gastos mes</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $gastos_mes; ?> €</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-shopping-cart  fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card shadow mb-4">
                                    <!-- Card Header -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Evolución saldo</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <canvas id="grafica-estado"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- row -->
<?php 
/*
// DEBUG
echo "<pre>";
$debug = get_defined_vars();
print_r($debug);
echo "</pre>";
*/
?>                        
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Ingresos vs Gastos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <canvas id="grafica-ingresos-gastos"></canvas>
                                    </div>
                                </div>
                            </div>
<?php
// Si hay transacciones categorizadas, mostramos la gráfica:
if (!empty($categorias_transacciones)) {
?>                            
                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Gastos por categoría</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <canvas id="grafica-categorias-gastos"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6 col-lg-4">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Ingresos por categoría</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <canvas id="grafica-categorias-ingresos"></canvas>
                                    </div>
                                </div>
                            </div>
<?php 
}
?>                            
                        </div> <!-- row -->
                        <div class="row">
                            <div class="col-xs-12 col-lg-8">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Últimos movimientos</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <!-- <th>Tipo</th> -->
                                                    <th>Detalle</th>
                                                    <th>Cuenta</th>
                                                    <th>Importe</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
    foreach ($ultimos_movimientos as $id => $datos) {
        if ($datos["importe"] > 0) {
            $estilo_importe = "";
        } else {
            $estilo_importe = "gasto";
        }

        $fecha = new DateTime($datos["fecha"]);
        $fecha = $fecha->format("d/m/Y");
            
        switch ($datos["tipo_id"]) {
            case 1:
                $icono_tipo_transaccion = "<i class=\"fas fa-long-arrow-alt-left\"></i> ";
                break;
            case 2:
                $icono_tipo_transaccion = "<i class=\"fas fa-long-arrow-alt-right\"></i> ";
                break;
            case 3:
                $icono_tipo_transaccion = "<i class=\"fas fa-exchange-alt\"></i> ";
                break;
        }

    echo "
                                                <tr>
                                                    <td>" . $fecha . "</td>
                                                    <!-- <td>" . $icono_tipo_transaccion . "</td> -->
                                                    <td><a href=\"transaccion_info.php?id=" . $datos["log_id"] . "\">" . $datos["descripcion"] . "</a></td>
                                                    <td><a href=\"cuenta_info.php?id=" . $datos["cuenta_id"] . "\">" . $datos["cuenta"] . "</a></td>
                                                    <td><span class=\"" . $estilo_importe . "\">" . number_format($datos["importe"], $datos["divisa_decimales"], ",", ".") . " " . $datos["divisa"] . "</span></td>
                                                </tr>" . PHP_EOL;
    }
?>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- col -->
                            <div class="col-xs-12 col-lg-4">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Saldo cuentas</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Cuenta</th>
                                                    <th>Saldo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php   
foreach ($cuentas_activos_usuario as $id => $datos_cuenta) {
    $saldo = obtener_saldo($id);
    if ($saldo < 0) {
        $estilo_saldo = "negativo";
    } else {
        $estilo_saldo = "";
    }
    $saldo = number_format($saldo, 2, ",", ".");
    echo "
                                                <tr>
                                                    <td><a href=\"cuenta_info.php?id=" . $id . "\">" . $datos_cuenta["nombre"] . "</a></td>
                                                    <td><span class=\"" . $estilo_saldo . "\">" . $saldo . " €</span></td>
                                                </tr>" . PHP_EOL;
}
?>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
<?php 
}
?>                            
                        </div> <!-- row -->
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Chartjs -->
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <!-- Gráficas -->
        <script>
            var config = {
                type: 'line',
                data: {
                    /*
                    labels: [
                            'Enero', 
                            'Febrero', 
                            'Marzo', 
                            'Abril', 
                            'Mayo', 
                            'Junio', 
                            'Julio', 
                            'Agosto', 
                            'Septiembre', 
                            'Octubre', 
                            'Noviembre',
                            'Diciembre'
                            ]
                            */
                    labels: <?php echo json_encode($meses_transcurridos); ?>,
                            
<?php
/*
$dias_mes_array = [];
for ($d = 1; $d <= $dias_mes; $d++) {
    $dias_mes_array[] = $d;
}
*/
?>                            
                    // labels: <?php //echo json_encode($dias_mes_array); ?>                    ,
                    datasets: [
<?php 
$contador_color = 0;
foreach ($datasets as $cuenta => $datos) {
?>
                                {
                                    data: <?php echo json_encode($datos["saldo"]); ?>,
                                    label: '<?php echo $datos["nombre"]; ?>',
                                    fill: false, // no rellena el área con color
                                    borderColor: '<?php echo $colores[$contador_color]; ?>',
                                    backgroundColor: '<?php echo $colores[$contador_color]; ?>'
                                },
<?php 
    $contador_color++;
}
?>                    
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    title: {
                        display: false,
                        text: 'Evolución del saldo'
                    },
                    legend: {
                        position: 'top',
                        display: true
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = tooltipItems.yLabel;
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                return data.datasets[tooltipItems.datasetIndex].label +': ' + importe + ' €';
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
        };

        var config_gastos_ingresos = {
                type: 'pie',
                data: {
                    
                    datasets: [
                                {
                                    data: [<?php echo $ingresos_total; ?>, <?php echo $gastos_total; ?>],
                                    //label: 'Ingresos',
                                    fill: false, // no rellena el área con color
                                    borderColor: ['<?php echo $colores[6]; ?>', '<?php echo $colores[0]; ?>'],
                                    backgroundColor: ['<?php echo $colores[6]; ?>', '<?php echo $colores[0]; ?>']
                                }
                    ],
                    labels: [
                            'Ingresos', 
                            'Gastos'
                            ]
                },
                options: {
                    responsive: true,
                    title: {
                        display: false,
                        text: 'Ingresos vs Gastos'
                    },
                    legend: {
                        position: 'top',
                        display: false
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                // Pintamos título del dato + dato + €
                                return data.labels[tooltipItems.index] + ': ' + importe + ' €'
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
        };

        // Gráfica de gastos del mes actual por categorías
        var config_gastos = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: <?php echo $gastos_js; ?>,
                        backgroundColor: [
                                '#f44336', // Rojo        #f44336
                                '#e91e63', // rosa
                                '#ff9800', // naranja     #f76d3c
                                '#ff5722', // naranja oscuro
                                '#ffeb3b', // amarillo    #f7d842
                                '#ffc107', // ámbar
                                '#4caf50', // verde       #98cb4a, #4caf50
                                '#8bc34a', // verde claro
                                '#cddc39', // lima
                                '#2196f3', // azul        #5481e6
                                '#03a9f4', // azul claro
                                '#3f51b5', // indigo
                                '#00bcd4', // cyan
                                '#009688', // teal
                                '#9c27b0', // morado     #913ccd
                                '#673ab7', // morado oscuro
                                '#795548', // marrón
                                '#9e9e9e', // gris        #839098
                                '#607d8b'  // gris azulado
                        ],
                        label: 'Gastos'
                    }],
                    labels: <?php echo $categorias_gastos_js; ?>
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Gastos por categorías'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                // Pintamos título del dato + dato + €
                                return data.labels[tooltipItems.index] + ': ' + importe + ' €'
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };

        // Gráfica de ingresos del mes actual por categorías
        var config_ingresos = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: <?php echo $ingresos_js; ?>,
                        backgroundColor: [
                                '#f44336', // Rojo        #f44336
                                '#e91e63', // rosa
                                '#ff9800', // naranja     #f76d3c
                                '#ff5722', // naranja oscuro
                                '#ffeb3b', // amarillo    #f7d842
                                '#ffc107', // ámbar
                                '#4caf50', // verde       #98cb4a, #4caf50
                                '#8bc34a', // verde claro
                                '#cddc39', // lima
                                '#2196f3', // azul        #5481e6
                                '#03a9f4', // azul claro
                                '#3f51b5', // indigo
                                '#00bcd4', // cyan
                                '#009688', // teal
                                '#9c27b0', // morado     #913ccd
                                '#673ab7', // morado oscuro
                                '#795548', // marrón
                                '#9e9e9e', // gris        #839098
                                '#607d8b'  // gris azulado
                        ],
                        label: 'Ingresos'
                    }],
                    labels: <?php echo $categorias_ingresos_js; ?>
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Ingresos por categorías'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                // Pintamos título del dato + dato + €
                                return data.labels[tooltipItems.index] + ': ' + importe + ' €'
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
        window.onload = function() {
            var ctx = document.getElementById('grafica-estado').getContext('2d');
            var ctx_gastos_ingresos = document.getElementById('grafica-ingresos-gastos').getContext('2d');
            var ctx_gastos = document.getElementById('grafica-categorias-gastos').getContext('2d');
            var ctx_ingresos = document.getElementById('grafica-categorias-ingresos').getContext('2d');
            window.myPie = new Chart(ctx_gastos, config_gastos);
            window.myPie = new Chart(ctx_ingresos, config_ingresos);
            window.myBar = new Chart(ctx, config);
            window.myBar = new Chart(ctx_gastos_ingresos, config_gastos_ingresos);
        }
        </script>
    </body>
</html>