<?php
// Script que comprueba y crea usuarios

require_once("functions.php");

if (existe_usuario($usuario, $email)) {
    echo "Ya existe un usuario con ese nombre y/o e-mail";
} else {
    if (crear_usuario($usuario, $email, $clave)) {
        header("Location: registro_exito.php");
    } else {
        echo "Error al crear usuario" . PHP_EOL;
    }
}

?>