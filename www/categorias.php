<?php
//  Página de categorías
//
//      * Listado de categorías
//      * Edición de categorías
//      * Eliminación de categorías
//      * Gráficas de gastos e ingresos
//    

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$categorias_gastos = [];
$categorias_ingresos = [];
$gastos = [];
$ingresos = [];
/*
$transacciones_total = [];
$transacciones_por_categoria = transacciones_categorias($usuario_id);
*/
$ano_actual = date("Y");
$mes_actual = date("n");

$categorias_transacciones = obtener_transacciones_categorizadas($usuario_id);

// Si hay transacciones categorizadas
if (!empty($categorias_transacciones)) {

    // Suma de gastos agrupados por categorías
    $importe_categorias_gastos = importe_categorias($usuario_id, "gastos", $ano_actual, $mes_actual);

    foreach ($importe_categorias_gastos as $id => $datos) {
        if ($id == 0) {
            $categorias_gastos[] = "Otros";
        } else {
            $categorias_gastos[] = $datos["nombre"];
        }
        $gastos[] = round($datos["importe"], 2);
    }

    // Suma de ingresos agrupados por categorías
    $importe_categorias_ingresos = importe_categorias($usuario_id, "ingresos", $ano_actual, $mes_actual);

    foreach ($importe_categorias_ingresos as $id => $datos) {
        if ($id == 0) {
            $categorias_ingresos[] = "Otros";
        } else {
            $categorias_ingresos[] = $datos["nombre"];
        }
        $ingresos[] = round($datos["importe"], 2);
    }
    /*
    foreach ($transacciones_por_categoria as $categoria => $transacciones) {
        $categorias[] = $categoria;
        $transacciones_total[] = $transacciones;
    }
    */

    $categorias_gastos_js = json_encode($categorias_gastos);
    $gastos_js = json_encode($gastos);

    $categorias_ingresos_js = json_encode($categorias_ingresos);
    $ingresos_js = json_encode($ingresos);
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Categorías - Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Categorías</h1>
                        <p>Las categorías sirven para agrupar transacciones y poder tener una visión en conjunto sobre los gastos / ingresos.</p>
                        <p><button onclick="mostrar_insertar_categoria();" class="btn btn-primary">Añadir categoría</button></p>
                        <div id="insertar-categoria" class="row mb-3 ml-0" style="display: none;" >
                            <form>
                                <div class="form-row align-items-center">
                                    <div class="col-xs-6">
                                        <input class="form-control" type="text" name="categoria_nueva" id="categoria_nueva" value="" placeholder="Nueva categoría">
                                    </div>
                                    <div class="col-xs-2">
                                        <a class="btn-light btn-primary-light" href="#" onclick="nueva_categoria(<?php echo $usuario_id; ?>);"><i class="fas fa-check"></i></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="alert alert-success" role="alert" id="insertar-categoria-resultado" style="display: none;">
                        </div>
<?php 
if (count(listar_categorias($usuario_id)) != 0) {
?>                        
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="card shadow">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">Listado de categorías</h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body" id="categorias-listado">
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre categoría</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    $categorias = listar_categorias($usuario_id);

    foreach ($categorias as $categoria) {
        echo "
                                                <tr>
                                                    <td id=\"cat-" . $categoria["id"] . "\"><a href=\"categoria_info.php?id=" . $categoria["id"] . "\">" . $categoria["nombre"] . "</a></td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $categoria["id"] . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_categoria(" . $categoria["id"] . ");\" title=\"Editar la categoría\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"categoria_eliminar.php?id=" . $categoria["id"] . "\"><i class=\"far fa-trash-alt\" title=\"Eliminar la categoría\"></i></a>
                                                        </div>
                                                    </td>
                                                    <!-- <td id=\"edit-" . $categoria["id"] . "\"><a href=\"#\" onclick=\"campo_editable_categoria(" . $categoria["id"] , ");\"><i class=\"far fa-edit\"></i></a></td>
                                                    <td><a href=\"categoria_eliminar.php?id=" . $categoria["id"] . "\"><i class=\"far fa-trash-alt\"></i></td> -->
                                                </tr>" . PHP_EOL;
    }
    echo "
                                            </tbody>
                                        </table>" . PHP_EOL;
?>                                        
                                    </div>
                                </div>
                            </div>
<?php
    // Si hay transacciones categorizadas, mostramos la gráfica:
    if (!empty($categorias_transacciones)) {
?>
                            <!-- Gráficas de categorías -->
                            <div class="col-sm-12 col-lg-6">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="card shadow mb-4">
                                            <!-- Card Header - Dropdown -->
                                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                <h6 class="m-0 font-weight-bold text-primary">Gastos mes actual</h6>
                                            </div>
                                            <!-- Card Body -->
                                            <div class="card-body">
                                                <canvas id="grafica-categorias-gastos"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="card shadow mb-4">
                                            <!-- Card Header - Dropdown -->
                                            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                <h6 class="m-0 font-weight-bold text-primary">Ingresos mes actual</h6>
                                            </div>
                                            <!-- Card Body -->
                                            <div class="card-body">
                                                <canvas id="grafica-categorias-ingresos"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php 
    }
?>                            
                        </div> <!-- row -->
<?php 
}
?>                        
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal eliminar categoría -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar categoría</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">¿Estás seguro de querer eliminar la categoría.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-danger" href="logout.php">Eliminar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizadso -->
        <script src="js/scripts.js"></script>
        <!-- Chartjs -->
        <script src="vendor/chartjs/Chart.bundle.min.js"></script>
        <!-- Gráficas -->
        <script>

        // Gráfica de gastos del mes actual por categorías
        var config_gastos = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: <?php echo $gastos_js; ?>,
                        backgroundColor: [
                                '#f44336', // Rojo
                                '#e91e63', // Rosa
                                '#ff9800', // Naranja
                                '#ff5722', // naranja oscuro
                                '#ffeb3b', // Amarillo
                                '#ffc107', // Ámbar
                                '#4caf50', // Verde
                                '#8bc34a', // Verde claro
                                '#cddc39', // Lima
                                '#2196f3', // Azul
                                '#03a9f4', // Azul claro
                                '#3f51b5', // Índigo
                                '#00bcd4', // Cyan
                                '#009688', // Teal
                                '#9c27b0', // Morado
                                '#673ab7', // Morado oscuro
                                '#795548', // Marrón
                                '#9e9e9e', // Gris
                                '#607d8b'  // Gris azulado
                        ],
                        label: 'Gastos'
                    }],
                    labels: <?php echo $categorias_gastos_js; ?>
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Gastos por categorías'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                // Pintamos título del dato + dato + €
                                return data.labels[tooltipItems.index] + ': ' + importe + ' €'
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };

        // Gráfica de ingresos del mes actual por categorías
        var config_ingresos = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: <?php echo $ingresos_js; ?>,
                        backgroundColor: [
                                '#f44336', // Rojo
                                '#e91e63', // Rosa
                                '#ff9800', // Naranja
                                '#ff5722', // naranja oscuro
                                '#ffeb3b', // Amarillo
                                '#ffc107', // Ámbar
                                '#4caf50', // Verde
                                '#8bc34a', // Verde claro
                                '#cddc39', // Lima
                                '#2196f3', // Azul
                                '#03a9f4', // Azul claro
                                '#3f51b5', // Índigo
                                '#00bcd4', // Cyan
                                '#009688', // Teal
                                '#9c27b0', // Morado
                                '#673ab7', // Morado oscuro
                                '#795548', // Marrón
                                '#9e9e9e', // Gris
                                '#607d8b'  // Gris azulado
                        ],
                        label: 'Ingresos'
                    }],
                    labels: <?php echo $categorias_ingresos_js; ?>
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Ingresos por categorías'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(tooltipItems, data) { 
                                var importe = data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
                                // Sustituimos los puntos por las comas como separador de decimales
                                importe = String(importe).replace(".", ",");
                                // Pintamos título del dato + dato + €
                                return data.labels[tooltipItems.index] + ': ' + importe + ' €'
                            }
                        }
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
        window.onload = function() {
            var ctx_gastos = document.getElementById('grafica-categorias-gastos').getContext('2d');
            var ctx_ingresos = document.getElementById('grafica-categorias-ingresos').getContext('2d');
            window.myPie = new Chart(ctx_gastos, config_gastos);
            window.myPie = new Chart(ctx_ingresos, config_ingresos);
        }
        </script>
    </body>
</html>