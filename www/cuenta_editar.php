<?php
//
// Formulario para editar una cuenta
//

ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");

foreach ($_POST as $clave => $valor) {
    if ($valor == "") {
        $$clave = null;
    } else {
        $$clave = $valor;
    }
}

if (!isset($saldo_inicial)) {
    $saldo_inicial = 0;
} else {
    // Convertimos los decimales en la notación inglesa para poder
    // introducirlo sin problemas en la base de datos
    $saldo_inicial = (float) str_replace(",", ".", $saldo_inicial);
}

if (editar_cuenta($cuenta_id, $cuenta_tipo_id, $nombre_cuenta, $iban, $bic, $saldo_inicial, $divisa_id, $descripcion)) {
    echo "Cuenta editada";
} else {
    echo "Error al editar cuenta";
}
?>
