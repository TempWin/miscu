// Sustituye el contenido de la celda con el nombre de la categoría
// por una etiequeta input para poder modificarlo
function campo_editable_categoria(id) {
    var campo = $("td#cat-" + id);
    var boton = $("a#edit-" + id);
    var categoria = campo.text();
    console.log(campo);
    campo.html("<input type=\"text\" class=\"form-control\" id=\"categoria_nombre_id_" + id + "\" name=\"categoria_nombre_id_" + id + "\" value=\"" + categoria + "\">");
    boton.replaceWith("<a id=\"edit-" + id + "\" href=\"#\" class=\"btn btn-default\" onclick=\"editar_categoria(" + id + ");\"><i class=\"fas fa-check\"></i></a>");
}

// Sustituye el contenido de la celda con el nombre del presupuesto
// por una etiqueta input para poder modificarlo
function campo_editable_presupuesto(id) {
    var campo = $("td#prep-" + id);
    var boton = $("a#edit-" + id);
    var presupuesto = campo.text();
    console.log(campo);
    campo.html("<input type=\"text\" class=\"form-control\" id=\"presupuesto_nombre_id_" + id + "\" name=\"presupuesto_nombre_id_" + id + "\" value=\"" + presupuesto + "\">");
    boton.replaceWith("<a id=\"edit-" + id + "\" href=\"#\" class=\"btn btn-default\" onclick=\"editar_presupuesto(" + id + ");\"><i class=\"fas fa-check\"></i></a>");
}

// Sustituye el contenido de la celda con el nombre de la hucha
// por una etiqueta input para poder modificarlo
function campo_editable_hucha(id) {
    var campo = $("td#hucha-" + id);
    var boton = $("a#edit-" + id);
    var hucha = campo.text();
    console.log(campo);
    campo.html("<input type=\"text\" class=\"form-control\" id=\"hucha_nombre_id_" + id + "\" name=\"hucha_nombre_id_" + id + "\" value=\"" + hucha + "\">");
    boton.replaceWith("<a id=\"edit-" + id + "\" href=\"#\" class=\"btn btn-default\" onclick=\"editar_hucha(" + id + ");\"><i class=\"fas fa-check\"></i></a>");

}

function editar_categoria(id) {
    var categoria_id = id;
    var categoria_nombre = $("input#categoria_nombre_id_" + id).val();
    var boton = $("a#edit-" + id);
    var campo = $("td#cat-" + id);
    console.log("id: " + categoria_id + " - nombre: " + categoria_nombre);
    
    $.ajax({
        method: "post",
        url: "categoria_editar.php",
        data: {
            categoria_id: categoria_id,
            categoria_nombre: categoria_nombre
        }
    })
        .done(function(msg) {
            console.log(msg);
            campo.html(categoria_nombre);
            boton.replaceWith("<a id=\"edit-" + id + "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_categoria(" + id + ");\"><i class=\"far fa-edit\"></i></a>");
        })
        .fail(function(msg) {
            alert("Error editar_categoria()");
            console.log(msg)
        });
}

function editar_presupuesto(id) {
    var presupuesto_id = id;
    var presupuesto_nombre = $("input#presupuesto_nombre_id_" + id).val();
    var boton = $("a#edit-" + id);
    var campo = $("td#prep-" + id);
    console.log("id: " + presupuesto_id + " - nombre: " + presupuesto_nombre);
    
    $.ajax({
        method: "post",
        url: "presupuesto_editar.php",
        data: {
            presupuesto_id: presupuesto_id,
            presupuesto_nombre: presupuesto_nombre
        }
    })
        .done(function(msg) {
            console.log(msg);
            campo.html(presupuesto_nombre);
            //boton.replaceWith("<a id=\"edit-" + id + "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_presupuesto(" + id + ");\"><i class=\"far fa-edit\"></i></a>");
            location.href = "presupuestos.php";
        })
        .fail(function(msg) {
            alert("Error editar_presupuesto()");
            console.log(msg)
        });
}

function editar_hucha(id) {
    var hucha_id = id;
    var hucha_nombre = $("input#hucha_nombre_id_" + id).val();
    var boton = $("a#edit-" + id);
    var campo = $("td#hucha-" + id);
    console.log("id: " + hucha_id + " - nombre: " + hucha_nombre);
    
    $.ajax({
        method: "post",
        url: "hucha_editar.php",
        data: {
            hucha_id: hucha_id,
            hucha_nombre: hucha_nombre
        }
    })
        .done(function(msg) {
            console.log(msg);
            campo.html(hucha_nombre);
            //boton.replaceWith("<a id=\"edit-" + id + "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_hucha(" + id + ");\"><i class=\"far fa-edit\"></i></a>");
            location.href = "huchas.php";
        })
        .fail(function(msg) {
            alert("Error editar_hucha()");
            console.log(msg)
        });
}

function mostrar_insertar_categoria() {
    var input_insertar_categoria = $("#insertar-categoria").toggle();
}

function mostrar_insertar_presupuesto() {
    var input_insertar_presupuesto = $("#insertar-presupuesto").toggle();
}

function mostrar_insertar_hucha() {
    var input_insertar_hucha = $("#insertar-hucha").toggle();
}

function mostrar_insertar_cuenta(cuenta_tipo, usuario_id) {
    // tipo_cuenta:
    //  1 - Activos
    //  2 - Ingresos
    //  3 - Gastos
    var form_insertar_cuenta = $("#insertar-cuenta").load("cuenta_form.php?tipo=" + cuenta_tipo + "&usuario=" + usuario_id).toggle();
}

function mostrar_editar_cuenta(cuenta_id) {
    var form_editar_cuenta = $("#editar-cuenta").load("cuenta_editar_form.php?id=" + cuenta_id).toggle();
}

function mostrar_editar_transaccion(transaccion_log_id) {
    var form_editar_transaccion = $("#editar-transaccion").load("transaccion_editar_form.php?id=" + transaccion_log_id).toggle();
}

//
//      Inserta una nueva categoría
//
//        * usuario_id (int): ID del usuario que crea la categoría
//
function nueva_categoria(usuario_id) {
    var categoria_nombre = $("#categoria_nueva").val();
    var resultado = $("#insertar-categoria-resultado");
    var listado_categorias = $("#categorias-listado");
    console.log(categoria_nombre);
    $.ajax({
        method: "post",
        url: "categoria_crear.php",
        data: {
            categoria_nombre: categoria_nombre,
            usuario_id: usuario_id
        }
    })
        .done(function(msg) {
            mostrar_insertar_categoria();
            console.log(msg);
            resultado.html(msg).toggle();
            //listado_categorias.load("categorias_listado.php");
            //$("html").load("categorias.php");
            location.href = "categorias.php";
        })
        .fail(function(msg) {
            console.log("Error nueva_categoria()");
            console.log(msg);
        });
}

//
//      Inserta un nuevo presupuesto
//
//        * usuario_id (int): ID del usuario que crea el presupuesto
//
function nuevo_presupuesto(usuario_id) {
    var presupuesto_nombre = $("#presupuesto_nuevo").val();
    var resultado = $("#insertar-presupuesto-resultado");
    var listado_presupuestos = $("#presupuestos-listado");

    console.log(presupuesto_nombre);
    $.ajax({
        method: "post",
        url: "presupuesto_crear.php",
        data: {
            presupuesto_nombre: presupuesto_nombre,
            usuario_id: usuario_id
        }
    })
        .done(function(msg) {
            mostrar_insertar_presupuesto();
            console.log(msg);
            resultado.html(msg).toggle();
            //listado_presupuestos.load("presupuestos_listado.php");
            location.href = "presupuestos.php";
        })
        .fail(function(msg) {
            console.log("Error nuevo_presupuesto()");
            console.log(msg);
        });
}

//
//      Inserta una nueva cuenta
//      
//      * usuario_id (int): ID del usuario que crea la cuenta
//      * cuenta_tipo (int): ID del tipo de cuenta (1: activos, 2:ingresos, 3: gastos)
//
function nueva_cuenta(usuario_id, cuenta_tipo) {
    console.log("usuario_id: " + usuario_id + " - cuenta_tipo: " + cuenta_tipo);
    var datos = $("#form-cuenta-nueva").serialize();
    var resultado = $("#insertar-cuenta-resultado");
    var listado_cuentas = $("#cuentas-listado");

    console.log(datos);
    $.ajax({
        method: "post",
        url: "cuenta_crear.php",
        data: datos
    })
    .done(function(msg) {
        // Si todo va bien, ocultamos el formulario y mostramos
        // el resultado de la operación
        mostrar_insertar_cuenta(cuenta_tipo, usuario_id);
        console.log(msg);
        resultado.html(msg).show();
        //listado_cuentas.load("cuentas_listado.php?id=" + cuenta_tipo);
        if (cuenta_tipo == 1) {
            location.href = "cuentas_activos.php";
        } else if (cuenta_tipo == 2) {
            location.href = "cuentas_ingresos.php";
        } else {
            location.href = "cuentas_gastos.php";
        }
    })
    .fail(function(msg) {
        console.log("Error nueva_cuenta()");
        console.log(msg);
    });
}

//
//  Modifica una cuenta existente
//
//
//
function editar_cuenta(cuenta_id) {
    var datos = $("#form-editar-cuenta").serialize();
    var resultado = $("#editar-cuenta-resultado");
    var cuenta_id = $("#cuenta_id").val();

    console.log(datos);

    $.ajax({
        method: "post",
        url: "cuenta_editar.php",
        data: datos
    })
    .done(function(msg) {
        // Si todo va bien, ocultamos el formulario y mostramos
        // el resultado de la operación
        mostrar_editar_cuenta(cuenta_id);
        console.log(msg);
        resultado.html(msg).show();
        location.href = "cuenta_info.php?id=" + cuenta_id;
    })
    .fail(function(msg) {
        console.log("Error editar_cuenta()");
        console.log(msg);
    });
}

//
//  Modificar una transacción
//
function editar_transaccion(transaccion_log_id) {
    var datos = $("#form-editar-transaccion").serialize();
    var resultado = $("#editar-transaccion-resultado");
    var transaccion_info = $("#transaccion-info");
    var transaccion_info_meta = $("#transaccion-info-meta");
    var transaccion_log_id = $("#transaccion_log_id").val();

    console.log(datos);
    console.log(transaccion_log_id);

    $.ajax({
        method: "post",
        url: "transaccion_editar.php",
        data: datos
    })
    .done(function(msg) {
        // Si todo va bien, ocultamos el formulario y mostramos
        // el resultado de la operación
        mostrar_editar_transaccion(transaccion_log_id);
        console.log(msg);
        console.log(transaccion_log_id);
        location.href = "transaccion_info.php?id=" + transaccion_log_id;
        resultado.html(msg).show();
    })
    .fail(function(msg) {
        console.log("Error editar_transaccion()");
        console.log(msg);
    });

}

function mostrar_divisa_extranjera() {
    var campos_divisa_extranjera = $(".divisa-extranjera").toggle();
}

// Actualiza el presupuesto del mes actual
function actualizar_importe_presupuesto(presupuesto_id) {
    var importe = $("#presupuesto_importe_id_" + presupuesto_id).val();

    $.ajax({
        method: "post",
        url: "presupuesto_importe_actualizar.php",
        data: {
            "presupuesto_id": presupuesto_id,
            "importe": importe
        }
    })
    .done(function(msg) {
        // Si todo va bien, ocultamos el formulario y mostramos
        // el resultado de la operación
        //mostrar_editar_transaccion(transaccion_log_id);
        console.log("OK actualizar_importe_presupuesto()");
        location.href = "presupuestos.php";
        //resultado.html(msg).show();
    })
    .fail(function(msg) {
        console.log("Error actualizar_importe_presupuesto()");
        console.log(msg);
    });

    console.log(importe);
}

function editar_perfil() {
    var datos = $("#form-perfil").serialize();
    var resultado = $("#actualizar-perfil-resultado");

    console.log(datos);

    $.ajax({
        method: "post",
        url: "perfil_editar.php",
        data: datos
    })
    .done(function(msg) {
        // Si todo va bien, ocultamos el formulario y mostramos
        // el resultado de la operación
        //mostrar_editar_transaccion(transaccion_log_id);
        console.log(msg);
        resultado.html("Información actualizada correctamente").toggle();
        console.log("OK editar_perfil()");
        //resultado.html(msg).show();
    })
    .fail(function(msg) {
        console.log("Error editar_perfil()");
        console.log(msg);
    });

}

//
//      Inserta una nueva hucha
//
//
function nueva_hucha() {
    var datos = $("#form-hucha-nueva").serialize();
    var resultado = $("#insertar-hucha-resultado");
    var listado_huchas = $("#huchas-listado");

    console.log(datos);
    $.ajax({
        method: "post",
        url: "hucha_crear.php",
        data: datos
    })
        .done(function(msg) {
            mostrar_insertar_hucha();
            console.log(msg);
            resultado.html(msg).toggle();
            //listado_huchas.load("huchas_listado.php"); // Pendiente de hacer
            location.href = "huchas.php";

        })
        .fail(function(msg) {
            console.log("Error nueva_hucha()");
            console.log(msg);
        });
}

function movimiento_hucha() {
    
    var datos = $(".form-hucha").serialize();

    console.log(datos);

    $.ajax({
        method: "post",
        url: "hucha_movimiento_crear.php",
        data: datos
    })
        .done(function(msg) {
            //mostrar_insertar_hucha();
            console.log(msg);
            //resultado.html(msg).toggle();
            listado_huchas.load("huchas_listado.php"); // Pendiente de hacer

        })
        .fail(function(msg) {
            console.log("Error movimiento_hucha()");
            console.log(msg);
        });
}