<?php
    //
    // Formulario para crear una cuenta
    //
    // ToDo:
    //      - Conseguir el ID del usuario

ini_set("display_errors", 1);
error_reporting(-1);
require_once("functions.php");
require_once("sql.php");

/*
// DEBUG
echo "<pre>" . PHP_EOL;
print_r($_POST);
echo "</pre>" . PHP_EOL;
exit();
*/

$categoria_nombre = $_POST["categoria_nombre"];
$usuario_id = $_POST["usuario_id"];

if (crear_categoria($usuario_id, $categoria_nombre)) {
    echo "Categoría creada";
} else {
    echo "Error al crear categoría" . PHP_EOL;
}
?>
