<?php 
// Listado de presupuestos del usuario
//
//  06/03/2019

session_start();
ini_set("display_errors", 1);
error_reporting(-1);

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

$cuentas = listar_cuentas($usuario_id, "activos");
$cuentas_id = [];

foreach ($cuentas as $id => $datos) {
    $cuentas_id[] = $id;
}

$huchas = [];

foreach ($cuentas_id as $cuenta_id) {
    $hucha = listar_huchas($cuenta_id);
    if (!empty($hucha)) {
        $huchas[$cuenta_id] = $hucha;
    }
}
?>
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Objetivo</th>
                                                    <th>Ahorrado</th>
                                                    <th>Restante</th>
                                                    <th>Acción</th>
                                                    <!--<th>Editar</th>
                                                    <th>Borrar</th>-->
                                                <tr>
                                            </thead>
                                            <tbody>
<?php
    foreach ($huchas as $cuenta_id => $datos) {
        foreach ($datos as $hucha_id => $datos_hucha) {
            $ahorrado = obtener_ahorro_hucha($hucha_id);
            $importe_objetivo = $datos_hucha["importe_objetivo"];
            $restante = $importe_objetivo - $ahorrado;
            $importe_objetivo = number_format($importe_objetivo, 2, ",", ".");
            $ahorrado = number_format($ahorrado, 2, ",", ".");
            $restante = number_format($restante, 2, ",", ".");
        echo "
                                                <tr>
                                                    <td><a href=\"hucha_info.php?id=" . $hucha_id . "\">" . $datos_hucha["nombre"] . "</a></td>
                                                    <td>" . $importe_objetivo . " €</td>
                                                    <td>
                                                            <a href=\"hucha_movimiento_crear.php?id=" . $hucha_id . "&t=1\"><i class=\"fas fa-plus\"></i></a>
                                                            " . $ahorrado . " €
                                                            <a href=\"hucha_movimiento_crear.php?id=" . $hucha_id . "&t=0\"><i class=\"fas fa-minus\"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>" . $restante . " €</td>
                                                    <td>
                                                        <div class=\"btn-group btn-group-sm\" role=\"group\" aria-label=\"opciones\">
                                                            <a id=\"edit-" . $hucha_id . "\" class=\"btn btn-primary\" href=\"#\" onclick=\"campo_editable_presupuesto(" . $hucha_id . ");\"><i class=\"far fa-edit\"></i></a>
                                                            <a class=\"btn btn-danger\" href=\"hucha_eliminar.php?id=" . $hucha_id . "\"><i class=\"far fa-trash-alt\"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>" . PHP_EOL;
        }
    }
    echo "
                                            </tbody>
                                        </table>" . PHP_EOL;
?>                                        