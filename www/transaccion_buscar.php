<?php
//  Página de búsqueda de transacciones
//
//  21/02/2019

session_start();

if(!isset($_SESSION["usuario_id"])) {
    header("Location: login.php");
} else {
    $usuario_id = $_SESSION["usuario_id"];
}
require_once("functions.php");

if (!isset($_POST["busqueda"])) {
    $error = true;
} else {
    $busqueda = $_POST["busqueda"];
    $resultados_busqueda = buscar_transacciones($busqueda);
    $error = false;
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <!-- Sidebar -->
<?php
require_once("sidebar.php");
?>          
            <!-- Sidebar -->  
            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">
                <!-- Main Content -->
                <div id="content">
                    <!-- Topbar -->
<?php 
require_once("topbar.php");
?>                    
                    <!-- End of Topbar -->
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <h1 class="h3 mb-4 text-gray-800">Búsqueda de transacciones</h1>
<?php 
if ($error) {
?>           
                        <div class="alert alert-warning" role="alert">
                            Debes introducir un concepto a buscar
                        </div>
<?php 
} else {
?>
                        <div class="row">
                            <!-- Area Chart -->
                            <div class="col-xl-7 col-lg-7">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary">Resultados de la búsqueda</h6>
                                    </div>
                                    <div class="card-body">
<?php 
    if (empty($resultados_busqueda)) {
?>                                
                                        <p>No se ha encontrado ninguna transacción por ese término de búsqueda</p>
<?php 
    } else {
?>                                                
                                        <table class="table table-bt0">
                                            <thead>
                                                <tr>
                                                    <th>Descripción</th>
                                                    <th>Fecha</th>
                                                    <th>Importe</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php 
        foreach ($resultados_busqueda as $datos) {
            $id = $datos["id"];
            $descripcion = $datos["descripcion"];
            $fecha = new DateTime($datos["fecha"]);
            $fecha_corta = $fecha->format("d/m/Y");
            $importe = $datos["importe"];
            if ($importe < 0) {
                $estilo_importe = "gasto";
            } else {
                $estilo_importe = "";
            }
            $importe = number_format($importe, 2, ",", ".");
            $divisa = $datos["divisa"];
            echo "
                                                <tr>
                                                    <td><a href=\"transaccion_info.php?id=" . $id . "\">" . $descripcion . "</a></td>
                                                    <td>" . $fecha_corta . "</td>
                                                    <td><span class=\"" . $estilo_importe . "\">" . $importe . " " . $divisa . "</span></td>
                                                </tr>" . PHP_EOL;
        }
?>                                        
                                                </tbody>
                                            </table>
<?php 
    }
?>                                        
                                    </div>
                                </div> <!-- card -->
                            </div>
                        </div> <!-- row -->
<?php 
}
?>                                                
                    </div> <!-- /.container-fluid -->
                </div> <!-- End of Main Content -->
                <!-- Footer -->
<?php
require_once("footer.php");
?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Selecciona <strong>Salir</strong> si quieres cerrar la sesión.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-primary" href="logout.php">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Scripts personalizadso -->
        <script src="js/scripts.js"></script>
    </body>
</html>
