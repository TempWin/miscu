<?php
//
//  Página de registro de usuarios
//
//  
session_start();

if ($_POST) {
    foreach ($_POST as $key => $value) {
        $$key = $value;
    }

    if (preg_match("/\\s/", $usuario)) {
        $error = "<div class=\"alert alert-danger\" role=\"alert\">No se puede introducir un nombre de usuario vacío o con espacios</div>";
    }

    // Por si el navegador no entiende el HTML5 del atributo type="email"
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = "<div class=\"alert alert-danger\" role=\"alert\">Formato incorrecto de e-mail</div>";
    } 

    if ($clave != $clave_confirmacion) {
        $error = "<div class=\"alert alert-danger\" role=\"alert\">Las contraseñas no coinciden</div>";
    }

    if (!isset($error)) {
        require_once("usuario_crear.php");
    }
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Miscu</title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <!-- Estilos personalizados -->
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body class="login-bg">
        <div class="container">
            <!-- Outer Row -->
            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-12 col-md-9">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Crea una cuenta</h1>
                                        </div>

                                        <form class="user" action="" method="post">
                                            <?php if (isset($error)) echo $error; ?>
                                            <div class="form-group">
                                                <input type="text" class="form-control form-control-user" id="usuario" name="usuario" placeholder="Usuario" value="<?php if(isset($_POST["usuario"])) echo $_POST["usuario"]; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="E-mail"  value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>" required>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <input type="password" class="form-control form-control-user" id="clave" name="clave" placeholder="Contraseña"  value="<?php if(isset($_POST["clave"])) echo $_POST["clave"]; ?>" required>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="password" class="form-control form-control-user" id="clave_confirmacion" name="clave_confirmacion" placeholder="Repite contraseña" value="<?php if(isset($_POST["clave_confirmacion"])) echo $_POST["clave_confirmacion"]; ?>" required>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Crear">
                                        </form>
                                        <hr>
                                        <div class="text-center">
                                            <a class="small" href="login.php">¿Ya tienes cuenta? ¡Inicia sesión!</a>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- row -->
                        </div>
                    </div> <!-- card -->
                </div> <!-- col -->
            </div> <!-- row -->
        </div> <!-- container -->
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
    </body>
</html>