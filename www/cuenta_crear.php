<?php
//
// Formulario para crear una cuenta
//
require_once("functions.php");

foreach ($_POST as $clave => $valor) {
    if ($valor == "") {
        $$clave = null;
    } else {
        $$clave = $valor;
    }
}

// Para cuentas de gastos o ingresos, no permitimos al usuario
// introducir un saldo inicial, así que lo inicializamos a 0
if (!isset($saldo_inicial)) {
    $saldo_inicial = 0;
}

// Convertimos los decimales en la notación inglesa para poder
// introducirlo sin problemas en la base de datos
$saldo_inicial = (float) str_replace(",", ".", $saldo_inicial);

if (crear_cuenta($cuenta_tipo_id, $nombre_cuenta, $usuario_id, $iban, $bic, $saldo_inicial, $divisa_id, $descripcion)) {
    echo "Cuenta creada";
} else {
    echo "Error al crear cuenta";
}
?>
