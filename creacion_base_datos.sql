-- ------------------------------------------------------------------------

--              Script de creación de tablas e inicialización para 
--              la instalación de Miscu

-- ------------------------------------------------------------------------

-- Si quieres crear también la base de datos y un usuario con acceso a ella,
-- descomenta las siguientes líneas:

-- CREATE DATABASE miscu_db CHARACTER SET utf8 COLLATE utf8_spanish_ci;
-- CREATE USER miscu_db_user@localhost IDENTIFIED BY 'contraseña';
-- GRANT ALL PRIVILEGES ON miscu_db.* TO miscu_db_user@localhost;
-- FLUSH PRIVILEGES;

--  Modificar para adaptar:
--
--      * miscu_db: nombre de la base de datos
--      * miscu_db_user: nombre del usuario de la base de datos
--      * localhost: servidor en el que se encuentra MariaDB

-- #########################################################################

--                          CREACIÓN DE TABLAS

-- #########################################################################

-- **** ¡IMPORTANTE!: Hay que seleccionar previamente la base de datos ****


-- usuarios
-- -------------------------------------------------------------------------

CREATE TABLE `usuarios` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`usuario` VARCHAR(50) NOT NULL COMMENT 'Nick del usuario' COLLATE
'utf8mb4_spanish_ci',
`email` VARCHAR(150) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`clave` VARCHAR(255) NOT NULL COMMENT 'Hash con la clave del usuario' COLLATE
'utf8mb4_spanish_ci',
`fecha_registro` DATETIME NOT NULL COMMENT 'Fecha de creación del usuario',
PRIMARY KEY (`id`)
)
COMMENT='Usuarios de la aplicación'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- usuarios_info
-- -------------------------------------------------------------------------

CREATE TABLE `usuarios_info` (
`usuario_id` INT(11) NOT NULL,
`avatar` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`nombre` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`apellidos` VARCHAR(150) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`fecha_nacimiento` DATE NULL DEFAULT NULL,
INDEX `FK_usuarios_info` (`usuario_id`),
CONSTRAINT `FK_usuarios_info` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (
`id`) ON DELETE CASCADE
)
COMMENT='Información adicional sobre los usuarios'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- usuarios_log
-- -------------------------------------------------------------------------

CREATE TABLE `usuarios_log` (
`usuario_id` INT(11) NOT NULL,
`fecha_login` DATETIME NOT NULL,
INDEX `FK_usuarios_log` (`usuario_id`),
CONSTRAINT `FK_usuarios_log` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`
id`) ON DELETE CASCADE
)
COMMENT='Registro de inicios de sesión (login) de los usuarios'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- divisas
-- -------------------------------------------------------------------------

CREATE TABLE `divisas` (
`id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`codigo` VARCHAR(3) NULL DEFAULT NULL COMMENT 'ISO 4217' COLLATE
'utf8mb4_spanish_ci',
`simbolo` VARCHAR(10) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`decimales` TINYINT(4) NOT NULL DEFAULT '2',
PRIMARY KEY (`id`)
)
COMMENT='Información sobre las diferentes divisas (tipo de monedas)'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- cuentas_tipos
-- -------------------------------------------------------------------------

CREATE TABLE `cuentas_tipos` (
`id` TINYINT(4) NOT NULL AUTO_INCREMENT,
`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`descripcion` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
PRIMARY KEY (`id`)
)
COMMENT='Tipos de cuenta (corriente, de ahorros, efectivo...)'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- cuentas
-- -------------------------------------------------------------------------

CREATE TABLE `cuentas` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`cuenta_tipo_id` TINYINT(4) NOT NULL,
`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`usuario_id` INT(11) NOT NULL COMMENT 'Creador y usuario de la cuenta',
`iban` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`bic` VARCHAR(11) NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`fecha_cierre` DATETIME NULL DEFAULT NULL,
`saldo_inicial` DECIMAL(22,12) NOT NULL,
`divisa_id` SMALLINT(6) NOT NULL,
`descripcion` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
PRIMARY KEY (`id`),
INDEX `FK_cuentas_tipos` (`cuenta_tipo_id`),
INDEX `FK_cuentas_usuarios` (`usuario_id`),
INDEX `FK_cuentas_divisas` (`divisa_id`),
CONSTRAINT `FK_cuentas_divisas` FOREIGN KEY (`divisa_id`) REFERENCES `divisas` (
`id`),
CONSTRAINT `FK_cuentas_tipos` FOREIGN KEY (`cuenta_tipo_id`) REFERENCES
`cuentas_tipos` (`id`),
CONSTRAINT `FK_cuentas_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios`
(`id`) ON DELETE CASCADE
)
COMMENT='Información de las cuentas'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- categorias
-- -------------------------------------------------------------------------

CREATE TABLE `categorias` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`usuario_id` INT(11) NOT NULL,
`fecha_creacion` DATETIME NOT NULL,
`fecha_modificacion` DATETIME NOT NULL,
PRIMARY KEY (`id`),
INDEX `FK_categorias_usuarios` (`usuario_id`),
CONSTRAINT `FK_categorias_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES
`usuarios` (`id`) ON DELETE CASCADE
)
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- presupuestos
-- -------------------------------------------------------------------------

CREATE TABLE `presupuestos` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`nombre` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`usuario_id` INT(11) NOT NULL,
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`fecha_fin` DATETIME NULL DEFAULT NULL,
`descripcion` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
PRIMARY KEY (`id`),
INDEX `FK_presupuestos_usuarios` (`usuario_id`),
CONSTRAINT `FK_presupuestos_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES
`usuarios` (`id`) ON DELETE CASCADE
)
COMMENT='Para agrupar las transacciones habituales. '
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- presupuestos_limites
-- -------------------------------------------------------------------------

CREATE TABLE `presupuestos_limites` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`presupuesto_id` INT(11) NOT NULL,
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`fecha_inicio` DATE NOT NULL COMMENT 'Fecha en la que comienza el presupuesto',
`fecha_fin` DATE NOT NULL COMMENT 'Fecha fin del presupuesto',
`importe` DECIMAL(22,12) NOT NULL,
PRIMARY KEY (`id`),
INDEX `FK_presupuestos_limites` (`presupuesto_id`),
CONSTRAINT `FK_presupuestos_limites` FOREIGN KEY (`presupuesto_id`) REFERENCES
`presupuestos` (`id`) ON DELETE CASCADE
)
COMMENT='Importes de los presupuestos y su duración'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- transacciones_tipos
-- -------------------------------------------------------------------------

CREATE TABLE `transacciones_tipos` (
`id` TINYINT(4) NOT NULL AUTO_INCREMENT,
`tipo` VARCHAR(25) NOT NULL COLLATE 'utf8mb4_spanish_ci',
PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- transacciones_log
-- -------------------------------------------------------------------------

CREATE TABLE `transacciones_log` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`usuario_id` INT(11) NOT NULL,
`fecha` DATETIME NOT NULL COMMENT 'Fecha de la transacción (no tiene por qué
coincidir con el momento actual)',
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`tipo_id` TINYINT(4) NOT NULL,
`divisa_id` SMALLINT(6) NOT NULL,
`descripcion` TEXT NOT NULL COLLATE 'utf8mb4_spanish_ci',
PRIMARY KEY (`id`),
INDEX `FK_transacciones_usuarios` (`usuario_id`),
INDEX `FK_transacciones_tipos` (`tipo_id`),
INDEX `FK_transacciones_divisas` (`divisa_id`),
CONSTRAINT `FK_transacciones_divisas` FOREIGN KEY (`divisa_id`) REFERENCES
`divisas` (`id`),
CONSTRAINT `FK_transacciones_tipos` FOREIGN KEY (`tipo_id`) REFERENCES
`transacciones_tipos` (`id`),
CONSTRAINT `FK_transacciones_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES
`usuarios` (`id`)
)
COMMENT='La transacción desde el punto de vista del usuario, es decir, será un
ingreso, gasto o transferencia. No se guarda información sobre la cantidad ni el abono
e ingresos en las cuentas relacionadas.'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- transacciones
-- -------------------------------------------------------------------------

CREATE TABLE `transacciones` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`cuenta_id` INT(11) NOT NULL,
`transaccion_log_id` INT(11) NOT NULL,
`importe` DECIMAL(22,12) NOT NULL,
`divisa_id` SMALLINT(6) NOT NULL,
`importe_extranjero` DECIMAL(22,12) NULL DEFAULT NULL,
`divisa_extranjera_id` SMALLINT(6) NULL DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX `FK_transacciones_transacciones_log` (`transaccion_log_id`),
INDEX `FK_transacciones_divisas_nolog` (`divisa_id`),
INDEX `FK_transacciones_divisas_extranjeras` (`divisa_extranjera_id`),
INDEX `FK_transacciones_cuentas` (`cuenta_id`),
CONSTRAINT `FK_transacciones_cuentas` FOREIGN KEY (`cuenta_id`) REFERENCES
`cuentas` (`id`) ON DELETE CASCADE,
CONSTRAINT `FK_transacciones_divisas_extranjeras` FOREIGN KEY (
`divisa_extranjera_id`) REFERENCES `divisas` (`id`),
CONSTRAINT `FK_transacciones_divisas_nolog` FOREIGN KEY (`divisa_id`) REFERENCES
`divisas` (`id`),
CONSTRAINT `FK_transacciones_transacciones_log` FOREIGN KEY (`transaccion_log_id`)
REFERENCES `transacciones_log` (`id`) ON DELETE CASCADE
)
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- transacciones_archivos
-- -------------------------------------------------------------------------

CREATE TABLE `transacciones_archivos` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`transaccion_log_id` INT(11) NOT NULL,
`nombre_fichero` VARCHAR(150) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`ruta_fichero` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`tamano_fichero` INT(11) NOT NULL COMMENT 'Tamaño en bytes del adjunto',
PRIMARY KEY (`id`),
INDEX `FK_transacciones_archivos` (`transaccion_log_id`),
CONSTRAINT `FK_transacciones_archivos` FOREIGN KEY (`transaccion_log_id`)
REFERENCES `transacciones_log` (`id`) ON DELETE CASCADE
)
COMMENT='Notas, documentos o fotografías sobre las transacciones'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- presupuestos_transacciones
-- -------------------------------------------------------------------------

CREATE TABLE `presupuestos_transacciones` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`presupuesto_id` INT(11) NOT NULL,
`transaccion_log_id` INT(11) NOT NULL,
PRIMARY KEY (`id`),
INDEX `FK_presupuestos_p` (`presupuesto_id`),
INDEX `FK_presupuestos_t` (`transaccion_log_id`),
CONSTRAINT `FK_presupuestos_p` FOREIGN KEY (`presupuesto_id`) REFERENCES
`presupuestos` (`id`) ON DELETE CASCADE,
CONSTRAINT `FK_presupuestos_t` FOREIGN KEY (`transaccion_log_id`) REFERENCES
`transacciones_log` (`id`) ON DELETE CASCADE
)
COMMENT='Relación entre las transacciones y presupuestos'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- categorias_transacciones
-- -------------------------------------------------------------------------

CREATE TABLE `categorias_transacciones` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`categoria_id` INT(11) NOT NULL,
`transaccion_log_id` INT(11) NOT NULL,
PRIMARY KEY (`id`),
INDEX `FK_categorias_c` (`categoria_id`),
INDEX `FK_categorias_t` (`transaccion_log_id`),
CONSTRAINT `FK_categorias_c` FOREIGN KEY (`categoria_id`) REFERENCES `categorias`
(`id`),
CONSTRAINT `FK_categorias_t` FOREIGN KEY (`transaccion_log_id`) REFERENCES
`transacciones_log` (`id`)
)
COMMENT='Relación de categorías y transacciones'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- huchas
-- -------------------------------------------------------------------------

CREATE TABLE `huchas` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`nombre` VARCHAR(150) NOT NULL COLLATE 'utf8mb4_spanish_ci',
`cuenta_id` INT(11) NOT NULL,
`importe_objetivo` DECIMAL(22,12) NOT NULL,
`descripcion` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_spanish_ci',
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
`fecha_inicio` DATE NOT NULL,
`fecha_objetivo` DATE NULL DEFAULT NULL,
PRIMARY KEY (`id`),
INDEX `FK_huchas_cuentas` (`cuenta_id`),
CONSTRAINT `FK_huchas_cuentas` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`
id`) ON DELETE CASCADE
)
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- huchas_movimientos
-- -------------------------------------------------------------------------

CREATE TABLE `huchas_movimientos` (
`id` INT(11) NOT NULL AUTO_INCREMENT,
`hucha_id` INT(11) NOT NULL,
`importe` DECIMAL(22,12) NOT NULL,
`fecha` DATE NOT NULL,
`fecha_creacion` DATETIME NOT NULL,
`fecha_actualizacion` DATETIME NOT NULL,
PRIMARY KEY (`id`),
INDEX `FK_huchas_movimientos_huchas` (`hucha_id`),
CONSTRAINT `FK_huchas_movimientos_huchas` FOREIGN KEY (`hucha_id`) REFERENCES
`huchas` (`id`) ON DELETE CASCADE
)
COMMENT='Dinero metido o sacado de las huchas'
COLLATE='utf8mb4_spanish_ci'
ENGINE=InnoDB
;

-- #########################################################################

--                          INICIALIZACIÓN DE TABLAS

-- #########################################################################

-- cuentas_tipos
-- -------------------------------------------------------------------------

INSERT INTO cuentas_tipos (nombre, descripcion) VALUES ('Cuenta corriente', 'Cuenta corriente');
INSERT INTO cuentas_tipos (nombre, descripcion) VALUES ('Cuenta de ahorros', 'Cuenta de ahorros');
INSERT INTO cuentas_tipos (nombre, descripcion) VALUES ('Efectivo', 'Dinero en efectivo');
INSERT INTO cuentas_tipos (nombre, descripcion) VALUES ('Cuenta de gastos', 'Cuenta de gastos');
INSERT INTO cuentas_tipos (nombre, descripcion) VALUES ('Cuenta de ingresos', 'Cuenta de ingresos');

-- divisas
-- -------------------------------------------------------------------------

INSERT INTO divisas (nombre, codigo, simbolo, decimales) VALUES ('Euro', 'EUR', '€', 2);
INSERT INTO divisas (nombre, codigo, simbolo, decimales) VALUES ('Dólar estadounidense', 'USD', '$', 2);
INSERT INTO divisas (nombre, codigo, simbolo, decimales) VALUES ('Libra esterlina', 'GBP', '£', 2);
INSERT INTO divisas (nombre, codigo, simbolo, decimales) VALUES ('Yen', 'JPY', '¥', 2);
INSERT INTO divisas (nombre, codigo, simbolo, decimales) VALUES ('Yuan chino', 'CNY', '¥', 2);


-- transacciones_tipos
-- -------------------------------------------------------------------------

INSERT INTO transacciones_tipos (tipo) VALUES ('Gasto');
INSERT INTO transacciones_tipos (tipo) VALUES ('Ingreso');
INSERT INTO transacciones_tipos (tipo) VALUES ('Transferencia');