# Miscu

Aplicación web para el control de las finanzas personales

Proyecto Fin de Ciclo DAW

## Organización de repositorio

* [doc/](https://gitlab.com/TempWin/miscu/tree/master/doc): documentación del proyecto
* [docker/](https://gitlab.com/TempWin/miscu/tree/master/docker): ficheros para crear el entorno donde implantar Miscu usando [Docker](https://www.docker.com/)
* [www/](https://gitlab.com/TempWin/miscu/tree/master/www): aplicación web Miscu
* `creacion_base_datos.sql`: script SQL con las sentencias para crear las tablas necesarias e inicializarlas.

![Miscu](miscu.png "Pantalla principal de Miscu")